﻿using UnityEngine.UI;

namespace UnityEditor.UI
{
    [CustomEditor(typeof(MultiGraphicButton), true)]
    [CanEditMultipleObjects]
    /// <summary>
    ///   Custom Editor for the Button Component.
    ///   Extend this class to write a custom editor for an Button-derived component.
    /// </summary>
    public class MultiGraphicButtonEditor : ButtonEditor
    {
        SerializedProperty ExtraGraphicsBlockProperty;
        SerializedProperty ExtraColorsBlockProperty;

        protected override void OnEnable()
        {
            base.OnEnable();
            ExtraGraphicsBlockProperty = serializedObject.FindProperty("ExtraGraphics");
            ExtraColorsBlockProperty = serializedObject.FindProperty("ExtraColors");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.Space();

            serializedObject.Update();
            EditorGUILayout.PropertyField(ExtraGraphicsBlockProperty, true);
            EditorGUILayout.PropertyField(ExtraColorsBlockProperty);
            serializedObject.ApplyModifiedProperties();
        }
    }
}