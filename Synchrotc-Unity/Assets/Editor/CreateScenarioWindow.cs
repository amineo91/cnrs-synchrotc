﻿using Keyveo.Scenarisation;
using UnityEditor;
using UnityEngine;

public class CreateScenarioWindow : EditorWindow
{
    bool clearConsoleAtStart = true;
    int dayId = 3;
    DifficultyMode difficulty;
    bool setInManagers = true;

    // Add menu item
    [MenuItem("Keyveo/Create Scenario")]
    public static void ShowWindow()
    {
        //Show existing window instance. If one doesn't exist, make one.
        EditorWindow.GetWindow(typeof(CreateScenarioWindow));
    }

    void OnGUI()
    {
        clearConsoleAtStart = EditorGUILayout.Toggle("Clear console first", clearConsoleAtStart);
        dayId = EditorGUILayout.IntField("Day", dayId);
        difficulty = (DifficultyMode)EditorGUILayout.EnumPopup(difficulty);
        setInManagers = EditorGUILayout.Toggle("Set in managers", setInManagers);

        if (GUILayout.Button("Create!"))
        {
            if (clearConsoleAtStart)
            {
                // Clear the console so that warning and error counts are relevant
                EditorUtils.ClearConsole();
            }

            ScenarioFactoryAsync scenarioFactoryAsync = FindObjectOfType<ScenarioFactoryAsync>();
            scenarioFactoryAsync.LoadAndCreateAsync(dayId, difficulty, true, (scenarioSO) =>
            {

                if (setInManagers)
                {
                    ScenarioManager scenarioManager = FindObjectOfType<ScenarioManager>();
                    scenarioManager.m_scenario = scenarioSO;

                    GameManager gameManager = FindObjectOfType<GameManager>();
                    gameManager.SetDifficultyFromEditor(difficulty);
                }
            });
        }
    }

}