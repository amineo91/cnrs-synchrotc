﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Waypoint))]
public class WaypointEditor : Editor
{
#if UNITY_EDITOR
    void OnSceneGUI()
    {
        Waypoint t = (Waypoint)target;

        if (Event.current.type == EventType.KeyDown && (Event.current.keyCode == KeyCode.P || Event.current.keyCode == KeyCode.O))
        {
            Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(ray, out hit, 1000.0f))
            {
                Vector3 newTilePosition = hit.point + Vector3.up;

                float minDist = 50000f;
                Waypoint target = null;
                foreach (Waypoint wp in FindObjectsOfType<Waypoint>())
                {
                    if (wp == t)
                        continue;

                    if (Vector3.Distance(wp.transform.position, newTilePosition) < minDist)
                    {
                        target = wp;
                        minDist = Vector3.Distance(wp.transform.position, newTilePosition);
                    }
                }

                Debug.Log(target + " : " + minDist);

                if (target == null || minDist > 1.0f)
                {
                    //CREATE NEW
                    GameObject go = new GameObject();
                    go.name = "WayPoint";
                    go.transform.position = newTilePosition;
                    go.transform.parent = t.transform.parent;
                    target = go.AddComponent<Waypoint>();
                    target.AssociatedGraph = t.AssociatedGraph;
                }

                if(target.HasLink(t) != null && t.HasLink(target) != null)
                {
                    target.RemoveLink(t);
                    t.RemoveLink(target);
                } else
                {
                    if(t.HasLink(target) == null)
                        t.AddLink(target, Event.current.keyCode == KeyCode.P);

                    if(target.HasLink(t) == null)
                        target.AddLink(t, Event.current.keyCode == KeyCode.P);
                }
            }
        }
    }
#endif
}
