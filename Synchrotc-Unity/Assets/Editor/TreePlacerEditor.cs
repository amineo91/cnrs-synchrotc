﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TreePlacer))]
public class TreePlacerEditor : Editor
{
    void OnSceneGUI()
    {
        TreePlacer t = (TreePlacer) target;

        if (!t.CanPlace)
            return;

        if (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.P)
        {
            Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(ray, out hit, 1000.0f))
            {
                Debug.Log(Event.current.mousePosition);
                Vector3 newTilePosition = hit.point;
                Instantiate(t.TreePrefab, newTilePosition, Quaternion.Euler(new Vector3(0, Random.Range(0, 360), 0))).transform.SetParent(t.transform);
            }
        }
    }
}