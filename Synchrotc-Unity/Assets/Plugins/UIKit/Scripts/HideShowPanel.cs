﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.UI;

public class HideShowPanel : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	private bool isOpen = true;
	private bool isShown = false;

	[SerializeField]
	private RectTransform m_Menu;

	[SerializeField]
	private Graphic[] m_HoverUIElements;
	[SerializeField]
	private Outline m_MenuOutline;

	[SerializeField]
	private Transform m_ButtonArrow;

	[SerializeField]
	private Vector2 m_OpenPosition;

	[SerializeField]
	private Vector2 m_ClosedPosition;


    // Use this for initialization
    void Start () {
		isOpen = true;
		Open();
	}

	void Update(){
		if(!isShown && !isOpen){
			ShowArrow();
		}
	}

	public void Toggle(){
		if(isOpen){
			Close();
		} else {
			Open();
		}
	}

	public void Open(bool instant = false){
		isOpen = true;
		m_Menu.DOKill();
		m_Menu.DOAnchorPos(m_OpenPosition, instant ? 0f : 1f).SetEase(Ease.InOutCirc);
		m_ButtonArrow.DOKill();
		m_ButtonArrow.DOScaleY(-2f, instant ? 0f : 1f).SetEase(Ease.InOutCirc);
		HideArrow();
	}

	public void Close(bool instant = false){
		isOpen = false;
		m_Menu.DOKill();
		m_Menu.DOAnchorPos(m_ClosedPosition, instant ? 0f : 1f).SetEase(Ease.InOutCirc);
		m_ButtonArrow.DOKill();
		m_ButtonArrow.DOScaleY(2f, instant ? 0f : 1f).SetEase(Ease.InOutCirc);
		HideArrow();
	}

	void ShowArrow(){
		isShown = true;
		foreach(Graphic g in m_HoverUIElements){
			g.DOKill();
			g.DOFade(1, 0.5f).SetEase(Ease.InOutCubic);
		}
		m_MenuOutline.DOKill();
		m_MenuOutline.DOFade(1, 0.5f).SetEase(Ease.InOutCubic);
	}

	void HideArrow(){
		isShown = false;
		foreach(Graphic g in m_HoverUIElements){
			g.DOKill();
			g.DOFade(0, 0.5f).SetEase(Ease.InOutCubic);
		}
		m_MenuOutline.DOKill();
		m_MenuOutline.DOFade(0, 0.5f).SetEase(Ease.InOutCubic);
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
		ShowArrow();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
		HideArrow();
    }
}
