﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class Fader : Showable {

	private Image image;

    private float defaultAlpha = 1f;

	// Use this for initialization
	protected override void Start () {
		image = GetComponent<Image>();
        defaultAlpha = image.color.a;
		base.Start();
	}
	
	public override Tweener Show(bool instant = false){
		AnimationData animationData = GetAnimation(true);

        image.enabled = true;
        image.DOKill();
        return image.DOFade(defaultAlpha, instant ? 0 : animationData.m_Speed).SetEase(animationData.m_Ease).OnComplete(() => m_Shown = true);
    }

	public override Tweener Hide(bool instant = false){
		AnimationData animationData = GetAnimation(false);

		image.DOKill();
		return image.DOFade(0, instant ? 0 : animationData.m_Speed).SetEase(animationData.m_Ease).OnComplete(() => {
            image.enabled = false;
            m_Shown = false;
        });
	}
}
