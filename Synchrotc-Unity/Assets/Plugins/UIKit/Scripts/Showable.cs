﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Showable : MonoBehaviour {

    protected bool m_Shown = false;

	[SerializeField]
	private bool m_HiddenByDefault = true;

	[SerializeField]
	private AnimationSpeed m_AnimationSpeed;
	[SerializeField]
	private AnimationType m_AnimationType;

	protected virtual void Start(){
		if(m_HiddenByDefault){
			Hide(true);
		} else {
			Show(true);
		}
	}

	public virtual void Toggle(){
		if(m_Shown){
			Hide();
		} else {
			Show();
		}
	}

	public virtual Tweener Show(bool instant = false){
		m_Shown = true;
        return null;
	}

	public virtual Tweener Hide(bool instant  = false){
		m_Shown = false;
        return null;
    }

	protected AnimationData GetAnimation(bool isIn){
		return UIConfigurationHandler.Instance.CurrentConfiguration.GetAnimation(isIn, m_AnimationType, m_AnimationSpeed);
	}

    public float GetDuration(bool isIn)
    {
        return GetAnimation(isIn).m_Speed;
    }
}
