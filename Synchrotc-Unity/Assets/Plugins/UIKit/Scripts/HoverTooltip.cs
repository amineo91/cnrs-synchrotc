﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HoverTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
	public TooltipText m_Text;

    public void OnPointerEnter(PointerEventData eventData)
    {
			if(m_Text != null){
				TooltipManager.Instance.DisplayTooltip(m_Text.Title, m_Text.Description);
			}
    }

    public void OnPointerExit(PointerEventData eventData)
    {
			if(m_Text != null){
				TooltipManager.Instance.HideCurrentTooltip();
			}
    }

}
