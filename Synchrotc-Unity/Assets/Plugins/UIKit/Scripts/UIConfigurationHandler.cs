﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIConfigurationHandler : MonoBehaviour {

	public static UIConfigurationHandler Instance;

	public UIConfiguration CurrentConfiguration;

	void Awake()
	{
		//Check if instance already exists
		if (Instance == null)
			//if not, set instance to this
			Instance = this;
		
		////If instance already exists and it's not this:
		//else if (Instance != this)
		//	//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
		//	Destroy(gameObject);
	}
}
