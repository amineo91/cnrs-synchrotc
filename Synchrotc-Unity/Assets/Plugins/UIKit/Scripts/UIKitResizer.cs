﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UIKitResizer : Showable
{
	[SerializeField]
	private Vector3 m_MinSize;

    [SerializeField]
    private Vector3 m_MaxSize;

    [SerializeField]
    private bool m_IsMaxSizeReinitAtAwake = true;

    private void Awake()
    {
        if (m_IsMaxSizeReinitAtAwake)
        {
            m_MaxSize = transform.localScale;
        }
    }

    public override Tweener Show(bool instant = false)
    {
		base.Show(instant);

		AnimationData animationData = GetAnimation(true);

		transform.DOKill();
        return transform.DOScale(m_MaxSize, instant ? 0 : animationData.m_Speed).SetEase(animationData.m_Ease);
    }

    public override Tweener Hide(bool instant = false)
    {
		base.Hide(instant);

		AnimationData animationData = GetAnimation(false);

		transform.DOKill();
        return transform.DOScale(m_MinSize, instant ? 0 : animationData.m_Speed).SetEase(animationData.m_Ease);
    }
}
