﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[System.Serializable]
public enum Direction{
	UP,
	RIGHT,
	DOWN,
	LEFT
}

//Works with centered object on screen. Might or might not work for objects positionned by another method of alignement
public class UIMover : Showable {

	[SerializeField]
	private Direction m_MovementDirection = Direction.DOWN;

    private Vector2 shownPosition;
    private Vector2 hiddenPosition;

    private RectTransform rect;

	// Use this for initialization
	protected override void Start () {
		rect = GetComponent<RectTransform>();

		hiddenPosition = (m_MovementDirection == Direction.UP || m_MovementDirection == Direction.DOWN) ? 
			new Vector2(0, (Screen.height*3) + rect.anchoredPosition.y) :
			new Vector2((Screen.width*3) + rect.anchoredPosition.x, 0);
		
		if(m_MovementDirection == Direction.UP || m_MovementDirection == Direction.RIGHT){
			hiddenPosition *= -1f;
		}

        shownPosition = rect.anchoredPosition;

        base.Start();
	}

	public override Tweener Show(bool instant = false){
		base.Show(instant);

		AnimationData animationData = GetAnimation(true);

		rect.DOKill();

        return rect.DOAnchorPos(shownPosition, instant ? 0 : animationData.m_Speed).SetEase(animationData.m_Ease);
	}

	public override Tweener Hide(bool instant = false){
		base.Hide(instant);

		AnimationData animationData = GetAnimation(false);

		rect.DOKill();
        return rect.DOAnchorPos(hiddenPosition, instant ? 0 : animationData.m_Speed).SetEase(animationData.m_Ease);
	}
}
