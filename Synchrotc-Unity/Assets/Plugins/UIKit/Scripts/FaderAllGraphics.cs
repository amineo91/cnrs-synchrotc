﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FaderAllGraphics : MonoBehaviour {

    [SerializeField]
    private float m_DefaultAnimationDuration = 0.5f;

    private Graphic[] graphics;
    private Dictionary<Graphic, float> originalGraphicsAlpha = new Dictionary<Graphic, float>();
    private List<Tweener> m_tweens = new List<Tweener>();

    private void Awake()
    {
        graphics = GetComponentsInChildren<Graphic>();
        foreach (Graphic graphic in graphics)
        {
            originalGraphicsAlpha.Add(graphic, graphic.color.a);
        }
    }
	
	public virtual void Show(float animationDuration = -1f, Ease ease = Ease.OutSine)
    {
        // Set default animation duration value from edtior
        if (animationDuration < 0)
        {
            animationDuration = m_DefaultAnimationDuration;
        }

        // Clean all previous tweens
        foreach (Tween tween in m_tweens)
        {
            tween.Kill();
        }
        m_tweens.Clear();

        // Start new tweens
        foreach (Graphic graphic in graphics)
        {
            graphic.enabled = true;
            float originalAlpha = originalGraphicsAlpha[graphic];
            m_tweens.Add(graphic.DOFade(originalAlpha, animationDuration).SetEase(ease));
        }
    }

	public virtual void Hide(float animationDuration = -1f, Ease ease = Ease.OutSine)
    {
        // Set default animation duration value from edtior
        if (animationDuration < 0)
        {
            animationDuration = m_DefaultAnimationDuration;
        }

        // Clean all previous tweens
        foreach (Tween tween in m_tweens)
        {
            tween.Kill();
        }
        m_tweens.Clear();

        // Start new tweens
        foreach (Graphic graphic in graphics)
        {
            float originalAlpha = originalGraphicsAlpha[graphic];
            m_tweens.Add(graphic.DOFade(0, animationDuration).SetEase(ease).OnComplete(() => graphic.enabled = false));
        }
    }

    /// <summary>
    /// Shows instantly all graphics.
    /// </summary>
    public virtual void InstantShow()
    {
        // Clean all previous tweens
        foreach (Tween tween in m_tweens)
        {
            tween.Kill();
        }
        m_tweens.Clear();

        // Update instant
        foreach (Graphic graphic in graphics)
        {
            Color color = graphic.color;
            color.a = originalGraphicsAlpha[graphic];
            graphic.color = color;
            graphic.enabled = true;
        }
    }

    /// <summary>
    /// Hides instantly all graphics.
    /// </summary>
    public virtual void InstantHide()
    {
        // Clean all previous tweens
        foreach (Tween tween in m_tweens)
        {
            tween.Kill();
        }
        m_tweens.Clear();

        // Update instant
        foreach (Graphic graphic in graphics)
        {
            Color color = graphic.color;
            color.a = 0;
            graphic.color = color;
            graphic.enabled = false;
        }
    }
}
