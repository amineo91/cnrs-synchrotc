﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiShowable : MonoBehaviour
{
    [SerializeField]
    private Showable[] m_Showables;

    public virtual void Show(bool instant = false)
    {
        foreach(Showable S in m_Showables)
        {
            S.Show(instant);
        }
    }

    public virtual void Hide(bool instant = false)
    {
        foreach (Showable S in m_Showables)
        {
            S.Hide(instant);
        }
    }

}
