﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Tooltip Text", menuName = "TooltipText/Create New Tooltip Text")]
public class TooltipText : ScriptableObject {

	public string Title;

	public string Description;
	
}
