﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Sirenix.OdinInspector;

[RequireComponent(typeof(Outline))]
public class OutlinePulser : MonoBehaviour {
    
    [SerializeField]
    private float m_PulseSpeed;

    [SerializeField]
    private Color m_MaxColor;
    [SerializeField]
    private Color m_MinColor;

    [SerializeField]
    private float m_MaxWidth;
    [SerializeField]
    private float m_MinWidth;

    [SerializeField]
    private bool m_Repeat; //true => repeat, false => ping pong

    private float lerper = 0;

    private Outline outline;

    [SerializeField]
    private bool m_ConstantPulse = true;

    void Start()
    {
        outline = GetComponent<Outline>();
    }

	// Update is called once per frame
	void Update () {

        if(!m_ConstantPulse){
            outline.effectDistance = new Vector2(m_MinWidth, m_MinWidth);
            outline.effectColor = m_MinColor;
            return;
        }

        lerper = m_Repeat ? Mathf.Repeat(Time.time * m_PulseSpeed, 1) : Mathf.PingPong(Time.time *  m_PulseSpeed, 1);

        outline.effectColor = Color.Lerp(m_MaxColor, m_MinColor, lerper);

        outline.effectDistance = new Vector2(
                Mathf.Lerp(m_MaxWidth, m_MinWidth, lerper),
                Mathf.Lerp(m_MaxWidth, m_MinWidth, lerper)
            );
    }

    [Button]
    public void Pulse(){
        if(m_ConstantPulse){
            Debug.LogWarning("Trying to single pulse a constant pulse Outline Pulser. Cancelling because it's useless.");
            return;
        }

        outline.DOKill();

        outline.effectDistance = new Vector2(m_MinWidth, m_MinWidth);
        outline.DOColor(m_MaxColor, m_PulseSpeed).SetEase(Ease.OutBack);
        outline.DOScale(new Vector2(m_MaxWidth, m_MaxWidth), m_PulseSpeed).SetEase(Ease.OutBack).OnComplete(delegate{
            outline.DOColor(m_MinColor, m_PulseSpeed).SetEase(Ease.InOutCirc);
        });
    }
}
