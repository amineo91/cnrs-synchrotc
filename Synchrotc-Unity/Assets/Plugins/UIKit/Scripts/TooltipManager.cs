﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class TooltipManager : MonoBehaviour {

	[SerializeField]
	private Graphic[] m_GraphicalElements;

	[SerializeField]
	private TextMeshProUGUI m_TitleText;
	
	[SerializeField]
	private TextMeshProUGUI m_DescriptionText;

	[SerializeField]
	private RectTransform m_TooltipTransform;

	public static TooltipManager Instance;

    private Dictionary<Graphic, float> alphaPerGraphicalElement;

	void Awake()
	{
		//Check if instance already exists
		if (Instance == null)
			//if not, set instance to this
			Instance = this;
		
		//If instance already exists and it's not this:
		else if (Instance != this)
			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);

        m_GraphicalElements = GetComponentsInChildren<Graphic>();
        alphaPerGraphicalElement = new Dictionary<Graphic, float>();
        foreach (Graphic graphic in m_GraphicalElements)
        {
            alphaPerGraphicalElement.Add(graphic, graphic.color.a);
        }
	}

	// Use this for initialization
	void Start () {
		HideCurrentTooltip(true);
	}
	
	// Update is called once per frame
	void Update () {
		//MOVE TOOLTIP ON MOUSE
		//m_TooltipTransform.position = Input.mousePosition;
	}

	public void DisplayTooltip(string titleText, string descriptionText){
		m_TitleText.text = titleText;
		m_DescriptionText.text = descriptionText;
		ShowTooltip();
	}

	public void DisplayTooltip(string descriptionText){
		m_TitleText.text = "";
		m_DescriptionText.text = descriptionText;
		ShowTooltip();
	}

	void ShowTooltip(){
		foreach(Graphic g in m_GraphicalElements){
			g.DOKill();
			g.DOFade(1, 0.35f).SetEase(Ease.OutCubic);
		}
	}

	public void HideCurrentTooltip(bool instant = false){
		foreach(Graphic g in m_GraphicalElements){
			g.DOKill();
			g.DOFade(0, instant ? 0f : 0.35f).SetEase(Ease.OutCubic);
		}
	}
}
