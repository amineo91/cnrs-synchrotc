﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public enum AnimationType{
	SOFT,
	STANDARD,
	FUNKY
}

public enum AnimationSpeed{
	SLOW,
	STANDARD,
	FAST
}

public class AnimationData{
	public float m_Speed;
	public Ease m_Ease;
}

[CreateAssetMenu(fileName = "New UIConfiguration", menuName = "Configuration/Create New UIConfiguration")]
public class UIConfiguration : ScriptableObject {
	[Header("PALETTE")]
	public Color m_PrimaryColor;
	public Color m_AccentColor;

	public Color m_SecondaryColor;

	[Header("Animations")]
	[SerializeField]	private float m_AnimationSlowSpeed;
	[SerializeField]	private float m_AnimationStandardSpeed;
	[SerializeField]	private float m_AnimationFastSpeed;
	[SerializeField]	private Ease m_AnimationSoftEaseIn;
	[SerializeField]	private Ease m_AnimationSoftEaseOut;
	[SerializeField]	private Ease m_AnimationStandardEaseIn;
	[SerializeField]	private Ease m_AnimationStandardEaseOut;
	[SerializeField]	private Ease m_AnimationFunkyEaseIn;
	[SerializeField]	private Ease m_AnimationFunkyEaseOut;

	public AnimationData GetAnimation(bool isIn, AnimationType type, AnimationSpeed speed){
		return new AnimationData(){
			m_Speed = speed == AnimationSpeed.SLOW ? m_AnimationSlowSpeed : (speed == AnimationSpeed.STANDARD ? m_AnimationStandardSpeed : m_AnimationFastSpeed),
			m_Ease = isIn ? (
				type == AnimationType.SOFT ? m_AnimationSoftEaseIn : type == AnimationType.STANDARD ? m_AnimationStandardEaseIn : m_AnimationFunkyEaseIn
			) : (
				type == AnimationType.SOFT ? m_AnimationSoftEaseOut : type == AnimationType.STANDARD ? m_AnimationStandardEaseOut : m_AnimationFunkyEaseOut
			)
		};
	}

}
