﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppartementObject : MonoBehaviour
{
    public List<GameObject> ListObjects = new List<GameObject>();

    public void DeactivateObject()
    {
        foreach (GameObject objectApp in ListObjects)
        {
            objectApp.SetActive(false);
        }
    }

    public void ActivateObject()
    {
        foreach (GameObject objectApp in ListObjects)
        {
            objectApp.SetActive(true);
        }
    }

}
