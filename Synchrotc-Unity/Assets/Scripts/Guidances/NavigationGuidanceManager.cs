﻿using System;
using Keyveo.Features.Guidance;
using Sirenix.OdinInspector;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Control navigation guidances: pins, directional arrow, in world lines
/// </summary>
public class NavigationGuidanceManager : SerializedMonoBehaviour
{
    public static NavigationGuidanceManager Instance;

    private Waypoint start;
    private Waypoint End;
    
    private Waypoint StartMap;
    private Waypoint EndMap;

    public Indicator m_guidanceGraph;
    
    private List<Waypoint> m_shortestPath;

    /// <summary>
    /// Used to flip direction of world line
    /// </summary>
    [SerializeField]
    private Material m_WorldLineGuidanceMaterial;

    /// <summary>
    /// Pins prefab for target location
    /// </summary>
    [SerializeField]
    private WorldPinsNavigationGuidance m_WorldPinsNavigationGuidancePrefab;

    /// <summary>
    /// Parent of world pins navigation guidances
    /// </summary>
    [SerializeField]
    private Transform m_ContainerPinsWorldNavigationGuidances;

    [SerializeField]
    private Dictionary<Location, GameObject> m_WorldPinsGuidances = new Dictionary<Location, GameObject>();

    [SerializeField]
    private Dictionary<Location, Waypoint> m_WorldWaypointGuidances = new Dictionary<Location, Waypoint>();
    
    [SerializeField] 
    private Transform positionMarker0;
    
    [SerializeField] 
    private Transform positionMarkerX;
    
    [SerializeField] 
    private Transform positionMarkerZ;
    
    [SerializeField] 
    private Transform positionMarkerP;
    
    [SerializeField] 
    private Transform positionMinimap0;
    
    [SerializeField] 
    private Transform positionMinimapX;
    
    [SerializeField] 
    private Transform positionMinimapY;
    
    [SerializeField] 
    private Transform positionMinimapP;
    
    [SerializeField] 
    private GameObject map;
    
    [SerializeField] 
    private GameObject waypointsParent;

    public Location lastKnownLocation;

    public GameObject choiceMinimapWindow;

    void Awake()
    {
        //Check if instance already exists
        if (Instance == null)
            //if not, set instance to this
            Instance = this;

        HideAll();

    }

    /// <summary>
    /// Show navigation guidances between two specified locations
    /// </summary>
    /// <param name="fromLocation"></param>
    /// <param name="toLocation"></param>
    public void Show(Location fromLocation, Location toLocation)
    {
        if (DifficultyService.HasWorldGuidances())
        {
            ShowWorldPins(toLocation);

            if (DifficultyService.HasPhoneLineGuidance())
            {
                ShowWorldLine(fromLocation, toLocation, true);
            }

            if (DifficultyService.HasWorldLineGuidance())
            {
                ShowWorldLine(fromLocation, toLocation);
            }

            if (DifficultyService.HasChoiceMinimapWindow())
            {
                choiceMinimapWindow.SetActive(true);
            }

            lastKnownLocation = fromLocation;
        }
    }

    public void HideMinimapChoices()
    {
        choiceMinimapWindow.SetActive(false);
    }

    public void PutThisLocationOnMinimap(Location locationToGo)
    {
        HideAll();
        ShowWorldLine(lastKnownLocation,locationToGo,true);
    }

    private void ShowWorldPins(Location toLocation)
    {
        if (m_WorldPinsGuidances.ContainsKey(toLocation))
        {
            GameObject worldPins = m_WorldPinsGuidances[toLocation];
            worldPins.SetActive(true);
        }
        else
        {
            // Create one dynamically
            WorldPinsNavigationGuidance worldPins = Instantiate(m_WorldPinsNavigationGuidancePrefab, m_ContainerPinsWorldNavigationGuidances);
            worldPins.name = toLocation.LocationName + "_" + worldPins.name;
            worldPins.SetLocation(toLocation);

            Waypoint waypoint = m_WorldWaypointGuidances[toLocation];
            worldPins.transform.position = waypoint.transform.position;

            // Store it
            m_WorldPinsGuidances.Add(toLocation, worldPins.gameObject);
        }
    }

    private void ShowWorldLine(Location fromLocation, Location toLocation, bool minimapOnly = false)
    {
        Location location = toLocation;
        m_WorldWaypointGuidances.TryGetValue(fromLocation, out start);
        m_WorldWaypointGuidances.TryGetValue(toLocation, out End);
        List<Waypoint> waypoints = CalculatePath();
        m_guidanceGraph.m_points.RemoveAll((transform) => { return transform; });
        foreach(Waypoint waypoint in waypoints)
        {
            m_guidanceGraph.m_points.Add(waypoint.transform);
        }
        m_guidanceGraph.CreateVisual(minimapOnly);
    }

    public void HideAll()
    {
        foreach (GameObject go in m_WorldPinsGuidances.Values)
        {
            go.SetActive(false);
        }
        RemoveVisuals();
    }

    public void RemoveVisuals()
    {
        Transform visuals = m_guidanceGraph.transform.Find("Visuals");
        if (visuals != null)
        {
            m_guidanceGraph.transform.Find("Visuals").gameObject.SetActive(false);
        }
    }

    private List<Waypoint> CalculatePath()
    {
        ResetWaypoints();
        DijkstraSearch();
        var shortestPath = new List<Waypoint>();
        shortestPath.Add(End);
        //Debug.Log(End);
        CreatePathRecursive(shortestPath, End);
        shortestPath.Reverse();
        return shortestPath;
    }

    private void ResetWaypoints()
    {
        List<Waypoint> nodes = m_guidanceGraph.transform.Find("Positions").GetComponentsInChildren<Waypoint>().ToList();
        foreach (Waypoint node in nodes)
        {
            node.ResetCost();
        }
    }
    
    private void DijkstraSearch()
    {
        start.MinimalCostFromStart = 0;
        List<Waypoint> prioQueue = new List<Waypoint>();
        Waypoint currentNode;
        prioQueue.Add(start);
        do
        {
            prioQueue = prioQueue.OrderBy(x => x.MinimalCostFromStart).ToList();
            currentNode = prioQueue.First();
            prioQueue.Remove(currentNode);
            foreach (var link in currentNode.GetLinks().OrderBy(x => x.Weight))
            {
                var childNode = link.Destination;
                if (childNode.MinimalCostFromStart < 0 || currentNode.MinimalCostFromStart + link.Weight < childNode.MinimalCostFromStart)
                {
                    childNode.MinimalCostFromStart = currentNode.MinimalCostFromStart + link.Weight;
                    childNode.NearestToStart = currentNode;
                    if (!prioQueue.Contains(childNode))
                        prioQueue.Add(childNode);
                }
            }
        } while (prioQueue.Any() && currentNode != End);
    }


    private void CreatePathRecursive(List<Waypoint> list, Waypoint node)
    {
        //Debug.Log(node.NearestToStart);
        
        if (!node.NearestToStart)
        {
            //Debug.Log("fail");
            return;
        }
        list.Add(node.NearestToStart);
        //Debug.Log("i'm in'");
        CreatePathRecursive(list, node.NearestToStart);
    }
   
    [Button]
    public void UpdateGraphDistances()
    {
        List<Waypoint> nodes = m_guidanceGraph.transform.Find("Positions").GetComponentsInChildren<Waypoint>().ToList();
        foreach(Waypoint node in nodes)
        {
            Vector3 nodeTransform = node.transform.position;
            foreach(WaypointLink link in node.GetLinks())
            {
                link.Weight = Mathf.Abs(Vector3.Distance(nodeTransform, link.Destination.transform.position));
            }
        }
    }

   

    #region methods for the minimap

    private void placeGameobjectOnTheMap(Transform objectToCopy, Transform objectToPlace = null)
    {
        var totalMarkerX = Math.Abs(positionMarkerX.transform.position.x - positionMarker0.transform.position.x);
        var totalMapX = Math.Abs(positionMinimapX.transform.position.x - positionMinimap0.transform.position.x);
        var totalMarkerY = Math.Abs(positionMarkerZ.transform.position.z - positionMarker0.transform.position.z);
        var totalMapY = Math.Abs(positionMinimapY.transform.position.y - positionMinimap0.transform.position.y);

        var valueXObjectToCopy = objectToCopy.position.x;
        var valueZObjectToCopy = objectToCopy.position.z;
        var totalXObjectToCopy = Math.Abs(valueXObjectToCopy - positionMarker0.transform.position.x);
        var totalZObjectToCopy = Math.Abs(valueZObjectToCopy - positionMarker0.transform.position.z);

        var pourcentX = (100 * totalXObjectToCopy) / totalMarkerX;
        var pourcentY = (100 * totalZObjectToCopy) / totalMarkerY;

        var totalMapXWithPourcent = (pourcentX / 100) * totalMapX;
        var totalMapYWithPourcent = (pourcentY / 100) * totalMapY;

        var posXObjectToPlace = positionMinimap0.transform.position.x + totalMapXWithPourcent;
        var posYObjectToPlace = positionMinimap0.transform.position.y + totalMapYWithPourcent;

        var positionObjectToPlace = new Vector3(posXObjectToPlace,posYObjectToPlace,0);

        objectToPlace.position = positionObjectToPlace;
        
        //Debug.Log(pourcentX +"blob"+ pourcentY);
    }
    
    /*[Button]
    public void testPourcent()
    {
        for (var i = 0; i<waypointsParent.transform.childCount ; i++)
        {
            var waypointMap = Instantiate(waypointsParent.transform.GetChild(i).gameObject, waypointsMapParent.transform);
            placeGameobjectOnTheMap(waypointsParent.transform.GetChild(i),waypointMap.transform);
        }
        
        for (var i = 0; i<waypointsParent.transform.childCount ; i++)
        {
            var previousLink = waypointsParent.transform.GetChild(i).GetComponent<Waypoint>().m_Links[0].Destination.gameObject.transform.GetSiblingIndex();
            var nextLink = waypointsParent.transform.GetChild(i).GetComponent<Waypoint>().m_Links[1].Destination.gameObject.transform.GetSiblingIndex();

            waypointsMapParent.transform.GetChild(i).GetComponent<Waypoint>().m_Links[0].Destination = waypointsMapParent.transform.GetChild(previousLink).GetComponent<Waypoint>();
            waypointsMapParent.transform.GetChild(i).GetComponent<Waypoint>().m_Links[1].Destination = waypointsMapParent.transform.GetChild(nextLink).GetComponent<Waypoint>();
        }

        
    }*/
    
    #endregion
}