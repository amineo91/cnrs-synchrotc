﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    private GameObject Player;

    private void Start()
    {
        Player = GameManager.Instance.m_FirstPersonController.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = new Vector3(Player.transform.position.x,gameObject.transform.position.y, Player.transform.position.z);
        var rotation = gameObject.transform.rotation;
        rotation.eulerAngles = new Vector3(gameObject.transform.rotation.eulerAngles.x, Player.transform.rotation.eulerAngles.y, gameObject.transform.rotation.eulerAngles.z);
        gameObject.transform.rotation = rotation;
    }
}
