﻿using UnityEngine;
using UnityEngine.UI;

public class WorldPinsNavigationGuidance : MonoBehaviour
{
    [SerializeField]
    private Location m_Location;

    private void Awake()
    {
        UpdateText();
    }

    public void SetLocation(Location location)
    {
        m_Location = location;
        UpdateText();
    }

    private void UpdateText()
    {
        GetComponentInChildren<Text>().text = m_Location.LocationName.ToUpper();
    }
}
