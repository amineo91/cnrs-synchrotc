﻿using Sirenix.OdinInspector;
using UnityEngine;

/// <summary>
/// TODO - Temporary static solution. Do the dynamic one: use current position of player instead of fromLocation.
/// </summary>
[CreateAssetMenu(fileName = "New Navigation", menuName = "Navigations/Create New Navigation")]
public class Navigation : SerializedScriptableObject
{
    public Location toLocation;
    public Location fromLocation;

    public Navigation(Location toLocation, Location fromLocation)
    {
        this.toLocation = toLocation;
        this.fromLocation = fromLocation;
    }

    public override bool Equals(object other)
    {
        if (other != null && other.GetType() == typeof(Navigation))
        {
            Navigation otherNavigation = other as Navigation;
            return this.toLocation == otherNavigation.toLocation && this.fromLocation == otherNavigation.fromLocation;
        }
        else
        {
            return false;
        }
    }

    public override int GetHashCode()
    {
        return this.toLocation.GetHashCode();
    }
}