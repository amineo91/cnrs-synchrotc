﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Net;
using System;
using UnityEngine.Networking;

public class TrackingManager : MonoBehaviour
{
    public GameObject Player;
    public GameObject TrackingPrefab;
    public GameObject TrackingColliderPrefab;
    public GameObject LineRenderer;

    public Camera cameraScreenTracker;

    public List<GameObject> colliderList = new List<GameObject>();

    public GameObject lastCollider;

    public static TrackingManager Instance;

    public RenderTexture rt;

    private bool needtoTrack = false;
    private bool takeHiResShot = false;
    private bool needToCreate = false;

    public DebugDBService debug;

    public string nameScreen;

    private float totalParcourLength = 0;
    private float countedParcourLength = 0;

    private Vector3 oldPosPlayer;
    private LineRenderer CurrentLine = null;

    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        oldPosPlayer = Player.transform.position;
        StartCoroutine(TrackPosition());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator TrackPosition()
    {
        if (needtoTrack && (oldPosPlayer.x != Player.transform.position.x || oldPosPlayer.z != Player.transform.position.z))
        {

            if (CurrentLine == null)
            {
                needToCreate = true;
            }
            else
            {
                needToCreate = false;
            }
            

            if (needToCreate)
            {
                GameObject trackingLR = Instantiate(LineRenderer, gameObject.transform);
                trackingLR.transform.position = Player.transform.position;
                CurrentLine = trackingLR.GetComponent<LineRenderer>();
                CurrentLine.positionCount++;
                CurrentLine.SetPosition(0, Player.transform.position);
            }
            else
            {
                CurrentLine.positionCount++;
                CurrentLine.SetPosition(CurrentLine.positionCount-1, Player.transform.position);
            }

            oldPosPlayer = Player.transform.position;

            if (StatManager.Instance.ScenarioEntryId != null && StatManager.Instance.ScenarioEntryId != "")
            {
                yield return TimeStatDBService.PutThePlayerPosInDB(StatManager.Instance.ScenarioEntryId, Player.transform.position.x, Player.transform.position.z);
            }

        }

        yield return new WaitForSeconds(1);
        StartCoroutine(TrackPosition());
    }

    public void switchNeedToTrack()
    {
        if (needtoTrack)
        {
            needtoTrack = false;
            CurrentLine = null;
        }
        else
        {
            needtoTrack = true;
        }
    }

    public void TakeHiResShot()
    {
        takeHiResShot = true;
    }

    public static string ScreenShotName(int width, int height)
    {
        return string.Format("{0}/screenshots/screen_{1}x{2}_{3}.png",
                             Application.streamingAssetsPath,
                             width, height,
                             System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
    }

    public void GetTotalParcourLength()
    {
        for(int i=1; i < colliderList.Count; i++)
        {
            totalParcourLength += Vector3.Distance(colliderList[i-1].transform.position,colliderList[i].transform.position);
        }
    }

    public float GetParcourLengthSinceLastTime()
    {
        float lengthSinceLastTime = 0;
        GetTotalParcourLength();

        lengthSinceLastTime = totalParcourLength - countedParcourLength;

        countedParcourLength += lengthSinceLastTime;

        return lengthSinceLastTime;

    }

    void LateUpdate()
    {
        takeHiResShot |= Input.GetKeyDown("k");
        if (takeHiResShot)
        {
            Texture2D screenShot = new Texture2D(430, 635, TextureFormat.RGB24, false);
            cameraScreenTracker.Render();
            RenderTexture.active = rt;
            screenShot.ReadPixels(new Rect(0, 165, 430, 635), 0, 0);
            byte[] bytes = screenShot.EncodeToPNG();
            string filename = ScreenShotName(430, 635);
            System.IO.File.WriteAllBytes(filename, bytes);
            //Debug.Log(string.Format("Took screenshot to: {0}", filename));
            takeHiResShot = false;
            string name = filename.Split('/')[filename.Split('/').Length - 1];
            nameScreen = name;
            //Debug.Log(nameScreen + "ToooTooo Yayaaaaa");
            StartCoroutine(registerScreenShot(filename));
        }
    }

    public IEnumerator registerScreenShot(string path)
    {
        yield return new WaitForSeconds(0.1f);
        if (debug.isActiveAndEnabled)
        {
            if (debug.m_IsLocal)
            {
                string name = path.Split('/')[path.Split('/').Length-1];
                //Debug.Log(name);
                //FileUtil.CopyFileOrDirectory(@"path", @"C:/wamp64/www/SynchroTCBack/dist/php/screenTracker/"+name);
            }
            else
            {
                string name = path.Split('/')[path.Split('/').Length - 1];
                //Debug.Log(name+"TAAATAAA YOYOOOOO");
                yield return new WaitForSeconds(0.5f);
                UploadFileToFTP(path, "/www/Apps/SynchroTC/dist/php/trackerScreenshots/" + name);
                yield return new WaitForSeconds(1);

                //TODO Delete file in unity si génant
            }
        }
    }

    private static void UploadFileToFTP(string source, string destination)
    {
        try
        {
            string filename = Path.GetFileName(source);
            string ftpfullpath = @"ftp://ftp.keyveo.com/";
            FtpWebRequest ftp = (FtpWebRequest)FtpWebRequest.Create(ftpfullpath + "/" + destination);
            ftp.Credentials = new NetworkCredential("keyveocoma", "cR8zaZFyeBX9");

            ftp.KeepAlive = true;
            ftp.UseBinary = true;
            ftp.Method = WebRequestMethods.Ftp.UploadFile;

            FileStream fs = File.OpenRead(source);
            byte[] buffer = new byte[fs.Length];
            fs.Read(buffer, 0, buffer.Length);
            fs.Close();

            Stream ftpstream = ftp.GetRequestStream();
            ftpstream.Write(buffer, 0, buffer.Length);
            ftpstream.Close();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
