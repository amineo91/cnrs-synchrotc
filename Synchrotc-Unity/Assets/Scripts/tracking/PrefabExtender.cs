﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabExtender : MonoBehaviour
{
    private bool shouldExtend = true;

    // Update is called once per frame
    void Update()
    {
        if (shouldExtend)
        {
            Vector3 scaleGameobject = transform.localScale;
            scaleGameobject.y = scaleGameobject.y + 0.2f;
            transform.localScale = scaleGameobject;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (TrackingManager.Instance.lastCollider == other.gameObject)
        {
            shouldExtend = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (TrackingManager.Instance.lastCollider == other.gameObject)
        {
            shouldExtend = false;
        }
    }
}
