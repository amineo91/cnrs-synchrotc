﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public static TimeManager Instance;

    private List<ITimeable> timeables = new List<ITimeable>();

    private float hour = 0;
    private float seconds = 0;

    private string Date;

    [SerializeField]
    private TextMeshProUGUI SmartphoneDate;

    [HideInInspector]
    public float DefaultTimeMultiplier = 1;

    public float timeMultiplier = 1;

    private bool isEnded = false;

    void Awake()
    {
        Instance = this;
        var DateNoForm = DateTime.Parse(InitData.Instance.Date);
        Date = DateNoForm.ToString("dd-MM");
    }

    // Start is called before the first frame update
    void Start()
    {
        hour = 8.30f;
        seconds = 30600;
        ////Debug.Log(Date);
        SmartphoneDate.text = Date;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.Instance.IsPaused)
        {
            ChangeTime(Time.deltaTime * timeMultiplier);

            foreach (ITimeable t in timeables)
            {
                t.OnTimeUpdate(hour);
            }

            ////Debug.Log(seconds);

            if (seconds > 70200 && !isEnded)
            {
                isEnded = true;
                StatManager.Instance.cause = "plus de 19h30";
                NotificationManager.Instance.Notify("Il est plus de 19h30 et vous n'avez pas fini la journée.", NotificationType.Error);
                TrackingManager.Instance.TakeHiResShot();
                StartCoroutine(TimedFail());
            }
        }
    }

    private IEnumerator TimedFail()
    {
        yield return new WaitForSeconds(5);
        GameManager.Instance.OnScenarioFail();
    }

    public float GetTime()
    {
        return hour;
    }

    public void ChangeTime(float timeAdd) {
        seconds += timeAdd;

        if(seconds < 0)
        {
            seconds = 86399;
        } else
        {
            seconds = seconds % 86400;
        }
        
        hour = seconds / 3600;
        
    }

    public void SuscribeTimeable(ITimeable timeable)
    {
        if (timeables.Contains(timeable))
            return;

        timeables.Add(timeable);
    }

    public void UnSubscribeTimeable(ITimeable timeable)
    {
        if (!timeables.Contains(timeable))
            return;

        timeables.Remove(timeable);
    }

    public string GetFormatedTime()
    {
        int secs = Mathf.RoundToInt(seconds);
        int hours = secs / 3600;
        int mins = (secs % 3600) / 60;
        secs = secs % 60;
        return string.Format("{0:D2}:{1:D2}", hours, mins);
    }
    
    public float GetTimeInHour()
    {
        hour = seconds / 3600;
        return hour;
    }

    public void TimeX3()
    {
        timeMultiplier = DefaultTimeMultiplier * 3;
    }

    public void ResetTime()
    {
        timeMultiplier = DefaultTimeMultiplier;
    }

}
