﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialColorTimeable : ITimeable
{
    private Material m_Material;

    [SerializeField]
    private string m_ColorName;

    // Start is called before the first frame update
    void Awake()
    {
        MeshRenderer mr = GetComponent<MeshRenderer>();
        m_Material = Instantiate(mr.material);
        mr.material = m_Material;
    }

    public override void UpdateValue(TimeData previous, TimeData next, float lerper)
    {
        m_Material.SetColor(m_ColorName, Color.Lerp(previous.colorValue, next.colorValue, lerper));
    }
}
