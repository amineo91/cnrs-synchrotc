﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SunTimeable : ITimeable
{
    public override void UpdateValue(TimeData previous, TimeData next, float lerper)
    {
        transform.rotation = Quaternion.Slerp(Quaternion.Euler(previous.vectorValue), Quaternion.Euler(next.vectorValue), lerper);
    }
}
