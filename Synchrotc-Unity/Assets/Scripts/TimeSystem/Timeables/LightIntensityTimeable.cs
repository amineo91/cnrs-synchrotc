﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightIntensityTimeable : ITimeable
{
    private Light m_Light;

    void Awake()
    {
        m_Light = GetComponent<Light>();
    }

    public override void UpdateValue(TimeData previous, TimeData next, float lerper)
    {
        m_Light.intensity = Mathf.Lerp(previous.floatValue, next.floatValue, lerper);
    }
}
