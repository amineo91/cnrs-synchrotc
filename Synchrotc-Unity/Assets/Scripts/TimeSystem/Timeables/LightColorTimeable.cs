﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightColorTimeable : ITimeable
{
    private Light m_Light;

    private void Awake()
    {
        m_Light = GetComponent<Light>();
    }

    public override void UpdateValue(TimeData previous, TimeData next, float lerper)
    {
        m_Light.color = Color.Lerp(previous.colorValue, next.colorValue, lerper);
    }
}