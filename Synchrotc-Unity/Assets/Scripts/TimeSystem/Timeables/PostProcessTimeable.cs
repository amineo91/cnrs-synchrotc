﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PostProcessTimeable : ITimeable
{
    void Awake()
    {
        foreach(TimeData td in m_Datas)
        {
            td.volume.weight = 0f;
        }
    }

    public override void UpdateValue(TimeData previous, TimeData next, float lerper)
    {
        previous.volume.weight = 1 - lerper;
        next.volume.weight = lerper;
    }
}
