﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialTimeable : ITimeable
{
    [SerializeField]
    private Material m_Material;

    [SerializeField]
    private string m_ColorName;

    public override void UpdateValue(TimeData previous, TimeData next, float lerper)
    {
        Color lastColor = m_Material.GetColor(m_ColorName);
        m_Material.SetColor(m_ColorName, new Color(lastColor.r, lastColor.g, lastColor.b, Mathf.Lerp(previous.floatValue, next.floatValue, lerper)));
    }
}
