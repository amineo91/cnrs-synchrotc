﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public abstract class ITimeable : MonoBehaviour
{
    [SerializeField]
    protected TimeData[] m_Datas;

    [System.Serializable]
    public class TimeData
    {
        public float time;
        public Vector3 vectorValue;
        public float floatValue;
        public bool boolValue;
        [ColorUsage(true, true)]
        public Color colorValue;
        public PostProcessVolume volume;
    }

    public virtual void OnTimeUpdate(float time)
    {
        if (time < m_Datas[0].time)
        {
            //EXEMPLE : time = 2, last data time = 22, first data time = 5 SO ===> lerp between 0 et (24 - 22 + 5) => 7. With time += 24 - 22;
            UpdateValue(m_Datas[m_Datas.Length - 1], m_Datas[0], Mathf.InverseLerp(0, (24f - m_Datas[m_Datas.Length - 1].time + m_Datas[0].time), time + (24f - m_Datas[m_Datas.Length - 1].time)));
            return;
        }

        for (int i = 0; i < m_Datas.Length; i++)
        {
            if (i == m_Datas.Length - 1)
            {
                if (m_Datas[i].time > m_Datas[0].time && m_Datas[i].time < time)
                {
                    UpdateValue(m_Datas[i], m_Datas[0], Mathf.InverseLerp(m_Datas[i].time, m_Datas[0].time + 24f, time));
                    return;
                }
            }
            if (m_Datas[i].time <= time && m_Datas[i + 1].time > time)
            {
                UpdateValue(m_Datas[i], m_Datas[(i + 1) % m_Datas.Length], Mathf.InverseLerp(m_Datas[i].time, m_Datas[i + 1].time, time));
                return;
            }
        }
    }

    public abstract void UpdateValue(TimeData previous, TimeData next, float lerper);

    void Start()
    {
        TimeManager.Instance.SuscribeTimeable(this);
    }

    void OnDestroy()
    {
        TimeManager.Instance.UnSubscribeTimeable(this);
    }
}
