﻿using System.Collections;
using UnityEngine;

public class AFKManager : MonoBehaviour
{
    static public AFKManager Instance;

    [SerializeField]
    private float DurationBeforeAFK = 30;

    [SerializeField]
    private float DurationBeforeEnding = 3600;

    [SerializeField]
    private GameObject AFKFeedback;

    private bool isAFK = false;
    private bool isEnded = false;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        
    }

    void Update()
    {
        if (GameManager.Instance.isGameStarted)
        {
            if (Input.anyKeyDown || Input.anyKey || GameManager.Instance.IsPaused)
            {
                if (isAFK)
                {
                    ResumeFromAFK();
                }

                StopAllCoroutines();
                StartCoroutines();
            }
        }
    }

    public void StartCoroutines()
    {
        StartCoroutine(WaitBeforeAFK());
        StartCoroutine(WaitBeforeEndingScenario());
    }

    public IEnumerator WaitBeforeAFK()
    {
        yield return new WaitForSeconds(DurationBeforeAFK);
        GoAFK();
    }  

    public IEnumerator WaitBeforeEndingScenario()
    {
        yield return new WaitForSeconds(DurationBeforeEnding);
        GoEnded();
    }

    private void ResumeFromAFK()
    {
        isAFK = false;
        AFKFeedback.GetComponent<UIMover>().Hide();
        AFKFeedback.GetComponent<Fader>().Hide();
    }

    private void GoAFK()
    {
        AFKFeedback.GetComponent<UIMover>().Show();
        AFKFeedback.GetComponent<Fader>().Show();
        isAFK = true;
        StatManager.Instance.nbErrorAFKCurrentObj++;
    }

    private void GoEnded()
    {
        StatManager.Instance.cause = "une heure d'inactivité";
        NotificationManager.Instance.Notify("Vous êtes inactif depuis plus d'une heure !", NotificationType.Error);
        TrackingManager.Instance.TakeHiResShot();
        StartCoroutine(TimedFail());
        isEnded = true;
    }

    private IEnumerator TimedFail() 
    {
        yield return new WaitForSeconds(2);
        GameManager.Instance.OnScenarioFail();
    }
}
