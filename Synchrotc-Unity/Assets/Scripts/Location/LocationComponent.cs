﻿using UnityEngine;

public class LocationComponent : MonoBehaviour
{
    [SerializeField]
    private Location m_Location;

    public Location Location { get { return m_Location; } }
}
