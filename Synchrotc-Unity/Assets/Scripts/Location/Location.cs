﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "New Location", menuName = "Locations/Create New Location")]
public class Location : SerializedScriptableObject
{
    public string LocationName;
    public string GoToInstruction;

    public Dictionary<Location, int> m_Exits = new Dictionary<Location, int>();
}
