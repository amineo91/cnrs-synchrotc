﻿using UnityEngine;

public class DebugDBService : MonoBehaviour
{
    [SerializeField]
    public bool m_IsLocal = true;

    [SerializeField]
    private string m_LocalURLPrefix, m_RemoteURLPrefix;

    void Start()
    {
        DBService.URL_ROOT = m_IsLocal ? m_LocalURLPrefix : m_RemoteURLPrefix;
    }

}
