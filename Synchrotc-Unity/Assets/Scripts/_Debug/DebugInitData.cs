﻿using UnityEngine;

public class DebugInitData : MonoBehaviour
{
    public bool AutoStart = true;
    public string Date = "2019-08-08";
    public int PatientId = 38;
    public DifficultyMode Difficulty = DifficultyMode.Easy;

#if UNITY_EDITOR
    void Awake()
    {
        InitData.Instance.AutoStart = AutoStart;
        InitData.Instance.Date = Date;
        InitData.Instance.PatientId = PatientId;
        InitData.Instance.Difficulty = Difficulty;
    }
#endif
}
