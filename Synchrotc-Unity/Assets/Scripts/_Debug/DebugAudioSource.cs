﻿using UnityEngine;

public class DebugAudioSource : MonoBehaviour
{
    [SerializeField]
    private AudioSource m_AudioSource;

    [SerializeField]
    private bool m_IsMuted = true;

#if UNITY_EDITOR
    void Start()
    {
        m_AudioSource.mute = m_IsMuted;
    }
#endif
}
