﻿using Keyveo.Scenarisation;
using UnityEngine;

public class DebugScenarioManager : MonoBehaviour
{
#if UNITY_EDITOR
    void Update()
    {
        if (GameManager.Instance.IsPaused == false)
        {
            if (Input.GetKeyUp(KeyCode.P))
            {
                ScenarioManager.Instance.StartScenario();
            }
            if (Input.GetKeyUp(KeyCode.N))
            {
                ScenarioManager.Instance.m_currentStep.Succeed();
            }
            if (Input.GetKeyUp(KeyCode.B))
            {
                ScenarioManager.Instance.m_currentStep.Fail();
            }
            if (Input.GetKeyUp(KeyCode.V))
            {
                SkipManager.Instance.SkipCurrentStep();
            }
        }
    }
#endif
}
