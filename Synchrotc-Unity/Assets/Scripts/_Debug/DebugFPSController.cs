﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

/// <summary>
/// Fabrice: Made FirstPersonController attributes public
/// </summary>
public class DebugFPSController : MonoBehaviour
{
#if UNITY_EDITOR
    [SerializeField]
    private int m_InitialNbIncrements = 5;

    /// <summary>
    /// Walk Speed is increased by this amount. Run Speed by twice this amount.
    /// </summary>
    [SerializeField]
    private float m_SpeedIncrement = 1f;

    [SerializeField]
    private KeyCode m_SpeedUpKey;

    [SerializeField]
    private KeyCode m_SpeedDownKey;

    [SerializeField]
    private FirstPersonController m_FirstPersonController;

    private float initialWalkSpeed;
    private float initialRunSpeed;

    private void Start()
    {
        initialWalkSpeed = m_FirstPersonController.m_WalkSpeed;
        initialRunSpeed = m_FirstPersonController.m_RunSpeed;
        updateSpeed(m_InitialNbIncrements);
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.IsPaused == false)
        {
            // Speed Up
            if (Input.GetKeyDown(m_SpeedUpKey))
            {
                updateSpeed(1);
                return;
            }
            // Speed Down
            if (Input.GetKeyDown(m_SpeedDownKey))
            {
                updateSpeed(-1);
                return;
            }
        }
    }

    void updateSpeed(int nbIncrement)
    {
        m_FirstPersonController.m_WalkSpeed += m_SpeedIncrement * nbIncrement;
        m_FirstPersonController.m_RunSpeed += m_SpeedIncrement * 2 * nbIncrement;

        // Ensure speed is positive
        m_FirstPersonController.m_WalkSpeed = Mathf.Max(m_FirstPersonController.m_WalkSpeed, initialWalkSpeed);
        m_FirstPersonController.m_RunSpeed = Mathf.Max(m_FirstPersonController.m_RunSpeed, initialRunSpeed);
    }
#endif
}
