﻿using UnityEngine;

public class DebugLogin : MonoBehaviour
{
    [SerializeField]
    private string m_Login, m_Password;

    [SerializeField]
    private Login m_LoginComponent;

    void Start()
    {
        m_LoginComponent.InputLogin.text = m_Login;
        m_LoginComponent.InputPassword.text = m_Password;
    }

}
