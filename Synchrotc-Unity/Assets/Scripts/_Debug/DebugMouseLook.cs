﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class DebugMouseLook : MonoBehaviour
{
    [SerializeField]
    private bool m_ForceCursorToFreeAndVisible = true;

#if UNITY_EDITOR
    void Start()
    {
        MouseLook.forceCursorToFreeAndVisible = m_ForceCursorToFreeAndVisible;
    }
#endif
}
