﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Teleporter : SerializedMonoBehaviour
{
    private static Teleporter instance;

    public static Teleporter Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<Teleporter>();
            }
            return instance;
        }
    }

    [SerializeField]
    [OdinSerialize]
    private Dictionary<KeyCode, Location> m_Keybinds = new Dictionary<KeyCode, Location>();

    [SerializeField]
    [OdinSerialize]
    private Dictionary<Location, Transform> m_Teleporters = new Dictionary<Location, Transform>();
    
    public void GoTo(Location location)
    {
        FirstPersonController.Instance.RotateTo(m_Teleporters[location]);
    }

#if UNITY_EDITOR
    // Update is called once per frame
    void Update()
    {
        if (GameManager.Instance.IsPaused == false)
        {
            foreach (var key in m_Keybinds.Keys)
            {
                if (Input.GetKeyDown(key))
                {
                    Location location = m_Keybinds[key];
                    GoTo(location);
                    return;
                }
            }
        }
    }
#endif
}
