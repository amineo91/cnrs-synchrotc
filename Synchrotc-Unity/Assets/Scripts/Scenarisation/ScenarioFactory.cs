﻿using Keyveo.Scenarisation;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ScenarioFactory: Object
{
    // Static attributes
    static string SCENARIOS_FOLDER_PATH = "Assets/Resources/Scriptables/Scenarios";
    static string SCENARIOS_REFERENCES_FOLDER_NAME = "_References";
    static string SCENARIOS_REFERENCES_FOLDER_PATH = SCENARIOS_FOLDER_PATH + "/" + SCENARIOS_REFERENCES_FOLDER_NAME;

    // Parameters
    bool isEditor;
    DifficultyMode difficulty;

    // Persistent attributes (set once and for all)
    bool isInit = false;
    ScenarioSO referenceScenarioSO;
    ObjectiveSO referenceObjectiveSO;
    StepSO referenceStepSO;
    Transform scenariosRoot;
    GiveItemScenarisationTarget giveItemScenarisationTarget;
    InventoryScenarisationTarget inventoryScenarisationTarget;
    LocationScenarisationTarget locationScenarisationTarget;
    FlouzScenarisationTarget flouzScenarisationTarget;

    // Call specific attributes (reset every call)
    ScenarioDB scenarioDB;
    int idDay;
    string folderPath;
    int idObjective;
    int idStep;
    int nbLogError = 0;
    int nbLogWarning = 0;

    // Last scenarisation occurences
    // Used to ease scenarisation method calls
    public ScenarioSO scenarioSO;
    ObjectiveSO objectiveSO;
    StepSO stepSO;
    GameObject scenarioObject;
    GameObject objectiveObject;
    Location fromLocation;

    #region Starters

    public IEnumerator Create(ScenarioDB scenarioDB, DifficultyMode difficulty, bool isEditor = true)
    {
        // Store parameters
        this.scenarioDB = scenarioDB;
        this.idDay = scenarioDB.idDay;
        this.difficulty = difficulty;
        this.isEditor = isEditor;

        // Check if factory is already initialised
        if (isEditor || isInit == false)
        {
            Init();
            isInit = nbLogError == 0 && nbLogWarning == 0;
        }

        if (isInit)
        {
            // Reset call specific attributes
            Reset();

            // Create scenario, objective and steps
            yield return CreateScenarioFromDB(scenarioDB);

            DebugLogStatus("Creation");
        }
        else
        {
            // Abort
            DebugLogStatus("Initialisation");
        }
    }

    void Init()
    {
        // Load SO references
        referenceScenarioSO = ScriptableObjectService.getScenarioSOByName(SCENARIOS_REFERENCES_FOLDER_NAME + "/_RefScenario");
        referenceObjectiveSO = ScriptableObjectService.getObjectiveSOByName(SCENARIOS_REFERENCES_FOLDER_NAME + "/_RefObjective");
        referenceStepSO = ScriptableObjectService.getStepSOByName(SCENARIOS_REFERENCES_FOLDER_NAME + "/_RefStep");
        if (referenceScenarioSO == null || referenceObjectiveSO == null || referenceStepSO == null)
        {
            DebugLogError("Reference scriptable objects not found.");
        }

        // Find scenarios root object
        scenariosRoot = Utils.FindObject("Scenarios").transform;
        if (scenariosRoot == null)
        {
            DebugLogError("Scenarios root not found.");
        }

        // Find scenarisation targets
        inventoryScenarisationTarget = FindObjectOfType<InventoryScenarisationTarget>();
        if (inventoryScenarisationTarget == null)
        {
            DebugLogError("Inventory Scenarisation Target not found.");
        }
        giveItemScenarisationTarget = FindObjectOfType<GiveItemScenarisationTarget>();
        if (giveItemScenarisationTarget == null)
        {
            DebugLogError("Give Item Scenarisation Target not found.");
        }
        locationScenarisationTarget = FindObjectOfType<LocationScenarisationTarget>();
        if (locationScenarisationTarget == null)
        {
            DebugLogError("Location Scenarisation Target not found.");
        }
        flouzScenarisationTarget = FindObjectOfType<FlouzScenarisationTarget>();
        if (flouzScenarisationTarget == null)
        {
            DebugLogError("Flouz Scenarisation Target not found.");
        }
    }

    void Reset()
    {
        // Reset scenarisation ids (starts at 1)
        idObjective = -1; // Currently only one objective created per call
        idStep = 1;

        scenarioSO = null;
        fromLocation = ScriptableObjectService.getLocationByName("Appartment");
    }

    #endregion

    IEnumerator CreateScenarioFromDB(ScenarioDB scenarioDB)
    {
        List<ObjectiveDB> objectivesDB = scenarioDB.objectivesDB;

        // Format scenario name
        string scenarioName = "SC - Day " + idDay + " " + difficulty + (isEditor ? " Editor" : "");
        folderPath = SCENARIOS_FOLDER_PATH + "/" + scenarioName;

        // Clean folder asset
#if UNITY_EDITOR
        if (isEditor)
        {
            AssetDatabase.DeleteAsset(folderPath);
            AssetDatabase.CreateFolder(SCENARIOS_FOLDER_PATH, scenarioName);
            AssetDatabase.SaveAssets();
        }
#endif

        // Clean hierarchy
        List<GameObject> oldScenarioObjects = new List<GameObject>();
        foreach (Transform oldScenarioTransform in scenariosRoot)
        {
            if (oldScenarioTransform.name == scenarioName)
            {
                oldScenarioObjects.Add(oldScenarioTransform.gameObject);
            }
        }
        foreach (GameObject oldScenarioObject in oldScenarioObjects)
        {
            DestroyImmediate(oldScenarioObject);
        }
        
        // Create scenario scriptable object and gameobject
        CreateScenarioSOGO(scenarioName);

        // Set starting money in wallet
        float startingMoney = 0;
        switch (difficulty)
        {
            case DifficultyMode.Easy:
                startingMoney = 1000;
                break;
            case DifficultyMode.Medium:
                startingMoney = 50;
                break;
            case DifficultyMode.Hard:
                startingMoney = 0;
                break;
        }
        FlouzManager.Instance.SetMoney(startingMoney);

        // Compute the cumulated price of all items to pay for
        float totalMoneyToPayAllItems = 0;
        List<Item> itemsToPay = new List<Item>();
        itemsToPay.AddRange(scenarioDB.getItemsToHaveFromOutdoor());
        itemsToPay.AddRange(scenarioDB.getItemsToGive());
        foreach (Item item in itemsToPay)
        {
            //Debug.Log(item.name + item.Price + "test");
            totalMoneyToPayAllItems += item.Price;
        }

        // List all items to get from appartment
        List<Item> itemsToHaveFromAppartment = scenarioDB.getItemsToHaveFromAppartment();
        if (totalMoneyToPayAllItems > 0)
        {
            itemsToHaveFromAppartment.Add(ScriptableObjectService.getItemByName("Portefeuille"));
        }

        if (difficulty <= DifficultyMode.Easy)
        {
            // If an item is required from appartement then...
            if (itemsToHaveFromAppartment.Count > 0)
            {
                // Add objective - Gather items from appartement
                CreateObjective("Récuperez vos affaires", fromLocation.LocationName);

                // Add step - Gather item from appartement
                string instruction = getInstructionsForItemsToGather(itemsToHaveFromAppartment);
                CreateStepSO(instruction);
                CreateInventoryStepObject(itemsToHaveFromAppartment, idObjective.ToString());
            }
        }

        // If, at the time when the scenario is created, the player doesn't have enough money to buy all items he has to then...
        if (totalMoneyToPayAllItems > FlouzManager.Instance.GetMoney())
        {
            // Add objective - Withdraw money from DAB
            CreateObjective("Retirer de l'argent", "Distributeur");

            if (DifficultyService.HasLocationStep())
            {
                // Add step - Go to DAB
                CreateLocationStep("Distributeur");
            }

            // Add step - Withdraw money
            CreateStepSO("Retirer de l'argent.");
            CreateFlouzStepObject(totalMoneyToPayAllItems);
        }

        foreach (ObjectiveDB objectiveDB in objectivesDB)
        {
            CreateObjective(objectiveDB);

            if (DifficultyService.HasLocationStep())
            {
                // Add step - Go to target location
                CreateLocationStep(objectiveDB.location);
            }

            // Add step - Give items
            // Give item first (ex: Rambo 1)
            List<Item> itemsToGive = objectiveDB.getItemsToGive();
            if (itemsToGive.Count > 0)
            {
                string instruction = getInstructionsForItemsToGive(itemsToGive);
                CreateStepSO(instruction);
                CreateGiveItemsStepObject(itemsToGive, objectiveDB.id);
            }

            // Add step - Gather items from outdoor
            // Get item next (ex: Rambo 2)
            List<Item> itemsToHaveFromOutdoor = objectiveDB.getItemsToHaveFromOutdoor();
            if (itemsToHaveFromOutdoor.Count > 0)
            {
                string instruction = getInstructionsForItemsToGather(itemsToHaveFromOutdoor);
                CreateStepSO(instruction);
                CreateInventoryStepObject(itemsToHaveFromOutdoor, objectiveDB.id);
            }
        }

        if (DifficultyService.HasChoiceMinimapWindow())
        {
            NavigationGuidanceManager.Instance.choiceMinimapWindow.SetActive(true);
        }
        /*
        }
        else // Hard mode à revoir si jamais on le bouge ou pas, voir si possible step/obj sur lesquels ce ratacher pour les errreurs.
        {
            // Add objective - Do all tasks in any order
            string location = "";
            for (int i = 0; i < objectivesDB.Count; i++)
            {
                location += (i != 0 ? "_" : "") + objectivesDB[i].location;
            }
            CreateObjective("Faire toutes les tâches dans n'importe quel ordre", location);

            // Add step - Do all tasks in any order
            List<Item> itemsToHaveFromOutdoor = scenarioDB.getItemsToHaveFromOutdoor();
            string instruction = getInstructionsForItemsToGather(itemsToHaveFromOutdoor);
            CreateStepSO(instruction);
            CreateInventoryStepObject(itemsToHaveFromOutdoor);
            NavigationGuidanceManager.Instance.choiceMinimapWindow.SetActive(true);

            // TODO
            //Debug.LogError("Mettre à jour le hard mode avec les objets à donner et les objets dans l'appartement et dehors");
        }
        */

        // For all difficulties
        // Add objective - Go back to your appartement
        CreateObjective("Retournez à votre appartement", "Appartment");
        CreateLocationStep("Appartment", true);
        
        yield return null;
    }

    #region Scenario

    void CreateScenarioSOGO(string scenarioName)
    {
        CreateScenarioSO(scenarioName);
        CreateScenarioGO();
    }

    ScenarioSO CreateScenarioSO(string scenarioName)
    {
        return CreateScenarioSO(scenarioName, folderPath);
    }

    ScenarioSO CreateScenarioSO(string scenarioName, string folderPath)
    {
        scenarioSO = Instantiate(referenceScenarioSO);
        scenarioSO.name = scenarioName;
        scenarioSO.m_successMessage = "Scénario réussi!";
#if UNITY_EDITOR
        if (isEditor)
        {
            AssetDatabase.CreateAsset(scenarioSO, folderPath + "/" + scenarioSO.name + ".asset");
            AssetDatabase.SaveAssets();
        }
#endif
        return scenarioSO;
    }

    GameObject CreateScenarioGO()
    {
        return CreateScenarioGO(scenarioSO, scenariosRoot);
    }

    GameObject CreateScenarioGO(ScenarioSO scenarioSO, Transform parent)
    {
        // GameObject
        scenarioObject = new GameObject();
        scenarioObject.name = scenarioSO.name;
        scenarioObject.transform.parent = parent;
        return scenarioObject;
    }

    #endregion

    #region Objective

    void CreateObjective(ObjectiveDB objectiveDB)
    {
        CreateObjectiveSO(objectiveDB.description, objectiveDB.location, objectiveDB.id_missionAccount);
        CreateObjectiveObject();
    }

    void CreateObjective(string description, string location)
    {
        CreateObjectiveSO(description, location);
        CreateObjectiveObject();
    }

    ObjectiveSO CreateObjectiveSO(string description, string location, string id_missionAccount = "-1")
    {
        return CreateObjectiveSO(idObjective++, scenarioSO, folderPath, description, location, id_missionAccount);
    }

    ObjectiveSO CreateObjectiveSO(int id, ScenarioSO scenarioSO, string folderPath, string description, string location, string missionAccountId)
    {
        idStep = 1;
        objectiveSO = Instantiate(referenceObjectiveSO);
        objectiveSO.m_description = description;
        objectiveSO.id_scenario = missionAccountId;
        objectiveSO.name = "O - " + location + "_" + objectiveSO.m_description.Replace(".", ""); // name used for file name, hence no dots
        scenarioSO.objectives.Add(objectiveSO);
#if UNITY_EDITOR
        if (isEditor)
        {
            AssetDatabase.CreateAsset(objectiveSO, folderPath + "/" + objectiveSO.name + ".asset"); // file name without dots
            AssetDatabase.SaveAssets();
        }
#endif
        return objectiveSO;
    }

    GameObject CreateObjectiveObject(bool isFirstSibling = false)
    {
        return CreateObjectiveObject(objectiveSO, scenarioObject.transform, isFirstSibling);
    }

    GameObject CreateObjectiveObject(ObjectiveSO objectiveSO, Transform parent, bool isFirstSibling)
    {
        // GameObject
        objectiveObject = new GameObject();
        objectiveObject.name = objectiveSO.name;
        objectiveObject.transform.parent = parent;
        ObjectiveComponent ObjComp = objectiveObject.AddComponent<ObjectiveComponent>();
        ObjComp.m_scenarisationObject = objectiveSO;
        //ObjComp
        if (objectiveSO.id_scenario != "-1")
        {
            ObjComp.m_callbacks.UponReaching.AddListener(delegate { StatManager.Instance.TimeBeginMission(objectiveSO.id_scenario); });
            ObjComp.m_callbacks.UponSucceeding.AddListener(delegate { StatManager.Instance.TimeEndMission(objectiveSO.id_scenario); });
        }

        if (isFirstSibling)
        {
            objectiveObject.transform.SetSiblingIndex(0);
        }

        return objectiveObject;
    }

    #endregion

    #region Step

    StepSO CreateStepSO(string description)
    {
        return CreateStepSO(description, idStep++, objectiveSO, folderPath);
    }

    StepSO CreateStepSO(string description, int id, ObjectiveSO objectiveSO, string folderPath)
    {
        stepSO = Instantiate(referenceStepSO);
        stepSO.name = "S" + id + " - " + description;
        stepSO.m_description = description; // DifficultyService.HasLocationStep() ? description : "";
        objectiveSO.steps.Add(stepSO);
#if UNITY_EDITOR
        if (isEditor)
        {
            AssetDatabase.CreateAsset(stepSO, folderPath + "/" + stepSO.name + ".asset");
            AssetDatabase.SaveAssets();
        }
#endif
        return stepSO;
    }

    // --- Location ---

    void CreateLocationStep(string locationName, bool isDisplayedInRecap = false)
    {
        CreateLocationStep(ScriptableObjectService.getLocationByName(locationName), isDisplayedInRecap);
    }

    void CreateLocationStep(Location targetLocation, bool isDisplayedInRecap = false)
    {
        CreateStepSO(targetLocation.GoToInstruction);
        CreateLocationStepObject(targetLocation, isDisplayedInRecap);
    }

    LocationStepComponent CreateLocationStepObject(string locationName, bool isDisplayedInRecap = false)
    {
        return CreateLocationStepObject(ScriptableObjectService.getLocationByName(locationName), isDisplayedInRecap);
    }

    LocationStepComponent CreateLocationStepObject(Location toLocation, bool isDisplayedInRecap = false)
    {
        return CreateLocationStepObject(toLocation, fromLocation, stepSO, objectiveObject.transform, isDisplayedInRecap);
    }

    LocationStepComponent CreateLocationStepObject(Location toLocation, Location fromLocation, StepSO stepSO, Transform parent, bool isDisplayedInRecap = false)
    {
        // GameObject
        GameObject stepObject = new GameObject();
        stepObject.name = stepSO.name;
        stepObject.transform.parent = parent;

        // StepComponent
        stepObject.SetActive(false); // Temporary desactivate the object to delay Awake method until all parameters are set
        LocationStepComponent stepComponent = stepObject.AddComponent<LocationStepComponent>();
        stepComponent.m_scenarisationObject = stepSO;
        stepComponent.m_targets.Add(locationScenarisationTarget);
        stepComponent.ToLocation = toLocation;
        stepComponent.FromLocation = fromLocation;
        stepComponent.isDisplayedInRecap = isDisplayedInRecap;
        stepObject.SetActive(true);

        // TODO - Get it at runtime
        this.fromLocation = stepComponent.ToLocation;

        return stepComponent;
    }

    // --- Inventory ---

    InventoryStepComponent CreateInventoryStepObject(Item itemToHave, string idObjectif = null)
    {
        return CreateInventoryStepObject(new List<Item>(new[] { itemToHave }), stepSO, objectiveObject.transform, idObjectif);
    }

    InventoryStepComponent CreateInventoryStepObject(List<Item> itemsToHave, string idObjectif = null)
    {
        return CreateInventoryStepObject(itemsToHave, stepSO, objectiveObject.transform, idObjectif);
    }

    InventoryStepComponent CreateInventoryStepObject(List<Item> itemsToHave, StepSO stepSO, Transform parent, string idObjectif = null)
    {
        // GameObject
        GameObject stepObject = new GameObject();
        stepObject.name = stepSO.name;
        stepObject.transform.parent = parent;

        parent.gameObject.AddComponent<dataObjectif>();

        if (idObjectif != null)
        {
            parent.GetComponent<dataObjectif>().idScenario = idObjectif;
        }

        // StepComponent
        stepObject.SetActive(false); // Temporary desactivate the object to delay Awake method until all parameters are set
        InventoryStepComponent stepComponent = stepObject.AddComponent<InventoryStepComponent>();
        stepComponent.m_scenarisationObject = stepSO;
        stepComponent.m_targets.Add(inventoryScenarisationTarget);
        stepComponent.ItemsToHave = itemsToHave;
        stepObject.SetActive(true);

        return stepComponent;
    }

    // --- Give item ---

    GiveItemStepComponent CreateGiveItemStepObject(Item item, string idObjectif = null)
    {
        return CreateGiveItemsStepObject(new List<Item>(new[] { item }), stepSO, objectiveObject.transform, idObjectif);
    }

    GiveItemStepComponent CreateGiveItemsStepObject(List<Item> items, string idObjectif = null)
    {
        return CreateGiveItemsStepObject(items, stepSO, objectiveObject.transform, idObjectif);
    }

    GiveItemStepComponent CreateGiveItemsStepObject(List<Item> items, StepSO stepSO, Transform parent, string idObjectif = null)
    {
        // GameObject
        GameObject stepObject = new GameObject();
        stepObject.name = stepSO.name;
        stepObject.transform.parent = parent;

        parent.gameObject.AddComponent<dataObjectif>();

        if (idObjectif != null)
        {
            parent.GetComponent<dataObjectif>().idScenario = idObjectif;
        }

        // StepComponent
        stepObject.SetActive(false); // Temporary desactivate the object to delay Awake method until all parameters are set
        GiveItemStepComponent stepComponent = stepObject.AddComponent<GiveItemStepComponent>();
        stepComponent.m_scenarisationObject = stepSO;
        stepComponent.m_targets.Add(giveItemScenarisationTarget);
        stepComponent.items = items;
        stepObject.SetActive(true);

        return stepComponent;
    }

    // --- Flouz ---

    FlouzStepComponent CreateFlouzStepObject(float moneyToHave)
    {
        return CreateFlouzStepObject(moneyToHave, stepSO, objectiveObject.transform);
    }

    FlouzStepComponent CreateFlouzStepObject(float moneyToHave, StepSO stepSO, Transform parent)
    {
        // GameObject
        GameObject stepObject = new GameObject();
        stepObject.name = stepSO.name;
        stepObject.transform.parent = parent;

        // StepComponent
        stepObject.SetActive(false); // Temporary desactivate the object to delay Awake method until all parameters are set
        FlouzStepComponent stepComponent = stepObject.AddComponent<FlouzStepComponent>();
        stepComponent.m_scenarisationObject = stepSO;
        stepComponent.m_targets.Add(flouzScenarisationTarget);
        stepComponent.MoneyToHave = moneyToHave;
        stepObject.SetActive(true);

        return stepComponent;
    }


    #endregion

    #region Services

    public static string getInstructionsForItemToGather(Item item)
    {
        return getInstructionsForItemsToGather(new List<Item>(new[] { item }));
    }

    public static string getInstructionsForItemsToGather(List<Item> items)
    {
        return getInstructionsForItems(items, "Obtenir");
    }

    public static string getInstructionsForItemToGive(Item item)
    {
        return getInstructionsForItemsToGive(new List<Item>(new[] { item }));
    }

    public static string getInstructionsForItemsToGive(List<Item> items)
    {
        return getInstructionsForItems(items, "Remettre");
    }

    public static string getInstructionsForItems(List<Item> items, string actionName)
    {
        if (items.Count < 1)
        {
            return "";
        }

        string instruction = actionName + " " + getItemInstruction(items[0]);
        for (int i = 1; i < items.Count; i++)
        {
            instruction += ((i == items.Count - 1) ? " et " : ", ") + getItemInstruction(items[i]);
        }
        instruction += ".";
        return instruction;
    }

    public static string getItemInstruction(Item item)
    {
        // Remove price from wallet
        string itemName = item.ItemName.Split(new string[] { " :" }, System.StringSplitOptions.RemoveEmptyEntries)[0];
        return item.Article + " " + itemName.ToLower();
    }

    #endregion

    #region Logs

    private void DebugLog(string message)
    {
        //Debug.Log("[SCENARISATION FACTORY] " + message);
    }

    private void DebugLogError(string message)
    {
        //Debug.LogError("[SCENARISATION FACTORY] " + message);
        nbLogError++;
    }

    private void DebugLogWarning(string message)
    {
        //Debug.LogWarning("[SCENARISATION FACTORY] " + message);
        nbLogWarning++;
    }

    private void DebugLogStatus(string step)
    {
        if (nbLogError > 0)
        {
            DebugLogError(step + (scenarioSO != null && scenarioSO.name != null ? " of '" + scenarioSO.name + "'" : "") +" failed: " + nbLogError + " error" + (nbLogError > 1 ? "s" : "") + ".");
        }
        else if (nbLogWarning > 0)
        {
            DebugLogWarning(step + (scenarioSO != null && scenarioSO.name != null ? " of '" + scenarioSO.name + "'" : "") + " compromised: " + nbLogWarning + " warning" + (nbLogWarning > 1 ? "s" : "") + ".");
        }
        else
        {
            DebugLog(step + (scenarioSO != null && scenarioSO.name != null ? " of '" + scenarioSO.name + "'" : "") + " successfull.");
        }
    }

    #endregion
}
