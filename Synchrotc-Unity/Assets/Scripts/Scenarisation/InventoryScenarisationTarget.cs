﻿using Keyveo.Scenarisation;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/// <summary>
/// Check each frame if inventory contains all items required for specified step
/// Script is enabled for an inventory step and disabled otherwise.
/// TODO - Add listeners to Inventory upon adding/removing an item instead of the Update calls
/// </summary>
public class InventoryScenarisationTarget : ScenarisationTarget
{
    /// <summary>
    /// Set in Editor
    /// </summary>
    [SerializeField]
    private Inventory Inventory;

    private Dictionary<Toggle, Item> itemByToggle = new Dictionary<Toggle, Item>();
    private Dictionary<Toggle, Item> itemByToggleSmartphone = new Dictionary<Toggle, Item>();

    private void Update()
    {
        // If all items to have are in inventory
        if (Inventory.GetItems().ContainsAll((m_scenarisationComponent as InventoryStepComponent).ItemsToHave) && GameManager.Instance.m_FirstPersonController.enabled == true)
        {
            ErrorDBService.Instance.addErrorsToDB();
            m_scenarisationComponent.Validate();
        }
        else
        {
            foreach (Item item in (m_scenarisationComponent as InventoryStepComponent).ItemsToHave)
            {
                foreach (ScenarioManager.effectByObjectName itemEffect in ScenarioManager.Instance.effectByObjectsName)
                {
                    if (item.name == itemEffect.name && itemEffect.effect == "fail" && GameManager.Instance.m_FirstPersonController.enabled == true && Inventory.GetItems().Contains(item))
                    {
                        //ErrorDBService.Instance.addErrorToObjectif();
                        //ErrorDBService.Instance.addErrorsToDB();
                        StatManager.Instance.nbErrorSpeechCurrentObj++;
                        m_scenarisationComponent.Validate();
                    }
                }
            }
        }

        /*foreach (Item item in (m_scenarisationComponent as InventoryStepComponent).ItemsToHave)
        {
            foreach (ScenarioManager.effectByObjectName itemEffect in ScenarioManager.Instance.effectByObjectsName)
            {
                if (item.name == itemEffect.name && itemEffect.effect == "fail" && GameManager.Instance.m_FirstPersonController.enabled == true && Inventory.GetItems().Contains(item))
                {
                    ErrorDBService.Instance.addErrorToObjectif();
                    ErrorDBService.Instance.addErrorsToDB();
                    m_scenarisationComponent.Validate();
                }
            }
        }*/

        // TODO - Ugly: Should use parallel steps
        foreach (Toggle toggle in itemByToggle.Keys)
        {
            toggle.isOn = Inventory.GetItems().Contains(itemByToggle[toggle]);
        }
        foreach (Toggle toggle in itemByToggleSmartphone.Keys)
        {
            toggle.isOn = Inventory.GetItems().Contains(itemByToggleSmartphone[toggle]);
        }
    }

    // TODO - Ugly: Should use parallel steps
    public void AddToggle(Toggle toggle, Item item)
    {
        itemByToggle.Add(toggle, item);
    }

    // TODO - Ugly: Should use parallel steps
    public void AddToggleSmartphone(Toggle toggle, Item item)
    {
        itemByToggleSmartphone.Add(toggle, item);
    }
}
