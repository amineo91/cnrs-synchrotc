﻿using Keyveo.Scenarisation;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryStepComponent : StepComponent
{
    /// <summary>
    /// List of items to have in inventory to validate step
    /// </summary>
    public List<Item> ItemsToHave = new List<Item>();

    /// <summary>
    /// List of objects take able in the world matching list of items to have
    /// </summary>
    private List<TakeAble> takeAbles;

    private InventoryScenarisationTarget inventoryScenarisationTarget;

    public int nbError;

    protected override void Start()
    {
        base.Start();

        inventoryScenarisationTarget = m_targets[0] as InventoryScenarisationTarget;

        // Retreive world objects corresponding to items to have
        takeAbles = new List<TakeAble>();
        foreach (Item item in ItemsToHave)
        {
            List<TakeAble> takeAbles = UtilsObject.getTakeAblesByItem(item);
            if (takeAbles.Count > 1)
            {
                Debug.LogWarning("Several '" + item.ItemName + "' found. Only one occurence is expected in the scene. First one is used.");
                this.takeAbles.Add(takeAbles[0]);
            }
            else if (takeAbles.Count == 1)
            {
                this.takeAbles.Add(takeAbles[0]);
            }
        }

        m_callbacks.UponFirstReaching.AddListener((sc, duration) => OnFirstReaching());
        m_callbacks.UponSucceeding.AddListener((sc, duration) => OnSucceeding());
        m_callbacks.UponSkipping.AddListener((sc, duration) => OnSkipping());
    }

    private void OnFirstReaching()
    {
        inventoryScenarisationTarget.m_scenarisationComponent = this;
        inventoryScenarisationTarget.enabled = true;
        
        if (DifficultyService.HasWorldGuidances())
        {
            foreach (TakeAble takeAble in takeAbles)
            {
                takeAble.ShowUI();
            }
        }
    }

    private void OnSucceeding()
    {
        inventoryScenarisationTarget.enabled = false;
    }

    private void OnSkipping()
    {
        // Remove takeAbles from scene and add items to inventory
        foreach (TakeAble takeAble in takeAbles)
        {
            takeAble.TakeIt();
        }
    }

    // TODO - Ugly: Should use parallel steps
    public void AddToggle(Toggle toggle, Item item)
    {
        inventoryScenarisationTarget.AddToggle(toggle, item);
    }
    public void AddToggleSmartphone(Toggle toggle, Item item)
    {
        inventoryScenarisationTarget.AddToggleSmartphone(toggle, item);
    }

    // --- Services ---

    //private List<TakeAble> getTakeAblesByItem(Item item)
    //{
    //    List<TakeAble> takeAbles = new List<TakeAble>(FindObjectsOfType<TakeAble>());
    //    return takeAbles.FindAll(takeAble => takeAble.getItem() == item);
    //}
}
