﻿using Keyveo.Scenarisation;

/// <summary>
/// Check each frame if wallet contains money required for specified step
/// Script is enabled for a flouz step and disabled otherwise.
/// TODO - Add listeners to FlouzManager upon adding/removing money instead of the Update calls
/// </summary>
public class FlouzScenarisationTarget : ScenarisationTarget
{
    private void Update()
    {
        // If all items to have are in inventory
        if (FlouzManager.Instance.GetMoney() >= (m_scenarisationComponent as FlouzStepComponent).MoneyToHave)
        {
            // Validate step
            m_scenarisationComponent.Validate();
        }
    }
}
