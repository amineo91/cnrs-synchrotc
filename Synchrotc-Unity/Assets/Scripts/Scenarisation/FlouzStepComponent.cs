﻿using Keyveo.Scenarisation;

public class FlouzStepComponent : StepComponent
{
    /// <summary>
    /// Player money to have in wallet to validate step
    /// </summary>
    public float MoneyToHave;

    private FlouzScenarisationTarget flouzScenarisationTarget;

    private void Awake()
    {
        flouzScenarisationTarget = m_targets[0] as FlouzScenarisationTarget;

        m_callbacks.UponFirstReaching.AddListener((sc, duration) => OnFirstReaching());
        m_callbacks.UponSucceeding.AddListener((sc, duration) => OnSucceeding());
        m_callbacks.UponSkipping.AddListener((sc, duration) => OnSkipping());
    }

    private void OnFirstReaching()
    {
        flouzScenarisationTarget.m_scenarisationComponent = this;
        flouzScenarisationTarget.enabled = true;
    }

    private void OnSucceeding()
    {
        flouzScenarisationTarget.enabled = false;
    }

    private void OnSkipping()
    {
        FlouzManager.Instance.TryAddMoney(MoneyToHave);
    }
}
