﻿using Keyveo.Scenarisation;
using System.Collections.Generic;
using UnityEngine;

public class LocationStepComponent : StepComponent
{
    /// <summary>
    /// Player target location to validate step
    /// </summary>
    public Location ToLocation;

    /// <summary>
    /// Player expected initial location
    /// Used for navigation guidance
    /// </summary>
    public Location FromLocation;

    public bool isDisplayedInRecap = false;

    /// <summary>
    /// Location component matching target location
    /// </summary>
    private LocationComponent LocationComponent;

    private LocationScenarisationTarget locationScenarisationTarget;

    private void Awake()
    {
        locationScenarisationTarget = m_targets[0] as LocationScenarisationTarget;

        // Retreive location component corresponding to target location
        LocationComponent = getLocationComponentByLocation(ToLocation);
        if (LocationComponent == null)
        {
            Debug.LogWarning("Location '" + ToLocation.LocationName + "' not found. Add one occurence to the scene.");
        }

        m_callbacks.UponFirstReaching.AddListener((sc, duration) => OnFirstReaching());
        m_callbacks.UponSucceeding.AddListener((sc, duration) => OnSucceeding());
        m_callbacks.UponSkipping.AddListener((sc, duration) => OnSkipping());
    }

    private void OnFirstReaching()
    {
        locationScenarisationTarget.m_scenarisationComponent = this;
        locationScenarisationTarget.enabled = true;

        NavigationGuidanceManager.Instance.Show(FromLocation, ToLocation);
    }

    private void OnSucceeding()
    {
        locationScenarisationTarget.enabled = false;
        NavigationGuidanceManager.Instance.HideAll();
    }

    private void OnSkipping()
    {
        locationScenarisationTarget.enabled = false;
        // TODO - Fix this to move using the location manager instead of debug feature
        //LocationManager.Instance.SwitchLocation(TargetLocation);
        Teleporter.Instance.GoTo(ToLocation);
    }


    // --- Services ---

    private LocationComponent getLocationComponentByLocation(Location location)
    {
        List<LocationComponent> locationComponents = new List<LocationComponent>(FindObjectsOfType<LocationComponent>());
        return locationComponents.Find(locationComponent => locationComponent.Location == location);
    }
}
