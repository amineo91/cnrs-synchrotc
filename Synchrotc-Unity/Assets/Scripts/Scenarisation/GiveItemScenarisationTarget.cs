﻿using Keyveo.Scenarisation;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

/// <summary>
/// Check each frame if inventory contains none of the specified items for this step
/// Script is enabled for an inventory step and disabled otherwise.
/// TODO - Add listeners to Inventory upon adding/removing an item instead of the Update calls
/// </summary>
public class GiveItemScenarisationTarget : ScenarisationTarget
{
    /// <summary>
    /// Set in Editor
    /// </summary>
    [SerializeField]
    private Inventory Inventory;

    private Dictionary<Toggle, Item> itemByToggle = new Dictionary<Toggle, Item>();
    private Dictionary<Toggle, Item> itemByToggleSmartphone = new Dictionary<Toggle, Item>();

    private List<Item> itemsToGive;

    protected override void Start()
    {
        base.Start();

        Inventory.Instance.OnObjectRemoved.AddListener(OnObjectRemoved);
    }

    public void Init()
    {
        itemsToGive = new List<Item>((m_scenarisationComponent as GiveItemStepComponent).items);
    }

    private void OnObjectRemoved(Item item)
    {
        itemsToGive.Remove(item);
    }

    private void Update()
    {
        if (GameManager.Instance.m_FirstPersonController.enabled)
        {
            // If all items are given
            if (itemsToGive.Count < 1)
            {
                ErrorDBService.Instance.addErrorsToDB();
                m_scenarisationComponent.Validate();
            }
        }

        // TODO - Ugly: Should use parallel steps
        foreach (Toggle toggle in itemByToggle.Keys)
        {
            toggle.isOn = Inventory.GetItems().Contains(itemByToggle[toggle]) == false;
        }
        foreach (Toggle toggle in itemByToggleSmartphone.Keys)
        {
            toggle.isOn = Inventory.GetItems().Contains(itemByToggleSmartphone[toggle]) == false;
        }
    }

    // TODO - Ugly: Should use parallel steps
    public void AddToggle(Toggle toggle, Item item)
    {
        itemByToggle.Add(toggle, item);
    }
    public void AddToggleSmartphone(Toggle toggle, Item item)
    {
        itemByToggleSmartphone.Add(toggle, item);
    }
}
