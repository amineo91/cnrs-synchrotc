﻿using Keyveo.Scenarisation;
using System;
using System.Collections;
using UnityEngine;

public class ScenarioFactoryAsync: MonoBehaviour
{
    public void LoadAndCreateAsync(int idDay, DifficultyMode difficulty, bool isEditor, Action<ScenarioSO> onComplete = null)
    {
        StartCoroutine(LoadAndCreate(idDay, difficulty, isEditor, onComplete));
    }

    IEnumerator LoadAndCreate(int idDay, DifficultyMode difficulty, bool isEditor, Action<ScenarioSO> onComplete = null)
    {
        yield return ScenarioDBService.LoadScenarioFromDB(idDay);
        ScenarioDB scenarioDB = ScenarioDBService.scenarioDB;

        if (scenarioDB.objectivesDB.Count > 0)
        {
            ScenarioFactory scenarioFactory = new ScenarioFactory();
            yield return scenarioFactory.Create(scenarioDB, difficulty, isEditor);
            if (onComplete != null)
            {
                onComplete(scenarioFactory.scenarioSO);
            }
        }
        else
        {
            //Debug.Log("No mission for day '" + idDay + "'");
        }
    }
}
