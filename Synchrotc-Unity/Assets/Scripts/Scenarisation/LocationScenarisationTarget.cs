﻿using Keyveo.Scenarisation;
using UnityEngine;

/// <summary>
/// Check each frame if player is at specified location
/// Script is enabled for a location step and disabled otherwise.
/// </summary>
public class LocationScenarisationTarget : ScenarisationTarget
{
    private void OnTriggerStay(Collider other)
    {
        // If player collides with a location trigger
        if (other.gameObject.layer == LayerMask.NameToLayer("Location"))
        {
            // Get the location associated to the trigger
            LocationComponent locationComponent = other.GetComponentInParent<LocationComponent>();

            // Update last known location for guidance
            NavigationGuidanceManager.Instance.lastKnownLocation = locationComponent.Location;

            // If this location is the target one
            if (enabled && locationComponent.Location == (m_scenarisationComponent as LocationStepComponent).ToLocation)
            {
                // Validate step
                m_scenarisationComponent.Validate();
            }
        }
    }
}
