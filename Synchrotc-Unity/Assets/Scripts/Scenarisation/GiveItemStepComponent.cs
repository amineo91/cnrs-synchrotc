﻿using Keyveo.Scenarisation;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GiveItemStepComponent : StepComponent
{
    /// <summary>
    /// List of items to not have in inventory to validate step
    /// </summary>
    public List<Item> items = new List<Item>();

    private GiveItemScenarisationTarget giveItemScenarisationTarget;

    public int nbError;

    protected override void Start()
    {
        base.Start();

        giveItemScenarisationTarget = m_targets[0] as GiveItemScenarisationTarget;

        m_callbacks.UponFirstReaching.AddListener((sc, duration) => OnFirstReaching());
        m_callbacks.UponSucceeding.AddListener((sc, duration) => OnSucceeding());
        m_callbacks.UponSkipping.AddListener((sc, duration) => OnSkipping());
    }

    private void OnFirstReaching()
    {
        giveItemScenarisationTarget.m_scenarisationComponent = this;
        giveItemScenarisationTarget.enabled = true;
        giveItemScenarisationTarget.Init();

        // TODO Show UI guidances ?
        //if (DifficultyService.HasWorldGuidances())
        //{
        //}
    }

    private void OnSucceeding()
    {
        giveItemScenarisationTarget.enabled = false;
    }

    private void OnSkipping()
    {
        // Remove items from inventory
        Inventory.Instance.RemoveObjects(items);
    }

    // TODO - Ugly: Should use parallel steps
    public void AddToggle(Toggle toggle, Item item)
    {
        giveItemScenarisationTarget.AddToggle(toggle, item);
    }
    public void AddToggleSmartphone(Toggle toggle, Item item)
    {
        giveItemScenarisationTarget.AddToggleSmartphone(toggle, item);
    }
}
