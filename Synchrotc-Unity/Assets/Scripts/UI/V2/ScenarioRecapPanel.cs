﻿using Keyveo.Scenarisation;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScenarioRecapPanel : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI DescriptionObjective;

    [SerializeField]
    private RectTransform TaskPanel;

    [SerializeField]
    private Toggle TaskPrefab;

    [SerializeField]
    private RectTransform TaskSmartphonePanel;

    [SerializeField]
    private Toggle TaskSmartphonePrefab;

    public void Clear()
    {
        List<GameObject> children = new List<GameObject>();
        foreach (Transform child in TaskPanel)
        {
            children.Add(child.gameObject);
        }
        foreach (Transform child in TaskSmartphonePanel)
        {
            children.Add(child.gameObject);
        }
        foreach (GameObject child in children)
        {
            Destroy(child);
        }
    }

    public void InitTasks()
    {
        StepSO stepSO1 = ScenarioManager.Instance.m_scenario.objectives[0].steps[0];
        Transform scenarioTransform = stepSO1.m_registeredComponent.transform.parent.parent;

        // TODO - Location retrieval is ugly
        List<LocationStepComponent> locationStepComponents = new List<LocationStepComponent>(scenarioTransform.GetComponentsInChildren<LocationStepComponent>());
        string location = "";
        if (locationStepComponents.Count > 1)
        {
            location = locationStepComponents[locationStepComponents.Count - 2].ToLocation.LocationName;
            location = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(location.ToLower());
        }
        DescriptionObjective.text = "8H30" + "\n" + location;

        List<StepComponent> stepComponents = new List<StepComponent>();
        stepComponents.AddRange(scenarioTransform.GetComponentsInChildren<StepComponent>());
        stepComponents = stepComponents.FindAll((stepComponent) =>
        {
            return stepComponent.GetType() == typeof(InventoryStepComponent) ||
                stepComponent.GetType() == typeof(GiveItemStepComponent) ||
                stepComponent.GetType() == typeof(LocationStepComponent) ||
                stepComponent.GetType() == typeof(FlouzStepComponent);
        });

        foreach (StepComponent stepComponent in stepComponents)
        {
            // TODO - Ugly: Should use parallel steps
            // List all items individually instead of a single long instruction
            if (stepComponent.GetType() == typeof(InventoryStepComponent) && (stepComponent as InventoryStepComponent).ItemsToHave.Count > 1)
            {
                foreach (Item item in (stepComponent as InventoryStepComponent).ItemsToHave)
                {
                    string instruction = ScenarioFactory.getInstructionsForItemToGather(item);
                    Toggle toggle = AddTask(instruction);
                    (stepComponent as InventoryStepComponent).AddToggle(toggle, item);
                    Toggle toggleSmartphone = AddTaskSmartphone(instruction);
                    (stepComponent as InventoryStepComponent).AddToggleSmartphone(toggleSmartphone, item);
                }
            }
            else if (stepComponent.GetType() == typeof(GiveItemStepComponent) && (stepComponent as GiveItemStepComponent).items.Count > 1)
            {
                foreach (Item item in (stepComponent as GiveItemStepComponent).items)
                {
                    string instruction = ScenarioFactory.getInstructionsForItemToGive(item);
                    Toggle toggle = AddTask(instruction);
                    (stepComponent as GiveItemStepComponent).AddToggle(toggle, item);
                    Toggle toggleSmartphone = AddTaskSmartphone(instruction);
                    (stepComponent as GiveItemStepComponent).AddToggleSmartphone(toggleSmartphone, item);
                }
            }
            else if (stepComponent.GetType() != typeof(LocationStepComponent) || (stepComponent as LocationStepComponent).isDisplayedInRecap)
            {
                Toggle toggle = AddTask(stepComponent.m_scenarisationObject.m_description);
                stepComponent.m_callbacks.UponSucceeding.AddListener((sc, duration) => toggle.isOn = true);
                Toggle toggleSmartphone = AddTaskSmartphone(stepComponent.m_scenarisationObject.m_description);
                stepComponent.m_callbacks.UponSucceeding.AddListener((sc, duration) => toggleSmartphone.isOn = true);
            }
        }
    }

    private Toggle AddTask(string description)
    {
        Toggle toggle = Instantiate(TaskPrefab, TaskPanel);
        toggle.GetComponentInChildren<TextMeshProUGUI>().text = description;
        return toggle;
    }

    private Toggle AddTaskSmartphone(string description)
    {
        Toggle toggle = Instantiate(TaskSmartphonePrefab, TaskSmartphonePanel);
        toggle.GetComponentInChildren<TextMeshProUGUI>().text = description;
        return toggle;
    }
}
