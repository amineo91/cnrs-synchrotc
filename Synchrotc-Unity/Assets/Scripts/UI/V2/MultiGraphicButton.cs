﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MultiGraphicButton : Button
{
    /// <summary>
    /// Extra graphic on which to apply color transition
    /// </summary>
    public List<Graphic> ExtraGraphics;

    /// <summary>
    /// Transition colors for extra graphic
    /// </summary>
    public ColorBlock ExtraColors;

    // Original color blocks used for reverting selection
    // ColorBlock is a struct
    private ColorBlock extraColorsOrg;
    private ColorBlock colorsOrg;

    protected override void Awake()
    {
        base.Awake();
        
        // ColorBlock is a struct => no need to instantiate anything
        colorsOrg = colors;
        extraColorsOrg = ExtraColors;
    }

    protected override void DoStateTransition(SelectionState state, bool instant)
    {
        base.DoStateTransition(state, instant);

        // Override this function to also perform a color transition for extra graphic depending on the current button state

        Color tintColor;

        switch (state)
        {
            case SelectionState.Normal:
                tintColor = ExtraColors.normalColor;
                break;
            case SelectionState.Highlighted:
                tintColor = ExtraColors.highlightedColor;
                break;
            case SelectionState.Pressed:
                tintColor = ExtraColors.pressedColor;
                break;
            case SelectionState.Disabled:
                tintColor = ExtraColors.disabledColor;
                break;
            default:
                tintColor = Color.black;
                break;
        }
        
        if (gameObject.activeInHierarchy)
        {
            switch (transition)
            {
                case Transition.ColorTint:
                    StartColorTween(tintColor * ExtraColors.colorMultiplier, instant);
                    break;
            }
        }
    }

    // Perform a color transition for extra graphic
    void StartColorTween(Color targetColor, bool instant)
    {
        if (ExtraGraphics == null)
            return;

        foreach(Graphic ExtraGraphic in ExtraGraphics)
        {
            ExtraGraphic.CrossFadeColor(targetColor, instant ? 0f : ExtraColors.fadeDuration, true, true);
        }
    }

    /// <summary>
    /// Make pressed color the default one for both graphics
    /// Button is thus irresponsive to color change
    /// </summary>
    public new void Select()
    {
        ExtraColors.normalColor = ExtraColors.pressedColor;
        ExtraColors.highlightedColor = ExtraColors.pressedColor;

        ColorBlock colorBlock = colors;
        colorBlock.normalColor = colorBlock.pressedColor;
        colorBlock.highlightedColor = colorBlock.pressedColor;
        colors = colorBlock;
    }

    /// <summary>
    /// Revert colors to orginal ones
    /// </summary>
    public void Deselect()
    {
        ExtraColors = extraColorsOrg;
        // colors setter automatically calls DoStateTransition
        // Thus updating both graphic colors
        colors = colorsOrg;
    }
}
