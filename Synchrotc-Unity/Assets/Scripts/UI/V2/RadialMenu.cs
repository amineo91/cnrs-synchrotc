﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using Sirenix.OdinInspector;
using UnityEngine.Events;

public class RadialMenu : MonoBehaviour
{
    [SerializeField]
    private RectTransform m_Selector;

    [SerializeField]
    private TextMeshProUGUI m_SelectedText;

    [SerializeField]
    private string[] m_SelectedTitles = new string[5];

    private int currentPart = -10;

    [SerializeField]
    private Transform m_Center;
    [SerializeField]
    private Transform m_Outer;

    [SerializeField]
    private UnityEvent[] m_OnClick = new UnityEvent[5];

    private bool displayed = false;

    public void Select(int part)
    {
        m_Selector.DOKill();
        m_Selector.DOLocalRotate(new Vector3(0f, 0f, currentPart * -72), 0.25f).SetEase(Ease.OutExpo);

        if(m_SelectedTitles.Length > part)
            m_SelectedText.text = m_SelectedTitles[part];
    }

    public void Update()
    {
        Vector2 centerToMouse = new Vector2(Input.mousePosition.x - (float)Screen.width / 2f, Input.mousePosition.y - (float)Screen.height / 2f).normalized;

        float angle = Mathf.Atan2(centerToMouse.x, centerToMouse.y) * Mathf.Rad2Deg;

        if(centerToMouse.x < 0)
        {
            angle = 360 - angle * -1;
        }

        int part = Mathf.FloorToInt(angle / 72f);

        if(part != currentPart)
        {
            currentPart = part;
            Select(part);
        }

        if (Input.GetKeyDown(KeyCode.Mouse0) && displayed)
        {
            if(m_OnClick.Length > currentPart)
            {
                if (m_OnClick[currentPart] != null)
                {
                    m_OnClick[currentPart].Invoke();
                }
            }
        }
    }

    [Button]
    public void Appear()
    {
        displayed = true;

        m_Center.localScale = Vector3.zero;
        m_Outer.localScale = Vector3.zero;

        m_Center.DOScale(1f, 0.75f).SetEase(Ease.OutBack);
        m_Outer.DOScale(1f, 0.75f).SetEase(Ease.OutExpo).SetDelay(0.15f);
    }

    [Button]
    public void Disappear()
    {
        displayed = false;

        m_Center.localScale = Vector3.one;
        m_Outer.localScale = Vector3.one;

        m_Center.DOScale(0f, 0.75f).SetEase(Ease.InBack).SetDelay(0.15f);
        m_Outer.DOScale(0f, 0.75f).SetEase(Ease.InExpo);
    }
}
