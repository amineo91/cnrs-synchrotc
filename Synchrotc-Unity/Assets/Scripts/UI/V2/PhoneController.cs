﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PhoneController : MonoBehaviour
{
    public static PhoneController Instance;

    [SerializeField] public GameObject containerHome;
    [SerializeField] public GameObject containerMap;
    [SerializeField] public GameObject containerNavigator;
    [SerializeField] public GameObject containerMissions;

    [SerializeField] public GameObject buttonTaskRecap;

    private List<GameObject> containers = new List<GameObject>();
    private GameObject previousContainer = null;

    void Awake()
    {
        //Check if instance already exists
        if (Instance == null)
            //if not, set instance to this
            Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        containers.Add(containerHome);
        containers.Add(containerMap);
        containers.Add(containerNavigator);
        containers.Add(containerMissions);
    }

    public void GoToContainer(GameObject containerToActivate)
    {
        foreach (GameObject container in containers)
        {
            container.SetActive(false);
        }

        containerToActivate.SetActive(true);

        if (containerToActivate == containerMap)
        {
            OnGoToMap();
        }

        if (containerToActivate == containerHome)
        {
            OnGoToHome();
        }

        previousContainer = containerToActivate;
    }

    private void OnGoToMap()
    {
        gameObject.GetComponent<RectTransform>().DOScale(new Vector3(1.7f,1.7f,1f),1.5f);

        if (DifficultyService.HasChoiceMinimapWindow())
        {
            NavigationGuidanceManager.Instance.choiceMinimapWindow.SetActive(true);
        }
    }

    private void OnGoToHome()
    {
        if (previousContainer == containerMap)
        {
            gameObject.GetComponent<RectTransform>().DOScale(new Vector3(1, 1, 1f), 1.5f);
        }
    }

    public void ReturnToHome()
    {
        GoToContainer(containerHome);
    }

    public void HideTaskRecapButton()
    {
        buttonTaskRecap.SetActive(false);
    }

}
