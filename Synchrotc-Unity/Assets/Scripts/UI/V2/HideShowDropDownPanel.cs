﻿using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HideShowDropDownPanel : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private bool isOpen = true;
    private bool isOpaque = false;

    [SerializeField]
    private RectTransform m_Content;

    [SerializeField]
    private TextMeshProUGUI m_HideShowButton;

    [SerializeField]
    private Graphic[] m_HoverUIElements = { };


    // Use this for initialization
    void Start()
    {
        Open();
    }

    void Update()
    {
        if (!isOpaque && !isOpen)
        {
            FadeOut();
        }
    }

    public void Toggle()
    {
        if (isOpen)
        {
            Close();
        }
        else
        {
            Open();
        }
    }

    public void Open(bool instant = false)
    {
        isOpen = true;
        m_Content.DOKill();
        m_Content.gameObject.SetActive(true);
        m_Content.DOLocalMoveY(0, instant ? 0f : 1f).SetEase(Ease.InOutCirc);
        m_HideShowButton.text = "-";
    }

    public void Close(bool instant = false)
    {
        isOpen = false;
        m_Content.DOKill();
        m_Content.DOLocalMoveY(m_Content.sizeDelta.y, instant ? 0f : 1f).SetEase(Ease.InOutCirc).OnComplete(delegate
            {
                m_Content.gameObject.SetActive(false);
            });
        m_HideShowButton.text = "+";
    }

    void FadeOut()
    {
        isOpaque = true;
        foreach (Graphic g in m_HoverUIElements)
        {
            g.DOKill();
            g.DOFade(1, 0.5f).SetEase(Ease.InOutCubic);
        }
    }

    void FadeIn()
    {
        isOpaque = false;
        foreach (Graphic g in m_HoverUIElements)
        {
            g.DOKill();
            g.DOFade(0, 0.5f).SetEase(Ease.InOutCubic);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        FadeOut();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        FadeIn();
    }
}
