﻿using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpeechBox : MonoBehaviour
{
    public List<Image> border;

    public void PulseOnce(Color color, float duration)
    {
        foreach (Image image in border)
        {
            image.DOColor(color, duration).SetEase(Ease.OutCubic).OnComplete(() => image.DORewind());
        }
    }
}
