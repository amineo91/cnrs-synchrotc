﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using DG.Tweening;

public class BottomMenu : MonoBehaviour {

	[SerializeField]
	private KeyCode m_InvokeKey;
	
	[SerializeField]
	private RectTransform m_NormalMenu;

	[SerializeField]
	private Transform m_ArrowButton;

	private bool isOpen = false;

	void Start(){
		isOpen = false;
		Close(true);
	}

	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(m_InvokeKey)){
			Toggle();
		}
	}

	[Button]
	public void Toggle(){
		if(isOpen){
			Close();
		} else {
			Open();
		}
	}

	public void Open(bool instant = false){
		isOpen = true;
		m_NormalMenu.DOKill();
		m_ArrowButton.DOKill();
		m_NormalMenu.DOAnchorPosY(0, instant ? 0f : 0.4f).SetEase(Ease.InOutCirc);
		m_ArrowButton.DOScaleY(-2, instant ? 0f : 0.4f).SetEase(Ease.InOutCirc);
	}

	public void Close(bool instant = false, float delay = 0){
        if (delay == 0)
        {
            CloseUndelayed(instant);
        }
        else if (isActiveAndEnabled)
        {
            StartCoroutine(CloseDelayed(instant, delay));
        }
    }

    private IEnumerator CloseDelayed(bool instant = false, float delay = 0)
    {
        yield return new WaitForSeconds(delay);

        CloseUndelayed(instant);
    }

    private void CloseUndelayed(bool instant = false)
    {
        isOpen = false;
        m_NormalMenu.DOKill();
        m_ArrowButton.DOKill();
        m_NormalMenu.DOAnchorPosY(-75, instant ? 0f : 1f).SetEase(Ease.InOutCirc);
        m_ArrowButton.DOScaleY(2, instant ? 0f : 1f).SetEase(Ease.InOutCirc);
    }
}
