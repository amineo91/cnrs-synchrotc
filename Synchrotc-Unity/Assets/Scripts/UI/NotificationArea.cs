﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine;

public class NotificationArea : MonoBehaviour
{
    [SerializeField]
    private float m_DefaultDuration = 3f;

    [SerializeField]
    private Color m_ErrorColor = Color.red;

    [SerializeField]
    private Notification m_NotificationPrefab;

    // Add prefab for success message with straw
    [SerializeField]
    private Notification m_NotificationStrawPrefab;
    
    [SerializeField]
    private Transform m_NotificationParent;

    [SerializeField]
    private bool m_HasMaxNbNotifications = true;

    [SerializeField]
    private int m_MaxNbNotifications = 1;

    void Awake()
    {
        // Start straw material animation
        // All straw notification will have synchronized animation (no material cloning)
        m_NotificationStrawPrefab.Border.materialForRendering.mainTextureOffset = Vector2.zero;
        m_NotificationStrawPrefab.Border.materialForRendering.DOOffset(new Vector2(1, 0), 1).SetLoops(-1);
    }

    public bool isNotificationAlreadyDisplayed(string message)
    {
        List<Notification> notifications = new List<Notification>(m_NotificationParent.GetComponentsInChildren<Notification>());
        return notifications.Exists(notification => notification.GetText() == message);
    }

    public void Add(string message, float duration, NotificationType notificationType)
    {
        if (duration < 0)
        {
            duration = m_DefaultDuration;
        }

        if (m_HasMaxNbNotifications)
        {
            // Remove oldest notifications
            while (m_NotificationParent.childCount >= Mathf.Max(m_MaxNbNotifications, 1))
            {
                Transform oldNotification = m_NotificationParent.GetChild(m_NotificationParent.childCount - 1);
                DestroyImmediate(oldNotification.gameObject);
            }
        }
        // add condition for check if the message need "straw" or not
        Notification notification = Instantiate(notificationType == NotificationType.Success ? m_NotificationStrawPrefab : m_NotificationPrefab, m_NotificationParent);
        if (notificationType == NotificationType.Error)
        {
            notification.SetColor(m_ErrorColor);
        }
        notification.UpdateText(message);
        notification.transform.SetAsFirstSibling();
        notification.InstantHide();
        notification.TemporarilyShowAsync(duration);
    }
}
