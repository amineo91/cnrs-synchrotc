﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public GameObject inventory;
    public GameObject smartphone;

    private bool isInventoryDisplayed;

    public bool isPhoneStatic;
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab) && !isInventoryDisplayed)
        {
            GameManager.Instance.FreezePlayer();
            isInventoryDisplayed = true;

            inventory.GetComponent<UIMover>().Toggle();
            inventory.GetComponent<Fader>().Toggle();

            if (!isPhoneStatic)
            {
                smartphone.GetComponent<UIMover>().Show();
                smartphone.GetComponent<Fader>().Show();
            }


        }
        else if (Input.GetKeyDown(KeyCode.Tab) && isInventoryDisplayed)
        {
            GameManager.Instance.UnfreezePlayer();
            isInventoryDisplayed = false;

            inventory.GetComponent<UIMover>().Toggle();
            inventory.GetComponent<Fader>().Toggle();

            if (!isPhoneStatic)
            {
                smartphone.GetComponent<UIMover>().Hide();
                smartphone.GetComponent<Fader>().Hide();
            }
        }
    }

    public void isPhoneStaticToggle()
    {
        if (isPhoneStatic)
        {
            isPhoneStatic = false;
        }
        else
        {
            isPhoneStatic = true;
        }
    }
}
