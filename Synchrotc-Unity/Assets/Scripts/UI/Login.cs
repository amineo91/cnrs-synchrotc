﻿using System.Collections;
using TMPro;
using UnityEngine;

public class Login : MonoBehaviour
{
    [SerializeField]
    private TMP_InputField m_InputLogin, m_InputPassword;

    public TMP_InputField InputLogin
    {
        get { return m_InputLogin; }
    }

    public TMP_InputField InputPassword
    {
        get { return m_InputPassword; }
    }

    [SerializeField]
    private TextMeshProUGUI m_Info;

    [SerializeField]
    private Fader m_Fader;

    [SerializeField]
    private UIMover m_UIMover;

    public void z_Summit()
    {
        StartCoroutine(Connection(m_InputLogin.text, m_InputPassword.text));
    }

    public void Show()
    {
        GameManager.Instance.PauseGame();
        m_Info.text = "";
        m_Fader.Show();
        m_UIMover.Show();
    }

    public void Hide()
    {
        NavigationGuidanceManager.Instance.choiceMinimapWindow.SetActive(true);
        hide();
    }

    public void hide()
    {
        GameManager.Instance.StartGame();
        m_Fader.Hide();
        m_UIMover.Hide();
    }

    private IEnumerator Connection(string login, string password)
    {
        // Reset info text immediately
        m_Info.text = "";

        yield return LoginDBService.Connection(login, password);

        int patientId = LoginDBService.PatientId;

        //Debug.Log("WAZAAAA" + patientId);

        if (patientId != -10)
        {
            switch (patientId)
            {
                case -1: // Incorrect login
                    m_Info.text = "Identifiant incorrect";
                    break;
                case -2: // Incorrect password
                    m_Info.text = "Mot de passe incorrect";
                    break;
                default: // Correct login and password
                    m_Info.text = "";
                    
                    InitData.Instance.PatientId = patientId;

                    yield return UserDetailsDBService.LoadUserDetailsFromDB(patientId);
                    InitData.Instance.Difficulty = UserDetailsDBService.Difficulty;

                    // Hide connection window
                    hide();

                    break;
            }
        }
        else
        {
            m_Info.text = "Echec de connection au serveur";
        }
    }
}
