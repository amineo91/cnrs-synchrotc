﻿using DG.Tweening;
using Keyveo;
using Keyveo.FrontEnd;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Base class for notifications, used to display text to player.
/// </summary>
public class Notification : MonoBehaviour
{
    [SerializeField]
    private bool m_isDisplayed = false;
    public bool IsDisplayed
    {
        get { return m_isDisplayed; }
    }

    [SerializeField]
    private float m_DefaultAnimationDuration = 0.5f;
    
    public Image Border;

    private TextMeshProUGUI m_Directive;
    private Graphic[] m_graphics;
    private Resizer m_Resizer;
    private List<Tweener> m_tweens = new List<Tweener>();


    #region MonoBehaviour

    protected virtual void Awake()
    {
        FrontEndManager.ApplySkinTo(transform);

        m_Directive = GetComponentInChildren<TextMeshProUGUI>();
        m_graphics = GetComponentsInChildren<Graphic>();
        m_Resizer = GetComponentInChildren<Resizer>();
    }

    #endregion

    public string GetText()
    {
        return m_Directive.text;
    }

    /// <summary>
    /// Updates the text of this Notification.
    /// </summary>
    /// <param name="text">The text to display.</param>
    public virtual void UpdateText(string text)
    {
        m_Directive.text = text;
    }

    /// <summary>
    /// Shows this Notification.
    /// </summary>
    public virtual void Show(float animationDuration = -1f)
    {
        // Set default animation duration value from edtior
        if (animationDuration < 0)
        {
            animationDuration = m_DefaultAnimationDuration;
        }

        // Clean all previous tweens
        foreach (Tween tween in m_tweens)
        {
            tween.Kill();
        }
        m_tweens.Clear();

        // Start new tweens
        m_tweens.Add(m_Resizer.Grow(0, animationDuration).OnComplete(() => m_isDisplayed = true));
        foreach (Graphic graphic in m_graphics)
        {
            m_tweens.Add(graphic.DOFade(1, animationDuration));
        }
    }

    /// <summary>
    /// Hides this Notification.
    /// </summary>
    public virtual void Hide(float animationDuration = -1f)
    {
        // Set default animation duration value from edtior
        if (animationDuration < 0)
        {
            animationDuration = m_DefaultAnimationDuration;
        }

        // Clean all previous tweens
        foreach (Tween tween in m_tweens)
        {
            tween.Kill();
        }
        m_tweens.Clear();

        // Remove any material on border to allow graphic fade
        Border.material = null;

        // Start new tweens
        foreach (Graphic graphic in m_graphics)
        {
            m_tweens.Add(graphic.DOFade(0, animationDuration));
        }
        m_tweens.Add(m_Resizer.Shrink(0, animationDuration).OnComplete(() => m_isDisplayed = false));
    }

    /// <summary>
    /// Hides instantly this Notification.
    /// </summary>
    public virtual void InstantHide()
    {
        // Clean all previous tweens
        foreach (Tween tween in m_tweens)
        {
            tween.Kill();
        }
        m_tweens.Clear();

        // Update instant
        foreach (Graphic graphic in m_graphics)
        {
            Color color = graphic.color;
            color.a = 0;
            graphic.color = color;
        }
        m_Resizer.InstantShrink();
        m_isDisplayed = false;
    }

    /// <summary>
    /// Coroutine that temporarily shows this Notification.
    /// </summary>
    /// <param name="duration">Duration between end of show and beginning of hide</param>
    /// <param name="delay">Duration before starting show/hide sequence</param>
    /// <returns></returns>
    public virtual IEnumerator TemporarilyShow(float duration, float delay = 0)
    {
        yield return new WaitForSeconds(delay);
        if (m_Directive.text != "")
        {

            Show();

            yield return new WaitForSeconds(duration + m_DefaultAnimationDuration);

            Hide();

            yield return new WaitForSeconds(m_DefaultAnimationDuration);
        }

        // Self destruction
        DestroyImmediate(gameObject);
    }

    /// <summary>
    /// Starts a coroutine that temporarily shows this Notification.
    /// </summary>
    /// <param name="duration">Duration between end of show and beginning of hide</param>
    /// <param name="delay">Duration before starting show/hide sequence</param>
    /// <returns></returns>
    public void TemporarilyShowAsync(float duration, float delay = 0)
    {
        StartCoroutine(TemporarilyShow(duration, delay));
    }

    /// <summary>
    /// Update border and text color
    /// </summary>
    /// <param name="color"></param>
    public void SetColor(Color color)
    {
        Border.color = color;
        m_Directive.color = color;
    }
}