﻿using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Crosshair : MonoBehaviour
{
    public Texture2D crosshairImage;
    public GameObject CrosshairVisual;

    public LayerMask ClickableLayer;

    [SerializeField]
    private float m_Distance = 2;

    public RaycastHit HitInfo;
    public bool HasHit;

    public static Crosshair Instance;
    
    void Awake()
    {
        Instance = this;
    }
    
    void Update()
    {
        HasHit = Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out HitInfo, m_Distance, ClickableLayer);
        Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward * HitInfo.distance, Color.yellow);
        //Debug.Log(HitInfo.collider.gameObject.name);

        CrosshairVisual.SetActive(MouseLook.m_cursorIsLocked);
    }
}
