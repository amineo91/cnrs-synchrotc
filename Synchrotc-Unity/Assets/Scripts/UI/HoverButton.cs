﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class HoverButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]
    private UnityEvent m_OnPointerEnter;

    [SerializeField]
    private UnityEvent m_OnPointerExit;

    public void OnPointerEnter(PointerEventData eventData)
    {
        m_OnPointerEnter.Invoke();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        m_OnPointerExit.Invoke();
    }
}
