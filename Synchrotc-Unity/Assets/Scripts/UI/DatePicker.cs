﻿using System;
using TMPro;
using UnityEngine;

// TODO - Remove
public class DatePicker: MonoBehaviour
{
    [SerializeField]
    private TMP_InputField m_DayInputField;

    [SerializeField]
    private TMP_InputField m_MonthInputField;

    [SerializeField]
    private TMP_InputField m_YearInputField;

    /// <summary>
    /// Date formated as "2019-08-23"
    /// </summary>
    /// <param name="date"></param>
    public void SetDate(string date)
    {
        string[] dateSplitted = date.Trim().Split(new[] { "-" }, StringSplitOptions.RemoveEmptyEntries);
        m_DayInputField.text = dateSplitted[2];
        m_MonthInputField.text = dateSplitted[1];
        m_YearInputField.text = dateSplitted[0].Substring(2);
    }

    public string GetDate()
    {
        // TODO - Add auto formated input fields to allow only digits characters, with exactly 2 digits
        return "20" + m_YearInputField.text + "-" + m_MonthInputField.text + "-" + m_DayInputField.text;
    }
}
