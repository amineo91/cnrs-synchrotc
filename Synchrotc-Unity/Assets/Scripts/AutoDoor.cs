﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
using UnityStandardAssets.Characters.FirstPerson;

public class AutoDoor : MonoBehaviour
{
    [SerializeField]
    private Transform RightDoor;

    [SerializeField] 
    private Transform LeftDoor;
    
    [SerializeField] 
    private Transform OpenRightDoorPos;
    
    [SerializeField] 
    private Transform OpenLeftDoorPos;

    [SerializeField] 
    private Transform DetectorSide;
    
    private Vector3 CloseRightDoorPos;
    private Vector3 CloseLeftDoorPos;
    
    private bool isTheDoorsOpen = false;

    [HideInInspector]
    public bool isLocked = false;

    public UnityEvent OnEnter;
    public UnityEvent OnExit;

    private void Start()
    {
        CloseRightDoorPos = new Vector3();
        CloseRightDoorPos = RightDoor.position;
        CloseLeftDoorPos = new Vector3();
        CloseLeftDoorPos = LeftDoor.position;
    }

    private void Update()
    {
        var hour = TimeManager.Instance.GetTimeInHour();
        if (hour < 8.5f || hour > 19.5f /*|| ( hour > 12 && hour < 13.5f )*/)
        {
            isLocked = true;
        }
        else
        {
            isLocked = false;
        }
    }

    public void ToogleDoor()
    {
        if (!isLocked)
        {
            if (!isTheDoorsOpen)
            {
                RightDoor.DOMove(OpenRightDoorPos.position,1).SetEase(Ease.OutQuad);
                LeftDoor.DOMove(OpenLeftDoorPos.position,1).SetEase(Ease.OutQuad);
                isTheDoorsOpen = true;
            }
            else
            {
                RightDoor.DOMove(CloseRightDoorPos,1).SetEase(Ease.OutQuad);
                LeftDoor.DOMove(CloseLeftDoorPos,1).SetEase(Ease.OutQuad);
                isTheDoorsOpen = false;
            }
        }
        else
        {
            if (isTheDoorsOpen)
            {
                RightDoor.DOMove(CloseRightDoorPos,1).SetEase(Ease.OutQuad);
                LeftDoor.DOMove(CloseLeftDoorPos,1).SetEase(Ease.OutQuad);
                isTheDoorsOpen = false;
            }

            if (IsInFront(gameObject.transform.position, DetectorSide.forward, FirstPersonController.Instance.gameObject.transform.position))
            {
                Debug.Log("toto");
                RightDoor.DOMove(OpenRightDoorPos.position,1).SetEase(Ease.OutQuad);
                LeftDoor.DOMove(OpenLeftDoorPos.position,1).SetEase(Ease.OutQuad);
                isTheDoorsOpen = true;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        OnEnter.Invoke();
    }
    
    private void OnTriggerExit(Collider other)
    {
        OnExit.Invoke();
    }
    
    public static bool IsInFront(Vector3 origin, Vector3 forward, Vector3 target)
    {
        Vector3 toTarget = (target - origin).normalized;
        return Vector3.Dot(toTarget, forward) > 0;
    }
}
