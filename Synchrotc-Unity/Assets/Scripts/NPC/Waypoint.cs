﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[System.Serializable]
public class WaypointLink
{
    public Waypoint Destination;
    public bool IsPassagePieton;
    public float Weight;
}

[ExecuteInEditMode]
public class Waypoint : MonoBehaviour
{
    [SerializeField]
    public List<WaypointLink> m_Links = new List<WaypointLink>();

    [SerializeField]
    private Color m_associatedGraph = Color.blue;

    public Color AssociatedGraph
    {
        get
        {
            return m_associatedGraph;
        }

        set
        {
            m_associatedGraph = value;
        }
    }


    public Waypoint NearestToStart { get; internal set; }
    
    
    [HideInInspector]
    public float MinimalCostFromStart = -1;

    public void ResetCost()
    {
        NearestToStart = null;
        MinimalCostFromStart = -1;
    }

    public List<WaypointLink> GetLinks()
    {
        return m_Links;
    }

    public void AddLink(Waypoint destination, bool isPassagePieton)
    {
        m_Links.Add(new WaypointLink() { Destination = destination, IsPassagePieton = isPassagePieton });
    }

    public void RemoveLink(Waypoint destination)
    {
        for(int i = 0; i < m_Links.Count; i++)
        {
            if (m_Links[i].Destination == destination)
            {
                m_Links.Remove(m_Links[i]);
                i--;
            }
        }
    }

    public WaypointLink HasLink(Waypoint destination)
    {
        foreach(WaypointLink link in m_Links)
        {
            if(destination == link.Destination)
            {
                return link;
            }
        }

        return null;
    }

    public WaypointLink GetRandomLink(Waypoint ignore = null)
    {
        if((ignore == null || HasLink(ignore) == null) && m_Links.Count > 1)
        {
            return m_Links[Random.Range(0, m_Links.Count)];
        } else
        {
            return m_Links[RandomExcept(0, m_Links.Count, m_Links.IndexOf(HasLink(ignore)))];
        }
    }

    public int RandomExcept(int min, int max, int except)
    {
        int random = Random.Range(min, max);
        if (random >= except) random = (random + 1) % max;
        return random;
    }

#region UNITY_EDITOR
#if UNITY_EDITOR

    void OnDestroy()
    {
        foreach(WaypointLink link in m_Links)
        {
            link.Destination.RemoveLink(this);
        }
    }

    //EDITOR
    void OnDrawGizmos()
    {
        Gizmos.color = AssociatedGraph;
        Gizmos.DrawSphere(transform.position, 0.05f);

        foreach (WaypointLink link in m_Links)
        {
            if (link.Destination == null)
                continue;

            if(link.Destination.gameObject == Selection.activeGameObject)
            {
                Gizmos.color = link.IsPassagePieton ? Color.white : Color.cyan;
            } else
            {
                Gizmos.color = link.IsPassagePieton ? Color.black : AssociatedGraph;
            }
            Gizmos.DrawLine(transform.position, link.Destination.transform.position);
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(transform.position, 0.1f);
        
        foreach(WaypointLink link in m_Links)
        {
            if (link.Destination == null)
                continue;

            Gizmos.color = link.IsPassagePieton ? Color.white : Color.cyan;
            Gizmos.DrawLine(transform.position, link.Destination.transform.position);
        }
    }
#endif
#endregion
}
