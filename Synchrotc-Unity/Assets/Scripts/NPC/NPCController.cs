﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCController : MonoBehaviour
{
    private Waypoint previousWaypoint;
    private Waypoint currentDestination;

    private NavMeshAgent agent;

    private bool isTravelling = false;

    private Animator animator;

    [SerializeField]
    private float m_IdleChance = 5f;

    [SerializeField]
    private Vector2 m_IdleDurationBounds;

    [SerializeField]
    private float m_SpeedMultiplicator = 1f;

    private bool isIdle = false;
    private float nextTick = 0f;

    void Awake()
    {
        animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
    }

    public void SetDestination(WaypointLink destination)
    {
        if (agent == null || animator == null)
            return;

        if(currentDestination != null)
        {
            previousWaypoint = currentDestination;
        }

        currentDestination = destination.Destination;

        if (Random.Range(0f, 100f) < m_IdleChance && !destination.IsPassagePieton)
        {
            isIdle = true;
            animator.SetBool("Walk", false);

            nextTick = Time.time + Random.Range(m_IdleDurationBounds.x, m_IdleDurationBounds.y);

            isTravelling = false;
        }
        else
        {
            isIdle = false;
            animator.SetBool("Walk", true);

            if (destination.IsPassagePieton)
            {
                animator.SetTrigger("Look Around");
            }
            
            agent.SetDestination(currentDestination.transform.position);

            isTravelling = true;
        }
    }

    void Update()
    {
        if (isIdle)
        {
            if(Time.time >= nextTick)
            {
                isIdle = false;
                animator.SetBool("Walk", true);
                agent.SetDestination(currentDestination.transform.position);
                isTravelling = true;
            }
        }

        if (!isTravelling)
            return;

        float dist = 0;

        if (!agent.isOnNavMesh)
        {
            gameObject.SetActive(false); //Reset if not on navmesh because... "reasons"
            transform.position = previousWaypoint.transform.position;
            gameObject.SetActive(true);
            return;
        } else
        {
            dist = agent.remainingDistance;
        }

        if (dist != Mathf.Infinity && agent.pathStatus == NavMeshPathStatus.PathComplete && agent.remainingDistance <= 0.1f)
        {
            isTravelling = false;
            OnArrival();
        }
    }

    void OnAnimatorMove()
    {
        agent.speed = (animator.deltaPosition / Time.fixedDeltaTime).magnitude * m_SpeedMultiplicator;
    }

    void OnArrival()
    {
        SetDestination(currentDestination.GetRandomLink(previousWaypoint));
    }
}
