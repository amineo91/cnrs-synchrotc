﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WannaTalkDetector : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == FindObjectOfType<CharacterController>().gameObject.name)
        {
            SpeechesManager.Instance.StartConversation(gameObject.transform.parent.name);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == FindObjectOfType<CharacterController>().gameObject.name)
        {
            SpeechesManager.Instance.EndConversation();
        }
    }
}
