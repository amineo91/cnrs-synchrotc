﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class NPCSystem : MonoBehaviour
{

    public static NPCSystem Instance;

    [SerializeField]
    private List<Waypoint> m_Waypoints = new List<Waypoint>();

    private List<NPCController> m_NPCs = new List<NPCController>();

    [SerializeField]
    private int m_NumberOfNPCsToSpawn;

    [SerializeField]
    private NPCController[] m_AvailableNPCsPool;

    void Awake()
    {
        Instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        bool canSpawnSeparately = false;

        List<int> pickedIndexes = new List<int>();

        if(m_NumberOfNPCsToSpawn < m_Waypoints.Count)
        {
            canSpawnSeparately = true;
        }

        for(int i = 0; i < m_NumberOfNPCsToSpawn; i++)
        {
            NPCController npc = Instantiate(m_AvailableNPCsPool[Random.Range(0, m_AvailableNPCsPool.Length)], transform);

            int selected_waypoint = Random.Range(0, m_Waypoints.Count);

            if (canSpawnSeparately)
            {
                int loops = 0;
                while(pickedIndexes.Contains(selected_waypoint) && loops < 1000)
                {
                    loops++;
                    selected_waypoint = Random.Range(0, m_Waypoints.Count);
                }

                pickedIndexes.Add(selected_waypoint);
            }

            Waypoint wayPoint = m_Waypoints[selected_waypoint];

            npc.transform.position = wayPoint.transform.position;
            npc.transform.rotation = wayPoint.transform.rotation;

            npc.SetDestination(wayPoint.GetRandomLink());
        }
    }
}
