﻿using DG.Tweening;
using Keyveo;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TakeAble : MonoBehaviour {

    [SerializeField]
    private Item m_Item;

    private Resizer resizer;

    [SerializeField]
	private Resizer m_UIResizer;

	[SerializeField]
	private Text m_Text;

    [SerializeField]
    private UnityEvent m_OnTake;

	private bool taken = false;
    private bool isUIShownForEver = false;
    private bool isOverObject = false;

    [SerializeField]
    private bool m_CanTake = true;

    public Item getItem()
    {
        return m_Item;
    }

	private void Start() {
		resizer = GetComponent<Resizer>();
        UpdateText();
        m_UIResizer.Hide();
    }

    private void Update()
    {
        OnRayEnter();
        OnRayExit();
        OnRayClick();
    }

    public void UpdateText()
    {
        m_Text.text = m_Item.ItemName;
    }

    public void OnRayClick()
    {
        if (taken || !m_CanTake)
            return;

        if (Crosshair.Instance.HasHit && (Crosshair.Instance.HitInfo.collider.gameObject == this.gameObject || Crosshair.Instance.HitInfo.collider.gameObject == this.gameObject.GetComponentInChildren<Collider>().gameObject))
        {
            if (Input.GetMouseButton(0)) {
                taken = TakeIt();
            }
        }
	}

    /// <summary>
    /// Try to add object to inventory.
    /// </summary>
    /// <returns>True if the object is taken correctly, false otherwise.</returns>
    public bool TakeIt()
    {
        taken = FindObjectOfType<Inventory>().TryAddObject(m_Item);
        if (taken)
        {
            m_OnTake.Invoke();
            resizer.Hide();
        }
        return taken;
    }

	public void OnRayEnter()
	{
        if (isOverObject == false && isUIShownForEver == false && Crosshair.Instance.HasHit && (Crosshair.Instance.HitInfo.collider.gameObject == this.gameObject || Crosshair.Instance.HitInfo.collider.gameObject == this.gameObject.GetComponentInChildren<Collider>().gameObject)) {
            isOverObject = true;
            m_UIResizer.Show();
        }
	}

	public void OnRayExit()
	{
        if (isOverObject && isUIShownForEver == false && (Crosshair.Instance.HasHit == false || (Crosshair.Instance.HitInfo.collider.gameObject != this.gameObject && Crosshair.Instance.HitInfo.collider.gameObject != this.gameObject.GetComponentInChildren<Collider>().gameObject)))
        {
            isOverObject = false;
            m_UIResizer.Hide();
        }
    }

    public void ShowUI(bool forEver = true)
    {
        m_UIResizer.Show();
        isUIShownForEver = forEver;
    }

    public void SetTakeAble(bool can)
    {
        m_CanTake = can;
    }
	
}
