﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;

public class ItemDepositZone : MonoBehaviour
{
    #region Declaration des variables
    [SerializeField]
    private MiniInventory miniInv;

    [SerializeField]
    private Item wantedItem;

    [SerializeField]
    private UnityEvent m_OnDeposit;

    public float wantedFlouz;

    #endregion

    #region Fonction
    public void OnMouseDown()
    {
        miniInv.Open(this, wantedItem, wantedFlouz);
    }

    public void AcceptObject()
    {
        m_OnDeposit.Invoke();
    }
    #endregion
}
