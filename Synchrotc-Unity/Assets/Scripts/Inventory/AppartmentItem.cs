﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class AppartmentItem : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
    
	private Item m_Item;

	[SerializeField]
	private Image m_Icon;

	[SerializeField]
	private Graphic[] m_OkGraphics;

	[SerializeField]
	private TextMeshProUGUI m_NameText;

	public void Setup(Item it){
        m_Item = it;
		m_Icon.sprite = m_Item.ItemIcon;
		m_Icon.color = Color.white;
		m_NameText.text = m_Item.ItemName;
		foreach(Graphic g in m_OkGraphics){
			g.color = new Color(g.color.r, g.color.g, g.color.b, 0);
		}
	}

	public void Obtain(){
		foreach(Graphic g in m_OkGraphics){
			g.color = new Color(g.color.r, g.color.g, g.color.b, 1);
		}
	}

	public Item GetItem(){
		return m_Item;
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
		TooltipManager.Instance.DisplayTooltip(m_Item.ItemName, m_Item.ItemDescription);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
		TooltipManager.Instance.HideCurrentTooltip();
    }
}
