﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField] [CreateAssetMenu(fileName = "New Item", menuName = "Items/Create New Item")]
public class Item : ScriptableObject {

	public string ItemName;

	public string ItemDescription;

	public Sprite ItemIcon;

    [Tooltip("Price in €")]
    public float Price;

    public string Article = "un";
}
