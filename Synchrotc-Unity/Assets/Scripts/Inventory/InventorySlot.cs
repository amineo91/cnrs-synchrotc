﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class InventorySlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    #region Declaration des variables
    [SerializeField]
	private Item m_Item;
	[SerializeField]
	private Image m_Icon;
    #endregion

    #region Fonctions
    // Use this for initialization
    public void RefreshSlot () {
		if(!IsEmpty())
        {
            m_Icon.gameObject.SetActive(true);
            m_Icon.sprite = m_Item.ItemIcon;
			//m_Icon.color = Color.white;
		} else
        {
            m_Icon.gameObject.SetActive(false);
            m_Icon.sprite = null;
			//m_Icon.color = new Color(0, 0, 0, 0);
		}
	}

	public void SetSlot(Item item){
		m_Item = item;
		RefreshSlot();
	}

	public Item GetItem(){
		return m_Item;
	}

	public bool IsEmpty(){
		return m_Item == null;
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
		if(!IsEmpty()){
			TooltipManager.Instance.DisplayTooltip(m_Item.ItemName, m_Item.ItemDescription);
		}
    }

    public void OnPointerExit(PointerEventData eventData)
    {
		if(!IsEmpty()){
			TooltipManager.Instance.HideCurrentTooltip();
		}
    }

    public void EmptySlot()
    {
        m_Item = null;
        RefreshSlot();
    }
    #endregion
}
