﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class MiniInventorySlot : MonoBehaviour
{
    #region Declaration des variables
    [SerializeField]
    private Item m_Item;
    [SerializeField]
    private Image m_Icon;
    [SerializeField]
    private TextMeshProUGUI m_NameText;
    #endregion

    #region Fonctions
    public void RefreshSlot()
    {
        if (!IsEmpty())
        {
            m_Icon.sprite = m_Item.ItemIcon;
            m_Icon.color = Color.white;
            m_NameText.text = m_Item.ItemName;
        }
        else
        {
            m_Icon.sprite = null;
            m_Icon.color = new Color(0, 0, 0, 0);
            m_NameText.text = "";
        }
    }

    public void SetSlot(Item item)
    {
        m_Item = item;
        RefreshSlot();
    }

    public void Select()
    {
        FindObjectOfType<MiniInventory>().Select(m_Item);
    }

    public Item GetItem()
    {
        return m_Item;
    }

    public bool IsEmpty()
    {
        return m_Item == null;
    }
    #endregion
}
