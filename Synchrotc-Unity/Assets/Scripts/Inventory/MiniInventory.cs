﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MiniInventory : MonoBehaviour
{
    #region Declaration des variables
    [SerializeField]
    private UIMover uimover;

    public Inventory inventory;
    public Transform slotContainer;
    public GameObject prefabSlot;
    private Item wanted;
    private float wantedFlouz;
    private ItemDepositZone currentZone;
    #endregion

    #region Fonctions
    public void Open(ItemDepositZone caller, Item wantedItem, float price = 0)
    {
        currentZone = caller;
        uimover.Show();
        wanted = wantedItem;
        wantedFlouz = price;
        foreach(Transform t in slotContainer)
        {
            if(t != slotContainer)
            {
                Destroy(t.gameObject);
            }
        }

        List<Item>Items = inventory.GetItems();
       
        foreach(Item i in Items)
        {
            GameObject slot = Instantiate(prefabSlot, slotContainer);
            slot.GetComponent<MiniInventorySlot>().SetSlot(i);
        }

    }

    public void Select(Item item)
    {
        if (item == wanted && wantedFlouz <= FlouzManager.Instance.GetMoney() && inventory.Contains(FlouzManager.Instance.GetWallet()))
        {
            uimover.Hide();
            if(wantedFlouz > 0)
            {
                FlouzManager.Instance.TryTakeOutMoney(wantedFlouz);
            }
            else
            {
                NotificationManager.Instance.Notify("Action réussie !", 3);
                inventory.RemoveObject(wanted);
            }
            currentZone.AcceptObject();
        }
        else if(item != wanted)
        {
            NotificationManager.Instance.Notify("Cet objet semble incorrect...",3);
        }
        else
        {
            FlouzManager.Instance.TryTakeOutMoney(wantedFlouz);
        }
    }
    #endregion
}
