﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

[System.Serializable]
public class ItemEvent : UnityEvent<Item>
{
}

public class Inventory : MonoBehaviour {
    #region Declaration des variables

    [SerializeField]
    private BottomMenu bottomMenu;

    [SerializeField]
    private Transform m_ContainerInventorySlots;

    [SerializeField]
	private List<InventorySlot> m_InventorySlots = new List<InventorySlot>();



    public ItemEvent OnObjectRemoved = new ItemEvent();

    public int NbEmptyInventorySlots
    {
        get
        {
            return m_InventorySlots.FindAll(inventorySlot => inventorySlot.IsEmpty()).Count;
        }
    }


    [SerializeField]
	private Graphic[] m_NewItemObtainNotifItems;

	public static Inventory Instance;

	private void Awake()
	{
		Instance = this;
		
        m_InventorySlots = new List<InventorySlot>(m_ContainerInventorySlots.GetComponentsInChildren<InventorySlot>());
    }

    private void Start()
    {
	    RefreshInventoryView();
    }
    
    private void Update(){
	    
    }
    #endregion

    #region Fonctions
    public bool TryAddObject(Item item){
		foreach(InventorySlot slot in m_InventorySlots){
			if(slot.IsEmpty()){
				slot.SetSlot(item);
                
                bottomMenu.Open();
                foreach (Graphic g in m_NewItemObtainNotifItems){
					g.DOKill();
                    
                    g.DOFade(1, 0.75f).OnComplete(delegate {
                        g.DOFade(0, 0.75f).SetDelay(2f);
					});
				}
                bottomMenu.Close(false, 4);

                return true;
			}
		}
		Debug.LogWarning("Trying to add item to inventory but it is full");
		return false;
	}

    public void Event_TryAddObject(Item item)
    {
        TryAddObject(item);
    }

    public bool RemoveObject(Item item)
    {
        InventorySlot objToRemove = m_InventorySlots.Find((slot) => { return slot.GetItem() == item; });
        if (objToRemove == null)
            return false;

        objToRemove.EmptySlot();
        if (OnObjectRemoved != null)
        {
            OnObjectRemoved.Invoke(item);
        }
        return true;
    }

    public bool RemoveObjects(List<Item> items)
    {
        foreach (Item item in items)
        {
            if (RemoveObject(item) == false)
            {
                return false;
            }
        }
        return true;
    }

    public void RefreshInventoryView(){
		foreach(InventorySlot slot in m_InventorySlots){
			slot.RefreshSlot();
		}
		foreach(Graphic g in m_NewItemObtainNotifItems){
			g.DOKill();
			g.DOFade(0, 0.2f);
		}
	}

	public List<Item> GetItems(){
		List<Item> items = new List<Item>();
		foreach(InventorySlot slot in m_InventorySlots){
            if (slot.IsEmpty())
                continue;
			items.Add(slot.GetItem());
		}
		return items;
    }

    public bool Contains(Item ContainsItem)
    {
        return m_InventorySlots.Find((slot) => { return slot.GetItem() == ContainsItem; });
    }

    public bool ContainsAll(List<Item> items)
    {
        foreach(Item item in items)
        {
            if (Contains(item) == false)
            {
                return false;
            }
        }
        return true;
    }
    #endregion
}
