﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;


public class FurnitureInventory : MultiShowable {
    #region Declaration des variables

    public static FurnitureInventory Instance;

    [SerializeField]
	private List<FurnitureSlot> m_Furniture = new List<FurnitureSlot>();

	[SerializeField]
	private Graphic[] m_NewItemObtainNotifItems;



	private void Start(){
        RefreshInventoryView();
	}

    private void Awake()
    {
        Instance = this;
    }
    #endregion

    #region Fonctions
    public void AddObject(Furniture furniture){
		foreach(FurnitureSlot slot in m_Furniture){
			if(slot.IsEmpty()){
				slot.SetSlot(furniture);

				FindObjectOfType<BottomMenu>().Open();
				foreach(Graphic g in m_NewItemObtainNotifItems){
					g.DOKill();

					g.DOFade(1, 0.75f).OnComplete(delegate {
                        g.DOFade(0, 0.75f).SetDelay(2f);
					});
				}

				return;
			}
		}
		Debug.LogWarning("Trying to add furniture to inventory but it is full");
		return;
	}

    public bool RemoveObject(Furniture furniture)
    {
        FurnitureSlot objToRemove = m_Furniture.Find((slot) => { return slot.GetFurniture() == furniture; });
        if (objToRemove == null)
            return false;

        objToRemove.EmptySlot();
        return true;
    }

	public void RefreshInventoryView(){
		foreach(FurnitureSlot slot in m_Furniture){
			slot.RefreshSlot();
		}
		foreach(Graphic g in m_NewItemObtainNotifItems){
			g.DOKill();
			g.DOFade(0, 0.2f);
		}
	}

	public List<Furniture> GetFurnitures(){
		List<Furniture> furnitures = new List<Furniture>();
		foreach(FurnitureSlot slot in m_Furniture){
            if (slot.IsEmpty())
                continue;
			furnitures.Add(slot.GetFurniture());
		}
		return furnitures;
	}

    public FurnitureSlot Contains(Furniture ContainsFurniture)
    {
        return m_Furniture.Find((slot) => { return slot.GetFurniture() == ContainsFurniture; });
    }

    
    public override void Show(bool instant = false)
    {
        base.Show();
    }

    public override void Hide(bool instant = false)
    {
        base.Hide();
    }

    public void LockSlot(Furniture furniture)
    {
        FurnitureSlot slot = Contains(furniture);
        if (slot != null)
        {
            slot.LockSlot();
        }
    }
    
    #endregion
}
