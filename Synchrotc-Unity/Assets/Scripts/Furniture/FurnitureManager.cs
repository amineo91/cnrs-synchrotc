﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FurnitureManager : MonoBehaviour
{
    #region Déclaration des Variables
    [SerializeField]
    BottomMenu bottomMenu;

    [SerializeField]
    HideShowPanel planningPanel;

    [SerializeField]
    private MultiShowable UiEditMod;

    [SerializeField]
    private Button quitPanelApartButton;

    [SerializeField]
    private Button quitButtonAppartButton;

    public static FurnitureManager Instance;

    private bool isInPlacementMode = false;

    [SerializeField]
    private FurnitureZone[] zones;

    public void Awake()
    {
        Instance = this;
    }
    #endregion

    #region Fonctions
    public void Activate(Furniture furniture)
    {
        FurnitureInventory.Instance.Hide();
        
        bottomMenu.gameObject.SetActive(false);
        planningPanel.gameObject.SetActive(false);

        UiEditMod.Show();

        quitButtonAppartButton.interactable = false;
        quitPanelApartButton.interactable = false;

        isInPlacementMode = true;


        foreach(FurnitureZone f in zones)
        {
            f.Activate(furniture);
        }
    }

    public bool IsPlacing()
    {
        return this.isInPlacementMode;
    }

    public void CancelPlacement()
    {
        UiEditMod.Hide();

        foreach (FurnitureZone f in zones)
        {
            f.DeActivate();
        }

        bottomMenu.gameObject.SetActive(true);
        planningPanel.gameObject.SetActive(true);
        bottomMenu.Close(true);
        planningPanel.Close(true);

        quitButtonAppartButton.interactable = true;
        quitPanelApartButton.interactable = true;


        isInPlacementMode = false;
    }

    public void ConfirmPlacement(Furniture furniture)
    {
        FurnitureInventory.Instance.LockSlot(furniture);
        CancelPlacement();
    }


    #endregion
}
