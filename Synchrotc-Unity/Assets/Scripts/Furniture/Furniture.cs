﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum FurnitureType
{
    SMALL, 
    MEDIUM,
    LARGE
}

[CreateAssetMenu(fileName = "New Furniture", menuName = "Furniture/Create New Furniture")]

public class Furniture : ScriptableObject
{
    public string FurnitureName;

    public string FurnitureDescription;

    public FurnitureType FurnitureType;

    public Sprite FurnitureSprite;

    public GameObject FurniturePrefab;
}
