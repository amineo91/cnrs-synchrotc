﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FurnitureZone : MonoBehaviour, IPointerClickHandler//, IPointerEnterHandler, IPointerExitHandler
{
    #region Déclaration des Variables
    [SerializeField]
    private FurnitureType furnitureType;

    private Furniture currentFurniture;

    private bool isInPlacementMode = false;

    [SerializeField]
    private Transform display;

    [SerializeField]
    private Transform furnitureContainer;



    #endregion

    #region Fonctions
    public void DeActivate()
    {
        if (isInPlacementMode)
        {
            currentFurniture = null;
            isInPlacementMode = false;
            display.gameObject.SetActive(false);
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if(isInPlacementMode && currentFurniture != null)
        {
            display.gameObject.SetActive(false);
            Instantiate(currentFurniture.FurniturePrefab, furnitureContainer);

            isInPlacementMode = false;
            FurnitureManager.Instance.ConfirmPlacement(currentFurniture);
        }
    }

    public void Activate(Furniture furniture)
    {
        if(currentFurniture != null || furnitureType != furniture.FurnitureType)
            return;
        currentFurniture = furniture;
        display.gameObject.SetActive(true);
        isInPlacementMode = true;
    }

    

    #endregion
}
