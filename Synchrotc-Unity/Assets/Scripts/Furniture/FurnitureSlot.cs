﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class FurnitureSlot : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    #region Declaration des variables
    [SerializeField]
	private Furniture m_Furniture;
	[SerializeField]
	private Image m_Icon;
	[SerializeField]
	private TextMeshProUGUI m_NameText;

    private bool intercatable = true;
    #endregion

    #region Fonctions
    // Use this for initialization
    public void RefreshSlot () {
		if(!IsEmpty()){
			m_Icon.sprite = m_Furniture.FurnitureSprite;
			m_Icon.color = Color.white;
			m_NameText.text = m_Furniture.FurnitureName;
		} else {
			m_Icon.sprite = null;
			m_Icon.color = new Color(0, 0, 0, 0);
			m_NameText.text = "";
		}
	}

	public void SetSlot(Furniture furniture){
		m_Furniture = furniture;
		RefreshSlot();
	}

	public Furniture GetFurniture(){
		return m_Furniture;
	}

	public bool IsEmpty(){
		return m_Furniture == null;
	}

    public void OnPointerEnter(PointerEventData eventData)
    {
		if(!IsEmpty()){
			TooltipManager.Instance.DisplayTooltip(m_Furniture.FurnitureName, m_Furniture.FurnitureDescription);
		}
    }

    public void OnPointerExit(PointerEventData eventData)
    {
		if(!IsEmpty()){
			TooltipManager.Instance.HideCurrentTooltip();
		}
    }

    public void EmptySlot()
    {
        m_Furniture = null;
        RefreshSlot();
    }

    public void LockSlot()
    {
        this.intercatable = false;
    }

    public void Select()
    {
        if (intercatable)
        { 
            FurnitureManager.Instance.Activate(m_Furniture);
        }
    }

    #endregion
}
