﻿using Keyveo;
using UnityEngine;
using UnityEngine.Events;
using UnityStandardAssets.Characters.FirstPerson;

public class UsableObject : MonoBehaviour
{
    public Resizer uiResizer;

    public UnityEvent OnInteraction;
    
    private bool isOverObject = false;


    private void Start()
    {
        uiResizer.Hide();
    }

    // Update is called once per frame
    void Update()
    {
            OnRayEnter();
            OnRayExit();
            OnRayClick(); 
    }

    private void OnRayClick()
    {
        if (Crosshair.Instance.HasHit && Crosshair.Instance.HitInfo.collider.gameObject == this.gameObject)
        {
            if (Input.GetMouseButtonDown(0))
            {
                OnInteraction.Invoke();
            }
        }
    }

    private void OnRayEnter()
    {
        if (isOverObject == false && Crosshair.Instance.HasHit && Crosshair.Instance.HitInfo.collider.gameObject == this.gameObject)
        {
            isOverObject = true;
            uiResizer.Show();
        }
    }

    private void OnRayExit()
    {
        if (isOverObject && (Crosshair.Instance.HasHit == false || Crosshair.Instance.HitInfo.collider.gameObject != this.gameObject))
        {
            isOverObject = false;
            uiResizer.Hide();
        }
    }

    public void TpToTarget(Transform target)
    {
        FirstPersonController.Instance.RotateTo(target);
    }

    public void startSpeech(GameObject npc)
    {

    }
}
