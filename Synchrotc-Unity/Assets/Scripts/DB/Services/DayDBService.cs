﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class DayDBService
{
    public static int idDay;

    /// <summary>
    /// Select first day with specified patient ID and date
    /// </summary>
    /// <returns>The day ID if any, -1 otherwise</returns>
    public static IEnumerator LoadDayFromDB(int idPatient)
    {
        string request = "GetTheDay.php?" + "idPatient=" + UnityWebRequest.EscapeURL(idPatient.ToString());

        idDay = -1;
        yield return DBService.RequestDB(request, ParseResponse);
    }

    private static void ParseResponse(string response)
    {
        string rowSeparator = "<html></br></br></html>";

        string[] splittedData = response.Split(new[] { rowSeparator }, StringSplitOptions.None);

        if (splittedData.Length > 0)
        {
            // Check if (idPatient, date) is unique key in DB
            /*if (splittedData.Length > 1)
            {
                //Debug.LogWarning("More than one day associated to this patient. First one is used, others are discarded.");
            }
            string data = splittedData[0].Trim();

            if (data.Length > 0)
            {
                idDay = int.Parse(data);
            }
            else // No row
            {
                idDay = -1;
            }*/

            for (var i = 0; i < splittedData.Length; i++) {
                ////Debug.Log(splittedData[i]);

                if (splittedData[i] == "fail" || splittedData[i] == "")
                {
                    idDay = int.Parse(splittedData[i+1]);
                    break;
                }
            }

            if(idDay > 17)
            {
                InitData.Instance.Difficulty = DifficultyMode.Hard;
            }
            else
            {
                InitData.Instance.Difficulty = DifficultyMode.Easy;
            }

            //Debug.Log(idDay);
        }
        else // Default response is " ", thus this case should never happen
        {
            idDay = -1;
        }
    }
}
