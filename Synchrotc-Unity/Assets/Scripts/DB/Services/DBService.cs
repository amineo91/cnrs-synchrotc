﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class DBService
{
    public static bool IS_AVAILABLE = true;
    public static string URL_ROOT = "http://keyveo.com/Apps/SynchroTC/"; // Updated by DebugDBService
    public static string PATH_REQUESTS = "dist/php/requests/";

    
    public static IEnumerator RequestDB(string request, Action<string> onSuccess, Action<UnityWebRequest> onError = null)
    {
        // Get the URL to the site and create a download object to get the result.
        request = URL_ROOT + PATH_REQUESTS + request;
        UnityWebRequest hs_post = UnityWebRequest.Get(request);
        yield return hs_post.SendWebRequest(); // Wait until the download is done
        
        if (hs_post.error != null)
        {
            //Debug.LogError("There was an error accessing the bdd: " + hs_post.error);
            if (onError != null)
            {
                onError(hs_post);
            }
        }
        else
        {
            //Debug.Log("Request=" + request);
            //Debug.Log("Response=" + hs_post.downloadHandler.text);
            onSuccess(hs_post.downloadHandler.text);
        }
    }

    public static string Md5Sum(string strToEncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        // encrypt bytes
        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        // Convert the encrypted bytes back to a string (base 16)
        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }
}
