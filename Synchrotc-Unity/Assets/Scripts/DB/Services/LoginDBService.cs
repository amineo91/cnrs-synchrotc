﻿using System.Collections;
using UnityEngine.Networking;
using UnityEngine;

public class LoginDBService
{
    private static string SECRET_KEY = "Madness7SalutesY0u";

    private static int patientId;

    public static int PatientId
    {
        get { return patientId; }
    }

    /// <summary>
    /// Select patient ID with specified login and password
    /// </summary>
    /// <returns>The patient ID if any, -1 otherwise</returns>
    public static IEnumerator Connection(string login, string password)
    {
        string hash = DBService.Md5Sum(login + password + SECRET_KEY);

        //Debug.Log(hash+"WAZAAAAA"+login+"WOZOOOO"+password);

        string request = "checkLogin.php?" + "login=" + UnityWebRequest.EscapeURL(login) + "&password=" + UnityWebRequest.EscapeURL(password) + "&hash=" + hash;

        patientId = -10;
        yield return DBService.RequestDB(request, ParseResponse);
    }

    private static void ParseResponse(string response)
    {
        patientId = int.Parse(response);
    }
}
