﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Keyveo.Scenarisation;

public class ErrorDBService : MonoBehaviour
{

    public static ErrorDBService Instance;

    private static string responseDB;

    private void Awake()
    {
        Instance = this;
    }

   /* private void Update()
    {
        //Debug.Log(ScenarioManager.Instance.m_currentStep.m_registeredComponent.gameObject.transform.parent.gameObject.name);
    }*/

    public void addErrorToObjectif()
    {
        dataObjectif dataObj = ScenarioManager.Instance.m_currentStep.m_registeredComponent.gameObject.transform.parent.GetComponent<dataObjectif>();
        dataObj.nberror++;
        //Debug.LogError("ERROR++");
    }

    public void addErrorsToDB()
    {
        dataObjectif dataObj = ScenarioManager.Instance.m_currentStep.m_registeredComponent.gameObject.transform.parent.GetComponent<dataObjectif>();
        // TODO
        //Debug.LogError("Revoir la sauvegarde des erreurs en BDD");
        //StartCoroutine(PutTheMistakesInDB(dataObj.idScenario, dataObj.nberror));
    }
    
    public IEnumerator PutTheMistakesInDB(string idScenario, float nbErrors)
    {
        string request = "SetErrorsScenario.php?" + "id_scenario=" + UnityWebRequest.EscapeURL(idScenario.ToString()) + 
            "&" + "nbErrors=" + UnityWebRequest.EscapeURL(nbErrors.ToString()) + 
            "&" + "id_date=" + UnityWebRequest.EscapeURL(GameManager.Instance.m_IdDay.ToString());

        //Debug.Log(idScenario.ToString());
        //Debug.Log(nbErrors.ToString());
        //Debug.Log(GameManager.Instance.m_IdDay.ToString());

        yield return DBService.RequestDB(request, ParseResponse);
    }

    private static void ParseResponse(string response)
    {
        //Debug.Log(response);
        responseDB = response;
    }

    public IEnumerator PutTheStateInDB(string state)
    {
        string request = "SetStateDay.php?" + "id_date=" + UnityWebRequest.EscapeURL(GameManager.Instance.m_IdDay.ToString()) +
            /*"&" + "finishTime=" + UnityWebRequest.EscapeURL(System.DateTime.Now.ToString()) + */"$" + "dayState=" + UnityWebRequest.EscapeURL(state);

        //Debug.Log(System.DateTime.Now.ToString());
        //Debug.Log(GameManager.Instance.m_IdDay.ToString());

        yield return DBService.RequestDB(request, ParseResponse);
    }
}
