﻿using System;
using System.Collections;
using System.Collections.Generic;
using Keyveo.Scenarisation;
using UnityEngine;
using UnityEngine.Networking;

public class ScenarioDBService
{
    public static ScenarioDB scenarioDB;

    
    
    /// <summary>
    /// Select all objectives, and their items, associated to specified day
    /// </summary>
    /// <returns>The whole scenario informations</returns>
    public static IEnumerator LoadScenarioFromDB(int idDay)
    {
        string request = "GetTheScenario.php?" + "id_day=" + UnityWebRequest.EscapeURL(idDay.ToString()) + "&id_player=" + UnityWebRequest.EscapeURL(LoginDBService.PatientId.ToString());

        scenarioDB = null;
        yield return DBService.RequestDB(request, (string response) => {
            ParseResponse(response);
            scenarioDB.idDay = idDay;
        });
    }
    
    private static void ParseResponse(string response)
    {
        scenarioDB = new ScenarioDB();
        List<ObjectiveDB> objectivesDB = scenarioDB.objectivesDB;

        string rowSeparator = "<html></br></br></html>";
        string fieldSeparator = ";";

        string querySeparatorItems = "<html></br></html>";
        string rowSeparatorItems = "</br>";

        string[] splittedScenarios = response.Trim().Split(new[] { rowSeparator }, StringSplitOptions.RemoveEmptyEntries);

        if (splittedScenarios.Length > 0)
        {
            for (int indexScenario = 0; indexScenario < splittedScenarios.Length; indexScenario++)
            {
                string splittedScenario = splittedScenarios[indexScenario];

                string[] splittedData = splittedScenario.Split(new[] { querySeparatorItems }, StringSplitOptions.RemoveEmptyEntries);
                string scenarioData = splittedData[0];
                string itemsData = splittedData[1];

                string[] fields = scenarioData.Split(new[] { fieldSeparator }, StringSplitOptions.None);

                ObjectiveDB objectiveDB = new ObjectiveDB();
                objectivesDB.Add(objectiveDB);

                int i = 0;
                ScenarioManager.effectByObjectName effect = null;
                objectiveDB.id = fields[i++]; // TODO - not used
                objectiveDB.name = fields[i++]; // TODO - not used
                objectiveDB.description = fields[i++];
                objectiveDB.location = fields[i++];
                objectiveDB.id_missionAccount = fields[i++];

                foreach (string row in itemsData.Split(new[] { rowSeparatorItems }, StringSplitOptions.RemoveEmptyEntries))
                {
                    fields = row.Split(new[] { fieldSeparator }, StringSplitOptions.None);
                    //Debug.Log(fields.Length);
                    ItemDB itemDB = new ItemDB();
                    objectiveDB.itemsDB.Add(itemDB);

                    effect = new ScenarioManager.effectByObjectName();

                    i = 0;
                    itemDB.name = fields[i++];

                    itemDB.id_place = int.Parse(fields[i++]);

                    itemDB.action = fields[i++];

                    itemDB.effect = fields[i++];
                    effect.name = itemDB.name;
                    effect.effect = itemDB.effect;

                    //itemDB.nberror = fields[i++]; // TODO

                    if(effect.effect != null && effect.effect != "")
                    {
                        ScenarioManager.Instance.effectByObjectsName.Add(effect);
                    }
                }
            }
        }
    }
}
