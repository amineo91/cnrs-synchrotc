﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Networking;

public class TimeStatDBService : MonoBehaviour
{
    public static string responseDB;

    public static IEnumerator PutTheBeginTimeScenarioInDB(int idDayScenario)
    {
        string request = "SetTimeBeginScenario.php?" + "id_day_scenario=" + UnityWebRequest.EscapeURL(idDayScenario.ToString()) + "&id_patient=" + UnityWebRequest.EscapeURL(LoginDBService.PatientId.ToString());

        yield return DBService.RequestDB(request, ParseResponse);
    }

    public static IEnumerator PutTheEndTimeScenarioInDB(string idScenarioRapport)
    {
        //Debug.Log("testBDDScreen" + TrackingManager.Instance.nameScreen);

        string request = "SetTimeEndScenario.php?" + "id_scenario_rapport=" + UnityWebRequest.EscapeURL(idScenarioRapport.ToString()) + "&name_screen=" + UnityWebRequest.EscapeURL(TrackingManager.Instance.nameScreen);

        yield return DBService.RequestDB(request, ParseResponse);
    }

    public static IEnumerator PutTheEndTimeAndStatusScenarioInDB(string idScenarioRapport, string cause)
    {
        string request = "SetTimeEndAndStatusScenario.php?" + "id_scenario_rapport=" + UnityWebRequest.EscapeURL(idScenarioRapport.ToString()) + "&cause=" + UnityWebRequest.EscapeURL(cause.ToString()) + "&name_screen=" + UnityWebRequest.EscapeURL(TrackingManager.Instance.nameScreen);

        yield return DBService.RequestDB(request, ParseResponse);
    }

    public static IEnumerator PutTheBeginTimeMissionInDB(string idMissionAccountRapport)
    {
        string request = "SetTimeBeginMission.php?" + "id_mission_account=" + UnityWebRequest.EscapeURL(idMissionAccountRapport.ToString());

        yield return DBService.RequestDB(request, ParseResponse);
    }

    public static IEnumerator PutTheEndTimeMissionInDB(string idMissionAccountRapport,int nbErrorSpeech,int nbErrorObject,int nbErrorAFK, float parcourLength)
    {
        //Debug.Log(idMissionAccountRapport+"IDMISSIONSACCOUNT");
        string request = "SetTimeEndMission.php?" + "id_mission_rapport=" + UnityWebRequest.EscapeURL(idMissionAccountRapport.ToString()) + "&nb_error_speech=" + UnityWebRequest.EscapeURL(nbErrorSpeech.ToString()) + "&nb_error_object=" + UnityWebRequest.EscapeURL(nbErrorObject.ToString()) + "&nb_error_afk=" + UnityWebRequest.EscapeURL(nbErrorAFK.ToString()) + "&parcour_length=" + UnityWebRequest.EscapeURL(parcourLength.ToString());

        yield return DBService.RequestDB(request, ParseResponse);
    }

    public static IEnumerator PutThePlayerPosInDB(string idScenarioRapport, float PosX, float PosY)
    {
        //Debug.Log(idMissionAccountRapport+"IDMISSIONSACCOUNT");
        string request = "AddPlayerPosToBDD.php?" + "id_scenario_rapport=" + UnityWebRequest.EscapeURL(idScenarioRapport.ToString()) + "&PosX=" + UnityWebRequest.EscapeURL(PosX.ToString()) + "&PosY=" + UnityWebRequest.EscapeURL(PosY.ToString());

        yield return DBService.RequestDB(request, ParseResponse);
    }

    private static void ParseResponse(string response)
    {
        //Debug.LogError(response+"WOZOOOOOOOOOO");
        responseDB = response;
    }
}
