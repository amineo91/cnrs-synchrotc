﻿using System;
using System.Collections;
using System.Globalization;
using UnityEngine;
using UnityEngine.Networking;

public class UserDetailsDBService
{
    public static DifficultyMode Difficulty;

    /// <summary>
    /// Select user details with specified ID
    /// </summary>
    /// <returns>The difficulty</returns>
    public static IEnumerator LoadUserDetailsFromDB(int idPatient)
    {
        string request = "GetUserDetails.php?" + "idPatient=" + UnityWebRequest.EscapeURL(idPatient.ToString());

        Difficulty = DifficultyMode.Easy;
        yield return DBService.RequestDB(request, ParseResponse);
    }

    private static void ParseResponse(string response)
    {
        string rowSeparator = "<html></br></br></html>";

        string[] splittedData = response.Split(new[] { rowSeparator }, StringSplitOptions.RemoveEmptyEntries);

        if (splittedData.Length > 0)
        {
            string data = splittedData[0].Trim();

            if (data.Length > 0)
            {
                string difficultyString = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(data.ToLower());
                Difficulty = (DifficultyMode) Enum.Parse(typeof(DifficultyMode), difficultyString);
            }
            else // No row, this case should never happen
            {
                Difficulty = DifficultyMode.Easy;
            }
        }
        else // Default response is " ", thus this case should never happen
        {
            Difficulty = DifficultyMode.Easy;
        }
    }
}
