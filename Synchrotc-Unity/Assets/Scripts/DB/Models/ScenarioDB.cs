﻿using System.Collections.Generic;

public class ScenarioDB
{
    public int idDay;

    public List<ObjectiveDB> objectivesDB = new List<ObjectiveDB>();

    public List<Item> getItemsToHaveFromOutdoor()
    {
        List<Item> items = new List<Item>();
        foreach (ObjectiveDB objectiveDB in objectivesDB)
        {
            items.AddRange(objectiveDB.getItemsToHaveFromOutdoor());
        }
        return items;
    }

    public List<Item> getItemsToHaveFromAppartment()
    {
        List<Item> items = new List<Item>();
        foreach (ObjectiveDB objectiveDB in objectivesDB)
        {
            items.AddRange(objectiveDB.getItemsToHaveFromAppartment());
        }
        return items;
    }

    public List<Item> getItemsToGive()
    {
        List<Item> items = new List<Item>();
        foreach (ObjectiveDB objectiveDB in objectivesDB)
        {
            items.AddRange(objectiveDB.getItemsToGive());
        }
        return items;
    }
}
