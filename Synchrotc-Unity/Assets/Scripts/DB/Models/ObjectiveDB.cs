﻿using System.Collections.Generic;

public class ObjectiveDB
{
    public string id = null;
    public string name = null;
    public string description = null;
    public string location = null;
    public string id_missionAccount = null;
    public List<ItemDB> itemsDB = new List<ItemDB>();

    public List<string> getItemNamesToHaveFromOutdoor()
    {
        List<string> items = new List<string>();
        foreach (ItemDB itemDB in itemsDB)
        {
            if (itemDB.action == "take" && itemDB.id_place != 1) // 1 = Appartment
            {
                items.Add(itemDB.name);
            }
        }
        return items;
    }

    public List<Item> getItemsToHaveFromOutdoor()
    {
        return ScriptableObjectService.getItemsByName(getItemNamesToHaveFromOutdoor());
    }

    public List<string> getItemNamesToHaveFromAppartment()
    {
        List<string> items = new List<string>();
        foreach (ItemDB itemDB in itemsDB)
        {
            if (itemDB.action == "take" && itemDB.id_place == 1) // 1 = Appartment
            {
                items.Add(itemDB.name);
            }
        }
        return items;
    }

    public List<Item> getItemsToHaveFromAppartment()
    {
        return ScriptableObjectService.getItemsByName(getItemNamesToHaveFromAppartment());
    }

    public List<string> getItemNamesToGive()
    {
        List<string> items = new List<string>();
        foreach (ItemDB itemDB in itemsDB)
        {
            if (itemDB.action == "give")
            {
                items.Add(itemDB.name);
            }
        }
        return items;
    }

    public List<Item> getItemsToGive()
    {
        return ScriptableObjectService.getItemsByName(getItemNamesToGive());
    }
}
