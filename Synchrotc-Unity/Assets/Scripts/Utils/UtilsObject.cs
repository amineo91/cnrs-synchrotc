﻿using System.Collections.Generic;
using UnityEngine;

public class UtilsObject : Object
{
    public static List<TakeAble> getTakeAblesByItem(Item item)
    {
        List<TakeAble> takeAbles = new List<TakeAble>(FindObjectsOfType<TakeAble>());
        return takeAbles.FindAll(takeAble => takeAble.getItem() == item);
    }
}
