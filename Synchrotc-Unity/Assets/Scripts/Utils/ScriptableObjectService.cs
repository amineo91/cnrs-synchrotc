﻿using Keyveo.Scenarisation;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ScriptableObjectService
{
    // --- Item ---

    public static Item getItemByName(string name)
    {
        return Resources.Load<Item>("Scriptables/Items/" + name);
    }

    public static List<Item> getItemsByName(List<string> names)
    {
        List<Item> items = new List<Item>();
        foreach (string name in names)
        {
            //Debug.Log(name);
            items.Add(getItemByName(name));
        }
        return items;
    }


    // --- Location ---

    public static Location getLocationByName(string name)
    {
        return Resources.Load<Location>("Scriptables/Locations/" + name);
    }


    // --- Scenario ---

    public static ScenarioSO getScenarioSOByName(string name)
    {
        return Resources.Load<ScenarioSO>("Scriptables/Scenarios/" + name);
    }

    public static ObjectiveSO getObjectiveSOByName(string name)
    {
        return Resources.Load<ObjectiveSO>("Scriptables/Scenarios/" + name);
    }

    public static StepSO getStepSOByName(string name)
    {
        return Resources.Load<StepSO>("Scriptables/Scenarios/" + name);
    }
}
