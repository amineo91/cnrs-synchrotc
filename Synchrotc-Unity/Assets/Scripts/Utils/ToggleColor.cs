﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleColor : MonoBehaviour
{
    public Image cadena;

    public void changeCadenaColor()
    {
        if (!gameObject.GetComponent<Toggle>().isOn)
        {
            cadena.color = gameObject.GetComponent<Toggle>().colors.pressedColor;
        }
        else
        {
            cadena.color = gameObject.GetComponent<Toggle>().colors.normalColor;
        }
        
    }
}
