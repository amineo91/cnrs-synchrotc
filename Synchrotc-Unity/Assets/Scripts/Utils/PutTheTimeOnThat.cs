﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PutTheTimeOnThat : MonoBehaviour
{

    private TextMeshProUGUI textHour;
    
    // Start is called before the first frame update
    void Start()
    {
        textHour = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        textHour.text = TimeManager.Instance.GetFormatedTime();
    }
}
