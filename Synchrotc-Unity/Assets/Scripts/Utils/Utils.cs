﻿using UnityEngine;

public class Utils
{
    public static GameObject FindObject(string name, bool includeInactives = true)
    {
        Transform[] trs = Resources.FindObjectsOfTypeAll<Transform>();
        foreach (Transform t in trs)
        {
            if (t.name == name)
            {
                return t.gameObject;
            }
        }
        return null;
    }
}
