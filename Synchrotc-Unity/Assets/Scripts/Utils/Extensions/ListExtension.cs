﻿using System.Collections.Generic;

public static class ListExtension
{

    public static bool ContainsAll<T>(this List<T> thisList, List<T> listOfItemsToSearchFor)
    {
        foreach(T item in listOfItemsToSearchFor)
        {
            if (thisList.Contains(item) == false)
            {
                return false;
            }
        }
        return true;
    }
}
