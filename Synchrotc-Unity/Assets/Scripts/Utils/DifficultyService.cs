﻿using UnityEngine;

/// <summary>
/// Gives available guidances for current difficulty mode
/// </summary>
public class DifficultyService : MonoBehaviour
{
    public static DifficultyMode GetDifficulty()
    {
        return FindObjectOfType<GameManager>().Difficulty;
    }

    public static bool HasWorldGuidances()
    {
        return GameManager.Instance.Difficulty <= DifficultyMode.Hard;
    }
    
    public static bool HasWorldLineGuidance()
    {
        return HasWorldGuidances() && GameManager.Instance.Difficulty <= DifficultyMode.Easy;
    }

    public static bool HasPhoneLineGuidance()
    {
        return HasWorldGuidances() && GameManager.Instance.Difficulty == DifficultyMode.Medium;
    }

    public static bool HasLocationStep()
    {
        return GameManager.Instance.Difficulty <= DifficultyMode.Medium;
    }

    public static bool HasTaskPanel()
    {
        return GameManager.Instance.Difficulty <= DifficultyMode.Medium;
    }

    public static bool HasChoiceMinimapWindow()
    {
        return GameManager.Instance.Difficulty >= DifficultyMode.Hard;
    }
}
