﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HomeManager : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Home;

    [SerializeField]
    private Button m_PlayButton, m_RestartButton, m_ResumeButton;

    private bool isHomeDisplayed;
    private bool isPlaying = false;

    private void Awake()
    {

    }

    private void Start()
    {
        z_Play();
        /*
        if (InitData.Instance.AutoStart)
        {
            z_Play();
        }
        else
        {
            // Ensure buttons are active correctly
            m_PlayButton.gameObject.SetActive(true);
            m_RestartButton.gameObject.SetActive(false);
            m_ResumeButton.gameObject.SetActive(false);

            // Ensure home is displayed
            ShowHome();
        }
        */
    }

    private void Update()
    {
        if (isPlaying && Input.GetKeyDown(KeyCode.Escape))
        {
            ToggleHome();
        }
    }

    /// <summary>
    /// Start the first game
    /// </summary>
    public void z_Play()
    {
        isPlaying = true;

        m_PlayButton.gameObject.SetActive(false);
        m_RestartButton.gameObject.SetActive(true);
        m_ResumeButton.gameObject.SetActive(true);

        InitData.Instance.AutoStart = true;

        HideHome();
        GameManager.Instance.ResumeGame();
    }

    /// <summary>
    /// Resume playing the current game
    /// </summary>
    public void z_Resume()
    {
        Resume();
    }

    /// <summary>
    /// Start a new game from the beginning
    /// </summary>
    public void z_Restart()
    {
        // Reload the scene
        GameManager.Instance.Replay();
    }

    /// <summary>
    /// Quit game
    /// </summary>
    public void z_Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
    }

    private void ToggleHome()
    {
        if (isHomeDisplayed)
        {
            Resume();
        }
        else
        {
            ShowHome();
        }
    }

    private void Resume()
    {
        HideHome();
        GameManager.Instance.ResumeGame();
    }

    private void ShowHome()
    {
        isHomeDisplayed = true;
        m_Home.SetActive(true);
        GameManager.Instance.PauseGame();
    }

    private void HideHome()
    {
        isHomeDisplayed = false;
        m_Home.SetActive(false);
    }
}
