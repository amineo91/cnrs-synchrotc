﻿using Keyveo.Scenarisation;
using Keyveo.Utilities;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public enum DifficultyMode
{
    Easy = 0,    // Full world guidances, directives, scenario step by step, including conditional ones
    Medium = 1,  // Full world guidances, scenario step by step, no conditionals
    Hard = 2,     // No guidances, scenario mono step
    None = 3     // Not logged
}

public class GameManager : Singleton<GameManager>
{
    [SerializeField]
    private Login m_LoginPanel;

    [SerializeField]
    public FirstPersonController m_FirstPersonController;

    [SerializeField]
    private bool isPaused = false;

    public bool IsPaused
    {
        get
        {
            return isPaused;
        }
    }

    [System.Obsolete("Use in Editor mode only")]
    public void SetDifficultyFromEditor(DifficultyMode difficulty)
    {
        InitData.Instance.Difficulty = difficulty;

        var timeMultiplier = TimeManager.Instance.DefaultTimeMultiplier;
        switch (difficulty)
        {
            case DifficultyMode.Easy:
                timeMultiplier = 5;
                break;
            case DifficultyMode.Medium:
                timeMultiplier = 10;
                break;
            case DifficultyMode.Hard:
                timeMultiplier = 15;
                break;
        }

        TimeManager.Instance.DefaultTimeMultiplier = timeMultiplier;
        TimeManager.Instance.timeMultiplier = timeMultiplier;
    }

    public DifficultyMode Difficulty
    {
        get { return InitData.Instance.Difficulty; }
    }

    public int m_IdDay = 8;

    public bool IsAuthenticated
    {
        get { return InitData.Instance.PatientId > 0; }
    }

    public bool isGameStarted = false;
    private ScenarioRecapPanel scenarioRecapPanel;


    protected override void Awake()
    {
        base.Awake();

        scenarioRecapPanel = FindObjectOfType<ScenarioRecapPanel>();
        scenarioRecapPanel.gameObject.SetActive(false);

        m_LoginPanel.gameObject.SetActive(true);
    }

    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void StartGame()
    {
        isGameStarted = true;
        UnpauseGame();
        if (IsAuthenticated)
        {
            LoadScenarioAsync();
            AFKManager.Instance.StartCoroutines();
        }
    }

    public void ResumeGame()
    {
        if (isGameStarted) {
            UnpauseGame();
        }
        else {
            if (IsAuthenticated)
            {
                StartGame();
            }
            else
            {
                m_LoginPanel.Show();
            }
        }
    }

    public void LoadScenarioAsync()
    {
        StartCoroutine(LoadScenario());
    }

    private IEnumerator LoadScenario()
    {
        int patientId = InitData.Instance.PatientId;
        string date = InitData.Instance.Date;


        yield return DayDBService.LoadDayFromDB(patientId/*, date*/);
        int idDay = DayDBService.idDay;

        DifficultyMode Difficulty = InitData.Instance.Difficulty;

        var timeMultiplier = TimeManager.Instance.DefaultTimeMultiplier;
        switch (Difficulty)
        {
            case DifficultyMode.Easy:
                timeMultiplier = 5;
                break;
            case DifficultyMode.Medium:
                timeMultiplier = 10;
                break;
            case DifficultyMode.Hard:
                timeMultiplier = 15;
                break;
        }

        TimeManager.Instance.DefaultTimeMultiplier = timeMultiplier;
        TimeManager.Instance.timeMultiplier = timeMultiplier;

        if (idDay > 0)
        {
            yield return ScenarioDBService.LoadScenarioFromDB(idDay);
            ScenarioDB scenarioDB = ScenarioDBService.scenarioDB;

            if (scenarioDB.objectivesDB.Count > 0)
            {
                //Debug.Log(scenarioDB.objectivesDB.Count + " mission" + (scenarioDB.objectivesDB.Count > 1 ? "s" : "") + " for patient '" + patientId + "' on day '" + idDay + "'");
                ScenarioFactory scenarioFactory = new ScenarioFactory();
                yield return scenarioFactory.Create(scenarioDB, Difficulty, false);
                ScenarioManager.Instance.m_scenario = scenarioFactory.scenarioSO;
            }
        }

        // No rush
        yield return new WaitForSeconds(1);

        // Go or no go
        if (ScenarioManager.Instance.m_scenario != null)
        {
            StartScenario();
        }
        else
        {
            //Debug.Log("No mission for patient '" + patientId + "' on day '" + idDay + "'");
            NotificationManager.Instance.Notify("Aucune mission aujourd'hui.");
            NavigationGuidanceManager.Instance.choiceMinimapWindow.SetActive(true);
        }
    }

    private void StartScenario()
    {
        scenarioRecapPanel.Clear();
        scenarioRecapPanel.InitTasks();
        scenarioRecapPanel.gameObject.SetActive(DifficultyService.HasTaskPanel());

        ScenarioManager.Instance.StartScenario();
    }

    public void OnScenarioComplete()
    {
        StartCoroutine(OnScenarioCompleteDelayed());
    }

    private IEnumerator OnScenarioCompleteDelayed()
    {
        // No rush
        yield return new WaitForSeconds(1);

        // Update UI
        scenarioRecapPanel.gameObject.SetActive(false);
        PhoneController.Instance.ReturnToHome();
        PhoneController.Instance.HideTaskRecapButton();

        // End of time
        TimeManager.Instance.GetComponent<AFKManager>().enabled = false;
        TimeManager.Instance.enabled = false;

        // TODO
        //Debug.LogError("TODO - Set day as success in DB");
        TrackingManager.Instance.TakeHiResShot();
        // No rush
        yield return new WaitForSeconds(2);
        StatManager.Instance.TimeEndScenario();
        //StartCoroutine(ErrorDBService.Instance.PutTheStateInDB("success"));
        NotificationManager.Instance.Notify("Bravo vous avez accompli toutes les tâches!", NotificationType.Success);

        NavigationGuidanceManager.Instance.choiceMinimapWindow.SetActive(true);
    }

    public void OnScenarioFail()
    {
        scenarioRecapPanel.gameObject.SetActive(false);
        StartCoroutine(ErrorDBService.Instance.PutTheStateInDB("fail"));
        StatManager.Instance.TimeEndScenario();
    }

    public void PauseGame()
    {
        isPaused = true;
        FreezePlayer();

        // TODO - Freeze in-game time
    }

    public void UnpauseGame()
    {
        isPaused = false;
        UnfreezePlayer();

        // TODO - Unfreeze in-game time
    }

    public void FreezePlayer()
    {
        m_FirstPersonController.enabled = false;
        m_FirstPersonController.m_MouseLook.SetCursorLock(false);
    }

    public void UnfreezePlayer()
    {
        m_FirstPersonController.enabled = true;
        m_FirstPersonController.m_MouseLook.SetCursorLock(true);
    }
}
