﻿using Keyveo.Utilities;
using System;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using Keyveo.Scenarisation;

public class FlouzManager : Singleton<FlouzManager>
{

    #region Declaration des variables
    [SerializeField]
    private Item wallet;
    [SerializeField]
    private Inventory inventory;
    [SerializeField]
    private float m_Money = 20f;
    [SerializeField]
    private Transform FlouzNotifier;
    [SerializeField]
    private GameObject FlouzGainPrefab;
    [SerializeField]
    private GameObject FlouzLostPrefab;
    
    private List<TakeAble> takeAblesWallet;

    [HideInInspector]
    public bool isBroken = false;

    private void Awake()
    {
        takeAblesWallet = UtilsObject.getTakeAblesByItem(wallet);
    }
    // Start is called before the first frame update
    void Start()
    {
        RefreshWallet();
        inventory.RefreshInventoryView();
    }

    // Update is called once per frame
    void Update()
    {
        SetIfUnderZero();
    }
    #endregion

    #region Public fonctions

    public Item GetWallet()
    {
        return wallet;
    }

    public float GetMoney()
    {
        return this.m_Money;
    }


    public void SetMoney(float pMoney)
    {
         this.m_Money = pMoney;
        RefreshWallet();
    }

    public void SetIfUnderZero()
    {
        if (GetMoney() < 0)
        {
            SetMoney(0f);
        }
    }

    public bool TryAddMoney(float MoneyToAdd)
    {
        if (inventory.Contains(wallet) == false)
        {
            NotificationManager.Instance.Notify("Vous n'avez pas votre portefeuille.", NotificationType.Error);
            return false;
        }
        else
        {
            AddMoney(MoneyToAdd);
            return true;
        }
    }

    public void Event_TryAddMoney(float MoneyToAdd)
    {
        TryAddMoney(MoneyToAdd);
    }

    public bool TryTakeOutMoney(float MoneyToTakeOut)
    {
        bool isSuccess = false;
        CheckMoneyThenDo(MoneyToTakeOut, () =>
        {
            TakeOutMoney(MoneyToTakeOut);
            isSuccess = true;
        });
        return isSuccess;
    }

    public void Event_TryTakeOutMoney(float MoneyToTakeOut)
    {
        TryTakeOutMoney(MoneyToTakeOut);
    }

    public bool TryBuyItem(Item item)
    {
        bool isSuccess = false;
        float MoneyToTakeOut = item.Price;

        isSuccess = CheckMoneyThenDo(MoneyToTakeOut, () =>
        {
            // If inventory operation is successfull
            if (inventory.TryAddObject(item))
            {
                // Validate transaction
                TakeOutMoney(MoneyToTakeOut);
                isSuccess = true;
            }
        });

        return isSuccess;
    }

    public bool TryBuyItems(List<Item> items)
    {
        bool isSuccess = false;
        float MoneyToTakeOut = 0;
        foreach(Item item in items)
        {
            MoneyToTakeOut += item.Price;
        }

        isSuccess = CheckMoneyThenDo(MoneyToTakeOut, () =>
        {
            // If inventory operation is successfull
            if (inventory.NbEmptyInventorySlots >= items.Count)
            {
                foreach(Item item in items)
                {
                    inventory.TryAddObject(item);
                }
                // Validate transaction
                TakeOutMoney(MoneyToTakeOut);
                isSuccess = true;
            }
        });

        return isSuccess;
    }

    public void Event_TryBuyItem(Item item)
    {
        TryBuyItem(item);
    }

    #endregion

    #region Private functions

    private void AddMoney(float MoneyToAdd)
    {
        this.m_Money += MoneyToAdd;
        NoticeFloozFlow(MoneyToAdd);
        RefreshWallet();
        inventory.RefreshInventoryView();
    }

    private void TakeOutMoney(float MoneyToTakeOut)
    {
        this.m_Money -= MoneyToTakeOut;
        NoticeFloozFlow(MoneyToTakeOut, false);
        RefreshWallet();
        inventory.RefreshInventoryView();

        //NotificationManager.Instance.Notify("Vous avez bien utilisé " + MoneyToTakeOut + " € de votre porte-feuille.", 2);
    }

    public bool CheckMoney(float MoneyToTakeOut)
    {
        if (MoneyToTakeOut == 0)
        {
            // Wallet is not mandatory if item is free
            return true;
        }
        if (inventory.Contains(wallet) == false)
        {
            //ErrorDBService.Instance.addErrorToObjectif();
            StatManager.Instance.nbErrorObjectCurrentObj++;
            NotificationManager.Instance.Notify("Vous n'avez pas votre portefeuille.", NotificationType.Error);
            return false;
        }
        else if (MoneyToTakeOut > GetMoney())
        {
            // ErrorDBService.Instance.addErrorToObjectif();
            StatManager.Instance.nbErrorObjectCurrentObj++;
            NotificationManager.Instance.Notify("Vous n'avez pas assez d'argent pour cette action.", NotificationType.Error);
            isBroken = true;
            return false;
        }
        else
        {
            return true;
        }
    }

    public bool CheckMoneyThenDo(float MoneyToTakeOut, Action successAction)
    {
        if (CheckMoney(MoneyToTakeOut))
        {
            successAction.Invoke();
            return true;
        }
        return false;
    }

    private void RefreshWallet()
    {
        wallet.ItemName = "Porte-feuille : " + GetMoney() + " €";
        foreach(TakeAble takeAble in takeAblesWallet)
        {
            takeAble.UpdateText();
        }
    }

    private void NoticeFloozFlow(float amount, bool Gain = true)
    {
        if (amount == 0)
        {
            return;
        }

        GameObject notifPrefab = null;

        string sign = null;
        
        if (Gain)
        {
            notifPrefab = FlouzGainPrefab;
            sign = "+";
        }
        else
        {
            notifPrefab = FlouzLostPrefab;
            sign = "-";
        }
        
        GameObject notif = Instantiate(notifPrefab,FlouzNotifier);
        notif.GetComponent<TextMeshProUGUI>().text = sign + amount + "€";
            
        var Rect = notif.transform.position;

        var tween = DOTween.To(() => Rect.y, x => Rect.y = x, -100, 3);
        
        tween.SetEase(Ease.Linear).OnUpdate(() =>
        {
            // Must rebuild layout to update visual
            notif.transform.position = Rect;
        }).OnComplete(() =>
        {
           Destroy(notif); 
        });

    }

    #endregion
}
