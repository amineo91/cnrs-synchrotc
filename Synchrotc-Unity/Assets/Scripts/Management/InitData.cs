﻿using Keyveo.ExtensionMethods;
using System;
using UnityEngine;

public class InitData : MonoBehaviour
{
    public bool AutoStart = true;
    public string Date;
    public int PatientId = -1;
    public DifficultyMode Difficulty = DifficultyMode.None;

    private static InitData instance;

    public static InitData Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<InitData>();
                if (instance == null)
                {
                    GameObject initDataGO = new GameObject();
                    initDataGO.name = "Init Data";
                    instance = initDataGO.AddComponent<InitData>();
                }
            }
            return instance;
        }
    }

    private void Awake()
    {
        this.DontDestroyOnLoad();

        // Init current Date
        DateTime localDate = DateTime.Now;
        Date = localDate.ToString("yyyy-MM-dd");
    }
}
