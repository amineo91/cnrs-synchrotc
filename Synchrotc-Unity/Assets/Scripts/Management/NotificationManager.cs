﻿using Keyveo.Utilities;
using System.Collections;
using UnityEngine;

public enum NotificationType
{
    Default,
    Error,
    Success
}

public class NotificationManager : Singleton<NotificationManager>
{
    [SerializeField]
    private NotificationArea m_NotificationArea;


    public void Notify(string message, NotificationType notificationType = NotificationType.Default, float duration = -1)
    {
        if (m_NotificationArea.isNotificationAlreadyDisplayed(message) == false)
        {
            m_NotificationArea.Add(message, duration, notificationType);
        }
    }

    public void Notify(string message, float delay, NotificationType notificationType = NotificationType.Default, float duration = -1)
    {
        if (delay > 0)
        {
            StartCoroutine(CoroutineNotify(message, delay, notificationType, duration));
        }
        else
        {
            Notify(message, duration, notificationType);
        }
    }

    public IEnumerator CoroutineNotify(string message, float delay, NotificationType notificationType = NotificationType.Default, float duration = -1)
    {
        yield return new WaitForSeconds(delay);

        m_NotificationArea.Add(message, duration, notificationType);
    }
}
