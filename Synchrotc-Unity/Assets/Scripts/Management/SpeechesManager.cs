﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;
using System.Linq;
using UnityEditor;
using UnityEngine.Networking;
using Keyveo.Scenarisation;

public class SpeechesManager : MonoBehaviour
{
    public static SpeechesManager Instance;

    [SerializeField]
    private GameObject SpeechesWindows;

    [SerializeField]
    private ScrollRect SpeechesScrollRect;

    /*[SerializeField]
    private GameObject PlayerWindows;*/

    [SerializeField]
    private GameObject PlayerLinePrefab;

    [SerializeField]
    private GameObject NPCLinePrefab;

    [SerializeField]
    private GameObject PlayerChoiceLinePrefab;

    [SerializeField]
    private float FeedbackInvalidAnswerDuration = 0.4f;

    [SerializeField]
    private Color FeedbackInvalidAnswerColor = Color.red;

    [SerializeField]
    private GameObject PlayerSpeechesChoices;

    public UIMover UIMoverPlayerChoice;
    
    [SerializeField]
    public Transform TransformPlayer;
    
    private bool isLookAt = false;

    Quaternion initialNeckRotation;

    [SerializeField]
    private bool m_IsVerbose = false;

    private List<Tween> tweens = new List<Tween>();

    private string secretKey = "Madness7SalutesY0u";

    private Transform PNJNeck;

    public List<speechesNPC> speechesNPCList = new List<speechesNPC>();
    public List<speechesPlayer> speechesPlayerList = new List<speechesPlayer>();
    public List<joinSpeechPlayerNPC> joinSpeechPlayerNPCList = new List<joinSpeechPlayerNPC>();
    public List<joinSpeechNPCPlayer> joinSpeechNPCPlayerList = new List<joinSpeechNPCPlayer>();

    public List<objectEffect> listObjectEffects = new List<objectEffect>();

    public BoxCollider colliderToReactivate = null;

    /// <summary>
    /// Data from BDD, NPC response to player
    /// </summary>
    public class joinSpeechPlayerNPC
    {
        public int idSpeechPlayer;
        public int idSpeechNPC;
        public string nameObject;
        public string action;
        public string effect;
    }

    /// <summary>
    /// Data from BDD, player response to NPC 
    /// </summary>
    public class joinSpeechNPCPlayer
    {
        public int idSpeechNPC;
        public int idSpeechPlayer;
        public List<string> nameObjects;
        public string action;
        public string effect;
    }

    /// <summary>
    /// Data from BDD, player speeches
    /// </summary>
    public class speechesPlayer
    {
        public int idNPC;
        public string nameNPC;
        public int idTextPlayer;
        public string textPlayer;
    }

    /// <summary>
    /// Data from BDD, NPC speeches
    /// </summary>
    public class speechesNPC
    {
        public int idNPC;
        public string nameNPC;
        public int idTextNPC;
        public int idScenario;
        public string textNPC;
    }

    public class objectEffect
    {
        public string name;
        public string effect;
    }
    
    private void Awake()
    {
        Instance = this;
        clearUI();
    }

    public bool isReady = true;

    /// <summary>
    /// Starts the conversation, call by trigger enter near a NPC
    /// </summary>
    public void StartConversation(string nameNPC)
    {
        if (isReady)
        {
            GameManager.Instance.FreezePlayer(); // TODO - Pause game instead ?
            StartCoroutine(Connection(nameNPC));
        }

    }
    
    /// <summary>
    /// Reactivate a collider if needed (dirty but it works fine)
    /// </summary>
    public void ReactivateCollider(GameObject objectWithCol = null)
    {
        if (objectWithCol != null && objectWithCol.GetComponent<BoxCollider>() != null )
        {
            colliderToReactivate = objectWithCol.GetComponent<BoxCollider>();
        }
    }

    /// <summary>
    /// End the conversation, call by trigger exit near a NPC and at the end of the discution
    /// </summary>
    public void EndConversation()
    {
        // Ensure chained tweens are killed
        foreach (Tween tween in tweens)
        {
            if (tween != null)
            {
                tween.Kill();
            }
        }
        tweens.Clear();
        hideTheSentenceAnimation();
        clearUI();
        resetHead();
        GameManager.Instance.UnfreezePlayer(); // TODO - Resume game instead ?
    }

    private void clearUI()
    {
        var container = PlayerSpeechesChoices.transform.GetChild(0);

        for (var i = 0; i < container.childCount; i++)
        {
            Destroy(container.GetChild(i).gameObject);
        }

        for (var i = 0; i < SpeechesWindows.transform.childCount; i++)
        {
            Destroy(SpeechesWindows.transform.GetChild(i).gameObject);
        }

        for (var i = 0; i < SpeechesWindows.transform.childCount; i++)
        {
            Destroy(SpeechesWindows.transform.GetChild(i).gameObject);
        }
    }

    /// <summary>
    /// Display the speeches choice windows
    /// </summary>
    public Tweener chooseASentenceAnimation()
    {
        return UIMoverPlayerChoice.Show();
    }

    /// <summary>
    /// Hide the speeches choice windows
    /// </summary>
    public Tweener hideTheSentenceAnimation()
    {
        return UIMoverPlayerChoice.Hide();
    }

    /// <summary>
    /// Display the NPC or player speech line
    /// </summary>
    public Tween sendMessage(string text, GameObject LinePrefab, bool isNPC)
    {
        var speechLine = CreateSpeechLine(text, LinePrefab, SpeechesWindows.transform);

        // Animate scroll rect to last speech line
        SpeechesScrollRect.DOVerticalNormalizedPos(0, 0.3f);

        return AnimateSpeechLine(speechLine, isNPC, false);
    }

    /// <summary>
    /// Display the NPC speech line
    /// </summary>
    public Tween sendNPCMessage(string text)
    {
        return sendMessage(text, NPCLinePrefab, true);
    }

    /// <summary>
    /// Display the player speech line
    /// </summary>
    public Tween sendPlayerMessage(string text)
    {
        return sendMessage(text, PlayerLinePrefab, false);
    }

    /// <summary>
    /// Add the corresponding speech line to the player response windows
    /// </summary>
    private Tween AddChoiceToUI(int idText, string text, List<string> objectNames = null, string action = null, string effect = null)
    {
        var speechLine = CreateSpeechLine(text, PlayerChoiceLinePrefab, PlayerSpeechesChoices.transform.GetChild(0));
        speechLine.GetComponent<Button>().onClick.AddListener(() => {
            bool isSuccess = ChooseASentence(idText, text, objectNames, action, effect);
            if (isSuccess == false)
            {
                speechLine.GetComponent<SpeechBox>().PulseOnce(FeedbackInvalidAnswerColor, FeedbackInvalidAnswerDuration);
            }
        });
        return AnimateSpeechLine(speechLine, false);
    }

    private GameObject CreateSpeechLine(string text, GameObject prefabLigneText, Transform parent, bool isFirstSibling = true)
    {
        GameObject previousChild = null;
        
        if (parent.childCount - 1 >= 0 )
        {
            previousChild = parent.GetChild(parent.childCount - 1).gameObject;
        }
        
        
        var speechLine = Instantiate(prefabLigneText, parent);
        
        /*if (isFirstSibling) //por qué ?
        {
            speechLine.transform.SetAsFirstSibling();
        }
        LayoutRebuilder.ForceRebuildLayoutImmediate(speechLine.GetComponent<RectTransform>());*/
        
        
        if (previousChild != null && previousChild.name == PlayerLinePrefab.name+"(Clone)")
        {
            RectOffset tempPadding = new RectOffset(
                previousChild.GetComponent<VerticalLayoutGroup>().padding.left,
                previousChild.GetComponent<VerticalLayoutGroup>().padding.right,
                previousChild.GetComponent<VerticalLayoutGroup>().padding.top,
                previousChild.GetComponent<VerticalLayoutGroup>().padding.bottom);
            tempPadding.left = 100;
            
            previousChild.GetComponent<VerticalLayoutGroup>().padding = tempPadding;
            
            //TODO: tween the shit out of that padding
            
           /* var tween = DOTween.To(() => tempPadding.left, x => tempPadding.left = x, 100, 0.5f);
            
            tween.SetEase(Ease.Linear).OnUpdate(() =>
            {
                previousChild.GetComponent<VerticalLayoutGroup>().padding = tempPadding;
            });*/
           
        }

        var textComponent = speechLine.GetComponentInChildren<TextMeshProUGUI>();
        textComponent.SetText(text);

        LayoutRebuilder.ForceRebuildLayoutImmediate(parent.GetComponent<RectTransform>());

        return speechLine;
    }

    private Tween AnimateSpeechLine(GameObject speechLine, bool isLeft, bool isChoice = true)
    {
        VerticalLayoutGroup layoutGroup = speechLine.transform.GetComponent<VerticalLayoutGroup>();

        // Reset to "out" state immediately
        if (isLeft)
        {
            layoutGroup.padding.right = 600;
        }
        else
        {
            layoutGroup.padding.left = 600;
        }

        // Animate to "in" state
        RectTransform rect = layoutGroup.GetComponent<RectTransform>();
        Tween tween = null;
        if (isLeft)
        {
            tween = DOTween.To(() => layoutGroup.padding.right, x => layoutGroup.padding.right = x, 0, 1);
        }
        else
        {
            tween = DOTween.To(() => layoutGroup.padding.left, x => layoutGroup.padding.left = x, isChoice ? 0 : 100, 1);
        }
        
        tween.SetEase(Ease.InOutBack).OnUpdate(() =>
        {
            // Must rebuild layout to update visual
            LayoutRebuilder.ForceRebuildLayoutImmediate(rect);
        });
        return tween;
    }

    /// <summary>
    /// Get the datas from the BDD and split them
    /// </summary>
    private void getDataFromBDD(string allTables)
    {
        speechesNPCList.Clear();
        speechesPlayerList.Clear();
        joinSpeechPlayerNPCList.Clear();
        joinSpeechNPCPlayerList.Clear();

        string[] splittedTables = allTables.Split(new[] { "<html></br></br></html>" }, StringSplitOptions.None);      

        string[] speechesNPC = null;
        string[] speechesPlayer = null;
        string[] joinPlayerNPC = null;
        string[] joinNPCPlayer = null;

        for (var i = 0; i < splittedTables.Length; i++)
        {
            switch (i)
            {
                case 0:
                    speechesNPC = splittedTables[i].Split(new[] { "<html></br></html>" }, StringSplitOptions.None);
                    break;
                case 1:
                    speechesPlayer = splittedTables[i].Split(new[] { "<html></br></html>" }, StringSplitOptions.None);
                    break;
                case 2:
                    joinPlayerNPC = splittedTables[i].Split(new[] { "<html></br></html>" }, StringSplitOptions.None);
                    break;
                case 3:
                    joinNPCPlayer = splittedTables[i].Split(new[] { "<html></br></html>" }, StringSplitOptions.None);
                    break;
                
            }
        }

        var count = 0;
        var countThreshold = 0;

        int idNPC = 0;
        string nameNPC = "";
        int idTextNPC = 0;
        int idScenario = 0;
        string textNPC = "";

        int idTextPlayer = 0;
        string textPlayer = "";

        int idspeechesnpc = 0;
        int idspeechesPlayer = 0;
        string nameObject = "";
        string action = "";
        string effect = "";
        

        foreach (string field in speechesNPC)
        {
            switch (count - countThreshold)
            {
                case 0:
                    Int32.TryParse(field, out idNPC);
                    break;
                case 1:
                    nameNPC = field;
                    break;
                case 2:
                    Int32.TryParse(field, out idTextNPC);
                    break;
                case 3:
                    Int32.TryParse(field, out idScenario);
                    break;
                case 4:
                    textNPC = field;
                    speechesNPC tempclass = new speechesNPC();
                    tempclass.idNPC = idNPC;
                    tempclass.nameNPC = nameNPC;
                    tempclass.idTextNPC = idTextNPC;
                    tempclass.textNPC = textNPC;
                    tempclass.idScenario = idScenario;
                    speechesNPCList.Add(tempclass);
                    countThreshold = count + 1;
                    break;
            }

            count++;

        }

        count = 0;
        countThreshold = 0;

        foreach (string field in speechesPlayer)
        {
            switch (count - countThreshold)
            {
                case 0:
                    Int32.TryParse(field, out idNPC);
                    break;
                case 1:
                    nameNPC = field;
                    break;
                case 2:
                    Int32.TryParse(field, out idTextPlayer);
                    break;
                case 3:
                    textPlayer = field;
                    speechesPlayer tempclass = new speechesPlayer();
                    tempclass.idNPC = idNPC;
                    tempclass.nameNPC = nameNPC;
                    tempclass.idTextPlayer = idTextPlayer;
                    tempclass.textPlayer = textPlayer;
                    speechesPlayerList.Add(tempclass);
                    countThreshold = count + 1;
                    break;
            }

            count++;

        }

        count = 0;
        countThreshold = 0;
        
        string nameObject2 = "", nameObject3 = "";
        foreach (string field in joinNPCPlayer)
        {
            switch (count - countThreshold)
            {
                case 0:
                    Int32.TryParse(field, out idspeechesnpc);
                    break;
                case 1:
                    Int32.TryParse(field, out idspeechesPlayer);
                    break;
                case 2:
                    nameObject = field;
                    break;
                case 3:
                    nameObject2 = field;
                    break;
                case 4:
                    nameObject3 = field;
                    break;
                case 5:
                    action = field;
                    break;
                case 6:
                    effect = field;
                    joinSpeechNPCPlayer tempclass = new joinSpeechNPCPlayer();
                    tempclass.idSpeechNPC = idspeechesnpc;
                    tempclass.idSpeechPlayer = idspeechesPlayer;
                    tempclass.nameObjects = new List<string>();
                    if (String.IsNullOrEmpty(nameObject) == false)
                    {
                        tempclass.nameObjects.Add(nameObject);
                    }
                    if (String.IsNullOrEmpty(nameObject2) == false)
                    {
                        tempclass.nameObjects.Add(nameObject2);
                    }
                    if (String.IsNullOrEmpty(nameObject3) == false)
                    {
                        tempclass.nameObjects.Add(nameObject3);
                    }
                    tempclass.action = action;
                    tempclass.effect = effect;
                    var _objecteffect = new objectEffect();
                    _objecteffect.name = nameObject;
                    _objecteffect.effect = effect;
                    listObjectEffects.Add(_objecteffect);
                    joinSpeechNPCPlayerList.Add(tempclass);
                    countThreshold = count + 1;
                    break;
            }

            count++;

        }

        count = 0;
        countThreshold = 0;
        
        foreach (string field in joinPlayerNPC)
        {
            switch (count - countThreshold)
            {
                case 0:
                    Int32.TryParse(field, out idspeechesPlayer);
                    break;
                case 1:
                    Int32.TryParse(field, out idspeechesnpc);
                    break;
                case 2:
                    nameObject = field;
                    break;
                case 3:
                    action = field;
                    break;
                case 4:
                    effect = field;
                    joinSpeechPlayerNPC tempclass = new joinSpeechPlayerNPC();
                    tempclass.idSpeechPlayer = idspeechesPlayer;
                    tempclass.idSpeechNPC = idspeechesnpc;
                    tempclass.nameObject = nameObject;
                    tempclass.action = action;
                    tempclass.effect = effect;
                    var _objecteffect = new objectEffect();
                    _objecteffect.name = nameObject;
                    _objecteffect.effect = effect;
                    listObjectEffects.Add(_objecteffect);
                    joinSpeechPlayerNPCList.Add(tempclass);
                    countThreshold = count + 1;
                    break;
            }

            count++;

        }
    }

    /// <summary>
    /// Connect to the BDD and retrieve the results from the PHP file it calls 
    /// </summary>
    IEnumerator Connection(string nameNPC)
    {
        string request = "GetTheSpeeches.php?" + "namenpc=" + UnityWebRequest.EscapeURL(nameNPC);
          
        yield return DBService.RequestDB(request, response =>
        {
            getDataFromBDD(response);
            BeginConversation();
        });
    }

    /// <summary>
    /// Call the first message from the NPC the player is near
    /// </summary>
    private void BeginConversation()
    {
        bool foundIt = false;

        if (speechesNPCList.Count() == 0)
        {
            GameManager.Instance.UnfreezePlayer();
            NotificationManager.Instance.Notify("Cette personne ne fait pas parti de vos objectifs");
            StartCoroutine(WaitTillTheEnd());
            return;
        }

        if (FlouzManager.Instance.gameObject.GetComponent<FlouzScenarisationTarget>().enabled)
        {
            GameManager.Instance.UnfreezePlayer();
            NotificationManager.Instance.Notify("Cette personne ne fait pas parti de vos objectifs");
            StartCoroutine(WaitTillTheEnd());
        }
        else
        {
            foreach (speechesNPC speech in speechesNPCList)
            {
                if (ScenarioManager.Instance.m_currentObjective.m_registeredComponent.GetComponent<dataObjectif>().idScenario == speech.idScenario.ToString())//speech.idNPC == 1) // TODO - Fabrice: why check idNPC ? aurélien: je sais plus, aurélien maintenant : c t pour checker si ça correpond bien au scenar
                {
                    sendNPCMessage(speech.textNPC);
                    GetChoiceForPlayer(speech.idTextNPC);
                    foundIt = true;
                    break;
                }
            }
        }

        

        if (!foundIt)
        {
            GameManager.Instance.UnfreezePlayer();
            NotificationManager.Instance.Notify("Cette personne ne fait pas parti de vos objectifs");
            StartCoroutine(WaitTillTheEnd());
        }
        else
        {
            foundIt = false;
        }
    }

    /// <summary>
    /// Get differents answer from the player speeches for the given NPC speech id
    /// </summary>
    private void GetChoiceForPlayer(int idNPCSpeech)
    {
        //chooseASentenceAnimation();

        var container = PlayerSpeechesChoices.transform.GetChild(0);

        for (var i = 0; i <container.childCount;i++)
        {
            Destroy(container.GetChild(i).gameObject);
        }

        bool hasAnyChoice = false;
        
        foreach (joinSpeechNPCPlayer speechNPCPlayer in joinSpeechNPCPlayerList)
        {
            if (speechNPCPlayer.idSpeechNPC == idNPCSpeech)
            {
                foreach (speechesPlayer responseSpeech in speechesPlayerList)
                {
                    if (responseSpeech.idTextPlayer == speechNPCPlayer.idSpeechPlayer)
                    {
                        hasAnyChoice = true;
                        chooseASentenceAnimation();
                        AddChoiceToUI(responseSpeech.idTextPlayer, responseSpeech.textPlayer, speechNPCPlayer.nameObjects, speechNPCPlayer.action, speechNPCPlayer.effect);
                    }
                }
            }
        }
        
        if (hasAnyChoice == false)
        {
            StartCoroutine(WaitTillTheEnd());
        }
        else
        {
            // TODO - keep it ?
            //// Add a way to end conversation whenever player wants to
            //var speechLine = CreateSpeechLine("Au revoir.", PlayerChoiceLinePrefab, PlayerSpeechesChoices.transform.GetChild(0), false);
            //speechLine.GetComponent<Button>().onClick.AddListener(() => EndConversation());
            //AnimateSpeechLine(speechLine, false);
        }

        LayoutRebuilder.ForceRebuildLayoutImmediate(PlayerSpeechesChoices.GetComponent<RectTransform>());
        LayoutRebuilder.ForceRebuildLayoutImmediate(PlayerSpeechesChoices.transform.GetChild(0).GetComponent<RectTransform>());
    }

    /// <summary>
    /// Coroutine to delay the last sentence from the NPC
    /// </summary>
    private IEnumerator WaitTillTheEnd()
    {
        isReady = false;
        StartCoroutine(waitIsReady());
        yield return new WaitForSeconds(2);
        EndConversation();
    }

    /// <summary>
    /// Get the answer from the NPC for the chosen speeche of the player
    /// </summary>
    private void GetNPCMessage(int idText)
    {
        int idSpeechPlayer = 0;

        foreach (speechesPlayer speech in speechesPlayerList)
        {
            if(speech.idTextPlayer == idText)
            {
                idSpeechPlayer = speech.idTextPlayer;
            }
        }

        bool hasAnyChoice = false;
        foreach (joinSpeechPlayerNPC speechPlayerNPC in joinSpeechPlayerNPCList)
        {
            if (speechPlayerNPC.idSpeechPlayer == idSpeechPlayer)
            {
                foreach (speechesNPC responseSpeech in speechesNPCList)
                {
                    if (responseSpeech.idTextNPC == speechPlayerNPC.idSpeechNPC)
                    {
                        hasAnyChoice = true;
                        if (String.IsNullOrEmpty(speechPlayerNPC.nameObject) == false)
                        {
                            DealWithObjects(new List<string>() { speechPlayerNPC.nameObject }, speechPlayerNPC.action);
                        }
                        sendNPCMessage(responseSpeech.textNPC);
                        GetChoiceForPlayer(responseSpeech.idTextNPC);
                    }
                }
            }
        }

        if (hasAnyChoice == false)
        {
            StartCoroutine(WaitTillTheEnd());
        }
    }

    /// <summary>
    /// Animate all the windows when a speech is chose by the player and deal with the 
    /// </summary>
    public bool ChooseASentence(int idText, string text, List<string> objectNames, string action, string effect)
    {
        var isSuccess = true;

        if(effect == "fail" || effect == "error")
        {
            StatManager.Instance.nbErrorSpeechCurrentObj++;
        }

        if (objectNames != null && objectNames.Count > 0 && action != null)
        {
            isSuccess = DealWithObjects(objectNames, action);
        }

        if (isSuccess)
        {
            tweens.Add(hideTheSentenceAnimation().OnComplete(()
                => tweens.Add(sendPlayerMessage(text).OnComplete(()
                    => GetNPCMessage(idText)
                ))
            ));
        }

        return isSuccess;
    }
    
    /// <summary>
    /// Add or retrieve the given objects form the inventory 
    /// </summary> 
    public bool DealWithObjects(List<string> objectNames, string action)
    {
        if (objectNames == null || objectNames.Count == 0 || action == null)
        {
            return false;
        }

        List<Item> items = ScriptableObjectService.getItemsByName(objectNames);

        switch (action)
        {
            case "donner":
            case "verifierPresence":
                {
                    // Check if all items are in inventory
                    if (Inventory.Instance.ContainsAll(items))
                    {
                        float price = 0;
                        foreach (Item item in items)
                        {
                            price += item.Price;
                        }

                        // Check if player has enough money
                        if (FlouzManager.Instance.CheckMoney(price))
                        {
                            if (action == "donner")
                            {
                                FlouzManager.Instance.TryTakeOutMoney(price); // always true
                                Inventory.Instance.RemoveObjects(items); // always true
                                return true;
                            }
                            else // verifierPresence
                            {
                                return true;
                            }
                        }
                        else
                        {
                            // Not enough money

                            return false;
                        }
                    }
                    else
                    {
                        // One or more item is missing
                        NotificationManager.Instance.Notify("Vous n'avez pas tous les objets requis pour cette action.", NotificationType.Error);
                        return false;
                    }
                }
            case "prendre":
                {
                    // Check if player has enough money and space in bags
                    var isBuyable = FlouzManager.Instance.TryBuyItems(items);
                    return isBuyable;
                }
            default:
                {
                    //Debug.LogError("Unkown action '" + action + "'");
                    return false;
                }
        }
    }

    public void mooveHead(GameObject head)
    {
        if (!isLookAt && isReady)
        {
            Transform neck = head.transform.parent;

            initialNeckRotation = neck.rotation;
            neck.GetComponentInParent<Animator>().enabled = false;

            // Make the NPC 'neck' look at the center of player
            // So that his 'head' is looking at player's head
            Vector3 targetPlayerPosition = TransformPlayer.position - Vector3.up * 0.7f;

            // Ensure rotation is clamped
            Quaternion fromRotation = neck.rotation;
            neck.LookAt(targetPlayerPosition);
            Quaternion toRotation = neck.rotation;
            neck.rotation = fromRotation;
            Quaternion clampedRotation = Quaternion.RotateTowards(fromRotation, toRotation, 40);

            // Animate rotation
            neck.DORotateQuaternion(clampedRotation, 1);

            isLookAt = true;
            PNJNeck = neck;
        }else if (!isReady)
        {
            if (colliderToReactivate != null)
            {
                colliderToReactivate.enabled = true;
            }
        }
    }

    public void resetHead()
    {
        if (colliderToReactivate != null)
        {
            colliderToReactivate.enabled = true;
        }

        if (isLookAt && PNJNeck != null)
        {
            PNJNeck.DORotateQuaternion(initialNeckRotation, 1).OnComplete(() =>
            {
                PNJNeck.GetComponentInParent<Animator>().enabled = true;
                isLookAt = false;
            });
        }
    }

    private IEnumerator waitIsReady()
    {
        yield return new WaitForSeconds(4);
        isReady = true;
    }
}
