﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [Header("UI Elements")]  
    [SerializeField]
    private GameObject Home = null;

    [SerializeField]
    private GameObject Paramètres = null;

    [SerializeField]
    private GameObject LoadingGame = null;


    [Header("DropDown Resolution")]
    public TMPro.TMP_Dropdown dropDownResolution;

    private Resolution[] resolutions;

    [Header("DropDown Quality")]
    public TMPro.TMP_Dropdown dropDownGraphicsQuality;

    [Header("Progress Bar")]

    [SerializeField]
    private Image ProgBar = null;

    [SerializeField]
    private TMPro.TMP_Text progressValueTxt = null;

    private void Start()
    {
        Home.SetActive(true);
        Paramètres.SetActive(false);
        LoadingGame.SetActive(false);

        Screen.fullScreen = true;

        resolutions = Screen.resolutions.Select(Resolution => new Resolution { width = Resolution.width, height = Resolution.height }).Distinct().ToArray();
        dropDownResolution.ClearOptions();

        List<string> options = new List<string>();

        int myResolution = 0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height;
            options.Add(option);

            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                myResolution = i;
            }
        }

        dropDownResolution.AddOptions(options);
        dropDownResolution.value = myResolution;
        dropDownResolution.RefreshShownValue();

        dropDownGraphicsQuality.value = QualitySettings.GetQualityLevel();
    }

    public void StartGame()
    {
        Home.SetActive(false);
        Paramètres.SetActive(false);
        LoadingGame.SetActive(true);
        StartCoroutine(LoadGameScene());
    }

    public void HomeToSettings()
    {
        Home.SetActive(false);
        Paramètres.SetActive(true);
    }

    public void SettingsToHome()
    {
        Paramètres.SetActive(false);
        Home.SetActive(true);
    }

    public void SetFullScreen()
    {
        Screen.fullScreen = !Screen.fullScreen;
    }


    public void SetResolution()
    {
        Resolution resolution = resolutions[dropDownResolution.value];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    public void SetgraphicsQuality()
    {
        QualitySettings.SetQualityLevel(dropDownGraphicsQuality.value);
    }

    public void LeaveGame()
    {
        Application.Quit();
    }

    private IEnumerator LoadGameScene()
    {
        AsyncOperation result = SceneManager.LoadSceneAsync(1);

        while (!result.isDone)
        {
            float progress = Mathf.Clamp01(result.progress / 0.9f);
            ProgBar.fillAmount = progress;
            progressValueTxt.text = (int)(progress*100)+" %";
            yield return null;
        }
    }

}
