﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatManager : MonoBehaviour
{

    public string ScenarioEntryId;
    public StatusScenario status;
    public string cause;

    public static StatManager Instance = null;

    private List<string> alreadyBeganMission = new List<string>();
    private List<string> alreadyEndedMission = new List<string>();

    public string currentRapportId;

    public int nbErrorSpeechCurrentObj = 0;
    public int nbErrorObjectCurrentObj = 0;
    public int nbErrorAFKCurrentObj = 0; 
    //public int nbErrorTimeCurrentObj = 0;

    public enum StatusScenario
    {
        success,
        fail
    }

    public class MissionsRapport
    {
        int id_mission_account;
        string status;
        string fail_cause;
        int parcour_length;
    }

    private void Awake()
    {
        Instance = this;
    }


    public void BeginScenarioTime()
    {
        ////Debug.LogError("BeginScenario");
        StartCoroutine(BeginScenarioTimeCoRout());
    }

    public IEnumerator BeginScenarioTimeCoRout()
    {
        yield return TimeStatDBService.PutTheBeginTimeScenarioInDB(DayDBService.idDay);
        ScenarioEntryId = TimeStatDBService.responseDB;
        ////Debug.LogError(ScenarioEntryId + "WAZAAAAAAA");
    }

    public void TimeBeginMission(string idAccountMission)
    {
        if (!alreadyBeganMission.Contains(idAccountMission))
        {
            ////Debug.LogError("BeginMission");
            alreadyBeganMission.Add(idAccountMission);
            StartCoroutine(BeginMissionCorout(idAccountMission));
        }
    }

    public void TimeEndMission(string idAccountMission)
    {
        if (!alreadyEndedMission.Contains(idAccountMission))
        {
            ////Debug.LogError("EndMission");
            alreadyBeganMission.Add(idAccountMission);
            StartCoroutine(EndMissionCorout());
        }
    }

    public void TimeEndScenario()
    {
        if (cause == null || cause == "")
        {
            StartCoroutine(EndScenarioTimeCoRout());
        }
        else
        {
            StartCoroutine(EndScenarioTimeAndStatusCoRout());
        }
    }

    public IEnumerator EndScenarioTimeCoRout()
    {
        yield return TimeStatDBService.PutTheEndTimeScenarioInDB(ScenarioEntryId);
    }

    public IEnumerator EndScenarioTimeAndStatusCoRout()
    {
        yield return TimeStatDBService.PutTheEndTimeAndStatusScenarioInDB(ScenarioEntryId,cause);
    }

    public IEnumerator EndMissionCorout(/*string currentRapportId,int nbError*/)
    {
        float MissionParcourLength = TrackingManager.Instance.GetParcourLengthSinceLastTime();
        Debug.Log("UpdateThings");
        yield return TimeStatDBService.PutTheEndTimeMissionInDB(currentRapportId, nbErrorSpeechCurrentObj, nbErrorObjectCurrentObj, nbErrorAFKCurrentObj, MissionParcourLength); 
    }

    public IEnumerator BeginMissionCorout(string missionAccountID)
    {
        yield return TimeStatDBService.PutTheBeginTimeMissionInDB(missionAccountID);
        currentRapportId = TimeStatDBService.responseDB;
        nbErrorSpeechCurrentObj = 0;
        nbErrorObjectCurrentObj = 0;
        nbErrorAFKCurrentObj = 0;

        ////Debug.Log(currentRapportId);
        ////Debug.Log(missionAccountID + "SCENARID");
    }
}
