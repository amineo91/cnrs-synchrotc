﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;
using TMPro;

namespace Keyveo.Features.Supervision {
    [CreateAssetMenu(fileName = "NewSupervisionSkin", menuName = "Keyveo/Supervision/Skin")]
    public class SupervisionSkin : SerializedScriptableObject {


        public class ApplicableColor {
            public string filter = "";

            [ColorUsage(true, false)]
            public Color color;
        }

        public List<ApplicableColor> panelColors = new List<ApplicableColor>();
        public List<ApplicableColor> textColors = new List<ApplicableColor>();


        public void Apply(Transform parent) {
            foreach (ApplicableColor c in panelColors) {
                foreach (Transform tr in FindChildRec(parent, c.filter)) {
                    tr.GetComponent<Image>().color = c.color;
                }
            }
            foreach (ApplicableColor c in textColors) {
                foreach (Transform tr in FindChildRec(parent, c.filter)) {
                    if (tr.GetComponent<TextMeshProUGUI>() != null)
                        tr.GetComponent<TextMeshProUGUI>().color = c.color;
                }
            }
        }

        public void ApplyToPanels(Transform parent) {
            foreach (SupervisionPanel item in parent.GetComponentsInChildren<SupervisionPanel>()) {
                foreach (ApplicableColor c in panelColors) {
                    foreach (Transform tr in FindChildRec(item.transform, c.filter)) {
                        tr.GetComponent<Image>().color = c.color;
                    }
                }
                foreach (ApplicableColor c in textColors) {
                    foreach (Transform tr in FindChildRec(item.transform, c.filter)) {
                        if (tr.GetComponent<TextMeshProUGUI>() != null)
                            tr.GetComponent<TextMeshProUGUI>().color = c.color;
                    }
                }
            }
        }

        private List<Transform> FindChildRec(Transform tr, string filter = "") {

            List<Transform> outList = new List<Transform>();

            for (int i = 0; i < tr.childCount; i++) {
                if (filter == "" || tr.GetChild(i).name == filter)
                    outList.Add(tr.GetChild(i));

                outList.AddRange(FindChildRec(tr.GetChild(i), filter));
            }

            return outList;
        }


    }

}