﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Keyveo.Features.Supervision {

    public class CommunicationPanel : SupervisionPanel {

        [SerializeField]
        private TMP_InputField inputField = null;

        public void SendMessage() {
            CommunicationManager.Instance.Send(inputField.text);
            Toggle(0);
        }

    }

}