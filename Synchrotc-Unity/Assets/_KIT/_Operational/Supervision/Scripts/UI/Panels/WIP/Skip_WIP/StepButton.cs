﻿using DG.Tweening;
using Keyveo.Scenarisation;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Keyveo.Features.Supervision {

    public class StepButton : MonoBehaviour {

        [HideInInspector]
        public StepSO m_stepSO = null;

        [HideInInspector]
        public Button m_button = null;

        [SerializeField]
        private Button m_subButton = null;
        private TextMeshProUGUI m_textMeshPro = null;

        private bool m_isDeployed = false;

        public void SetUp(StepSO step) {
            FrontEnd.FrontEndManager.ApplySkinTo(transform);

            m_stepSO = step;

            m_textMeshPro = GetComponentInChildren<TextMeshProUGUI>();
            m_textMeshPro.text = step.m_description;

            m_button = GetComponentInChildren<Button>();

            m_button.onClick.AddListener(() => {
                SkipPanel.Instance.SelectStep(this);
                Animation(!m_isDeployed);
            });

            m_subButton.onClick.AddListener(() => {
                SubButtonClick();
            });

            Animation(false,0);
        }

        public void Animation(bool inAnimation = true, float duration = 0.5f) {
            m_subButton.transform.parent.DOScaleX(inAnimation ? 1f : 0f, duration).OnComplete(() => {
                m_isDeployed = inAnimation;
            });
            m_subButton.GetComponent<Image>().DOFade(inAnimation ? 1f : 0f, duration);
        }

        void SubButtonClick() {
            SkipManager.Instance.SkipTo(m_stepSO);
        }

    }

}