﻿#if KEYVEO_LOCALIZATION
using Keyveo.Features.Localization;
#endif
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Keyveo.Features.Supervision
{

    public class LocalizationPanel : SupervisionPanel
    {
#if KEYVEO_LOCALIZATION
        [SerializeField]
        private Button m_launcherButton = null;

        [SerializeField]
        private UnityEvent OnCanceling = null;

        public override void SetUp()
        {
            base.SetUp();
            SetLauncherButtonIcon();
        }

        public void LocalizationButton(string languageID)
        {
            LocalizationManager.Instance.SetCurrentSceneLanguage(languageID);
            SetLauncherButtonIcon();
        }

        public void SetLauncherButtonIcon()
        {
            m_launcherButton.GetComponent<Image>().sprite = LocalizationManager.Instance.GetCurrentFlag();
        }

#if UNITY_EDITOR

        private void GenerateLocalizationButtons()
        {
            foreach (Language language in LocalizationManager.Instance.m_availableLanguages)
            {

            }
        }

#endif
#endif
    }

}