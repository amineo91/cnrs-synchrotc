﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Keyveo.Features.Supervision {

    /// <summary>
    /// SupervisionPanel allowing to skip the current scene. 
    /// </summary>
    public class SkipScenePanel : SupervisionPanel {


        [SerializeField, FoldoutGroup("Override References")]
        private Button confirmButton =null;

        [SerializeField]
        private UnityEvent OnAppSkipping = null;

        [SerializeField]
        private UnityEvent OnCanceling = null;

        [SerializeField]
        private Scene m_DestinationScene;

        public override void SetUp() {

            confirmButton.onClick.AddListener(() => OnAppSkipping.Invoke());
            m_closeButton.onClick.AddListener(() => OnCanceling.Invoke());
            base.SetUp();
        }

        public void SkipScene() {
            SceneManager.LoadScene(m_DestinationScene.buildIndex);
        }

    }

}