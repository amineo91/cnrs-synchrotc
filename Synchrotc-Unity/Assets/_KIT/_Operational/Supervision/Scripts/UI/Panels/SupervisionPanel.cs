﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Keyveo.Features.Supervision {

    /// <summary>
    /// Base class for panel appearing in the Supervision/Adimistration screen.
    /// </summary>
    [RequireComponent(typeof(Resizer))]
    public class SupervisionPanel : MonoBehaviour {

        private Resizer m_resizer = null;

        private List<SupervisionPanel> panels = new List<SupervisionPanel>();

        [SerializeField, FoldoutGroup("Override References")]
        protected Transform m_layoutParent;

        [SerializeField, FoldoutGroup("Override References")]
        protected Button m_closeButton;

        #region MonoBehaviour

        protected virtual void Awake() {
            // AutoRef

            if(m_resizer == null)
                m_resizer = GetComponent<Resizer>();

            if(m_closeButton != null)
                m_closeButton.onClick.AddListener(() => m_resizer.ToSecondary());

            if(m_layoutParent == null)
                m_layoutParent = transform.Find("Background");
            if(m_layoutParent == null)
                m_layoutParent = transform.Find("Foreground");

            if(m_layoutParent == null)
                m_layoutParent = transform;

        }

        // Start is called before the first frame update
        protected virtual void Start() {
            if(panels == null || panels.Count == 0)
                panels = GetComponentsInChildren<SupervisionPanel>().ToList();

            SetUp();
        }

        #endregion

        public virtual void SetUp() {

        }


        public virtual void ToggleInstant() {
            m_resizer.Toggle();
        }

        public virtual void Toggle(float delay) {
            m_resizer.ChangeToTheOpposit(delay);
        }

    }
}