﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Keyveo.Features.Supervision {

    public class StartPanel : SupervisionPanel {

        //[SerializeField]
        //private UnityEvent OnAppStarting = null;

        public override void SetUp() {

            //GetComponentInChildren<Button>().onClick.AddListener(() => OnAppStarting.Invoke());
            base.SetUp();
        }

        private void Update() {
            if(Scenarisation.ScenarioManager.Instance.m_scenarioHasStarted)
                GetComponent<Resizer>().ToSecondary();
        }

    }

}