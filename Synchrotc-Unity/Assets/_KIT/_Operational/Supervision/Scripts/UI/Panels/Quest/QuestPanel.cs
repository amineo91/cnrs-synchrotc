﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Keyveo.Scenarisation;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using DG.Tweening;
#if KEYVEO_LOCALIZATION
using Keyveo.Features.Localization;
#endif
using TMPro;

namespace Keyveo.Features.Supervision
{

    /// <summary>
    /// SupervisionPanel handling all QuestDisplayers.
    /// </summary>
    public class QuestPanel : SupervisionPanel
    {

        private List<QuestDisplayer> m_displayers = new List<QuestDisplayer>();

        private int currentStep = 0;

        [SerializeField, FoldoutGroup("Override References")]
        private Slider m_progressionSlider = null;

#region MonoBehaviour

        protected override void Awake()
        {
            base.Awake();

            if (m_progressionSlider == null)
                m_progressionSlider = GetComponentInChildren<Slider>();
        }

#endregion

#region Displayers

        /// <summary>
        /// Generate a new list of QuestDisplayers for the given list of StepSO.
        /// </summary>
        /// <param name="questPrefab">The QuestDisplayer Prefab to instantiate.</param>
        /// <param name="steps">The list of StepSO to display.</param>
        public void SetSteps(QuestDisplayer questPrefab, List<StepSO> steps)
        {
            m_displayers.Clear();
            currentStep = 0;
            for (int i = 0; i < steps.Count; i++)
            {
                QuestDisplayer qd = Instantiate(questPrefab, m_layoutParent);

#if KEYVEO_LOCALIZATION
                if (LocalizationManager.Instance != null)
                {
                    qd.SetText(LocalizationManager.Instance.ApplyTranslationTo(steps[i], ScenarisationState.Reached)).OnComplete(delegate
                    {
                        LocalizationManager.Instance.RegisterText(qd.m_text);
                    });
                }
                else
                {
#endif
                    qd.SetText(steps[i].m_description);
#if KEYVEO_LOCALIZATION
                }
#endif

                qd.m_relatedSO = steps[i];
                qd.m_button.onClick.AddListener(() => qd.m_relatedSO.Skip());
                if (i == 0)
                {
                    qd.SetCurrent();
                }
                qd.Show(i * 0.25f);
                m_displayers.Add(qd);
            }
        }

        /// <summary>
        /// Update QuestDisplayers upon reaching a new step.
        /// </summary>
        /// <param name="wasError">Was the previous step failed ?</param>
        public void NextStep(bool wasError)
        {
            currentStep++;
            if (currentStep >= m_displayers.Count)
            {
                //END
                for (int i = 0; i < m_displayers.Count; i++)
                {
                    m_displayers[i].Hide((m_displayers.Count - i) * 0.15f);
                }
                m_displayers.Clear();
            }
            else
            {
                //UPDATE
                if (wasError)
                {
                    m_displayers[currentStep - 1].SetError();
                }
                else
                {
                    m_displayers[currentStep - 1].SetPassed();
                }
                m_displayers[currentStep].SetCurrent();
            }
        }

#endregion

#region Progression

        /// <summary>
        /// Update the progression slider of the QuestPanel.
        /// </summary>
        public void UpdateProgression()
        {
            ScenarioManager.Instance.ComputeProgression();
            m_progressionSlider.DOValue(ScenarioManager.Instance.m_progression, 0.5f);

            if (m_progressionSlider.value == 1)
            {
                m_progressionSlider.fillRect.GetComponent<FrontEnd.FrontEndItem>().m_tag = "Confirmation";
                FrontEnd.FrontEndManager.ApplySkinTo(m_progressionSlider.fillRect.transform);
            }
        }

#endregion

    }

}