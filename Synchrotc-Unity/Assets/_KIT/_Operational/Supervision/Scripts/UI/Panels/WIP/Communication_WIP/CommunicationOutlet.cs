﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Linq;
using DG.Tweening;
using Keyveo.ExtensionMethods;

namespace Keyveo.Features.Supervision {

    public class CommunicationOutlet : MonoBehaviour {

        [SerializeField]
        private float m_duration = 5f;

        [SerializeField]
        private TextMeshProUGUI m_tmpGUI = null;

        [SerializeField]
        private TextMeshPro m_tmpText = null;

        private Text m_tweenText = null;

        private void Start() {
            FrontEnd.FrontEndManager.ApplySkinTo(transform);
            Write("");
            Toggle(false);
            m_tweenText = gameObject.AddComponent<Text>();
            m_tweenText.enabled = false;
        }

        
        public void Write(string message) {

            if (m_tmpGUI != null)
                m_tmpGUI.DOWrite(message, 2);
                //m_tweenText.DOText(message, 2f).OnUpdate(() => m_tmpGUI.text = m_tweenText.text);
            if (m_tmpText != null)
                m_tweenText.DOText(message, 2f).OnUpdate(() => m_tmpText.text = m_tweenText.text);

            Toggle(true);

            Invoke("Hide", m_duration);
        }


        public virtual void Toggle(bool truth) {
            if (!truth) {
                if (m_tmpGUI != null)
                    m_tmpGUI.gameObject.SetActive(false);
                if (m_tmpText != null)
                    m_tmpText.gameObject.SetActive(false);
            }

            GetComponentsInChildren<Image>().ToList().ForEach(x => x.DOFade(truth ? 1 : 0, 1).SetEase(Ease.InOutSine));

            if (truth) {
                if (m_tmpGUI != null)
                    m_tmpGUI.gameObject.SetActive(true);
                if (m_tmpText != null)
                    m_tmpText.gameObject.SetActive(true);
            }


        }

        private void Hide() {
            Toggle(false);
        }
    }

}