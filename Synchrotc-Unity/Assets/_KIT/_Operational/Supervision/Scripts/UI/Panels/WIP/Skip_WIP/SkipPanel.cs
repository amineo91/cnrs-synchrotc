﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Keyveo.Scenarisation;
using Sirenix.OdinInspector;

namespace Keyveo.Features.Supervision {

    public class SkipPanel : SupervisionPanel {

        public static SkipPanel Instance;

        [HideInInspector]
        public ObjectiveButton m_selectedObjective = null;
        [HideInInspector]
        public StepButton m_selectedStep = null;

        [SerializeField, FoldoutGroup("Prefabs")]
        private ObjectiveButton m_objectiveButtonPrefab = null;
        [SerializeField, FoldoutGroup("Prefabs")]
        private StepButton m_stepButtonPrefab = null;

        [SerializeField, FoldoutGroup("References")]
        private Transform m_objectiveParent = null;
        [SerializeField, FoldoutGroup("References")]
        private Transform m_stepParent = null;

        private List<StepButton> m_stepButtons = new List<StepButton>();

        protected override void Awake() {
            Instance = this;

            base.Awake();
        }

        public override void SetUp() {
            base.SetUp();

            GenerateObjectiveButton(ScenarioManager.Instance.m_scenario);
        }

        public void GenerateObjectiveButton(ScenarioSO scenario) {
            foreach(ObjectiveSO item in scenario.objectives) {
                ObjectiveButton ob = Instantiate(m_objectiveButtonPrefab, m_objectiveParent);
                ob.SetUp(item);
            }
        }

        public void SelectObjective(ObjectiveButton objectiveButton) {
            if(m_selectedObjective != null)
                m_selectedObjective.m_button.interactable = true;


            m_selectedObjective = objectiveButton;
            objectiveButton.m_button.interactable = false;

            GenerateStepButtons(m_selectedObjective.m_objectiveSO);
        }

        void GenerateStepButtons(ObjectiveSO objective) {
            CleanStepButtons();
            foreach(StepSO item in objective.steps) {
                StepButton sb = Instantiate(m_stepButtonPrefab, m_stepParent);
                sb.SetUp(item);
            }
        }

        void CleanStepButtons() {
            if(m_stepButtons != null) {
                for(int i = 0; i < m_stepButtons.Count; i++) {
                    DestroyImmediate(m_stepButtons[i].gameObject);
                }
            }
            m_stepButtons = new List<StepButton>();
        }

        public void SelectStep(StepButton stepButton) {
            if(m_selectedStep != null) {
                m_selectedStep.Animation(false);
                m_selectedStep.m_button.interactable = true;
            }
            
            m_selectedStep = stepButton;
            stepButton.m_button.interactable = false;

        }
    }

}