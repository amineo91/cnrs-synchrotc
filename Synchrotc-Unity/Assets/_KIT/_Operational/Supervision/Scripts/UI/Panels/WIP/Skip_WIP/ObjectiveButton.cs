﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Keyveo.Scenarisation;
using UnityEngine.UI;
using TMPro;

namespace Keyveo.Features.Supervision {

    public class ObjectiveButton : MonoBehaviour {

        public ObjectiveSO m_objectiveSO;

        [HideInInspector]
        public Button m_button;

        private TextMeshProUGUI m_textMeshPro;

        public void SetUp(ObjectiveSO objective) {
            FrontEnd.FrontEndManager.ApplySkinTo(transform);

            m_objectiveSO = objective;

            m_textMeshPro = GetComponentInChildren<TextMeshProUGUI>();
            m_textMeshPro.text = objective.m_description;

            m_button = GetComponentInChildren<Button>();

            m_button.onClick.AddListener(() => {
                SkipPanel.Instance.SelectObjective(this);
            });
        }
    }

}