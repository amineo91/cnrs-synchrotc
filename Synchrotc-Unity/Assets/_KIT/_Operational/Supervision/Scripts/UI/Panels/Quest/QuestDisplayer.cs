﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Keyveo.Scenarisation;
using TMPro;
using Keyveo.ExtensionMethods;

namespace Keyveo.Features.Supervision {

    /// <summary>
    /// Component handling the display of a ScenarisationObject on the QuestPanel of the SupervisionPanel.
    /// </summary>
    public class QuestDisplayer : MonoBehaviour {

        public TextMeshProUGUI m_text = null;

        public Button m_button = null;

        [HideInInspector]
        public ScenarisationObject m_relatedSO = null;

        [SerializeField]
        private Image m_Border = null;

        [SerializeField]
        private Color m_PassedColor = Color.grey;
        [SerializeField]
        private Color m_CurrentColor = Color.blue;
        [SerializeField]
        private Color m_NotYetColor = Color.white;
        [SerializeField]
        private Color m_ErrorColor = Color.red;

        private bool m_isCurrent = false;

        /// <summary>
        /// Sets this QuestDisplayer as the currently active one, handles the animation.
        /// </summary>
        public void SetCurrent() {
            if(m_isCurrent)
                return;

            m_isCurrent = true;

            m_text.DOComplete();
            m_Border.DOComplete();

            m_text.DOColor(m_CurrentColor, 0.5f);
            m_Border.DOColor(m_CurrentColor, 0.5f);

            m_button.gameObject.SetActive(true);
        }

        /// <summary>
        /// Sets this QuestDisplayer as passed, handles the animation.
        /// </summary>
        public void SetPassed() {
            if(!m_isCurrent)
                return;
            m_isCurrent = false;

            m_text.DOComplete();
            m_Border.DOComplete();

            m_text.DOColor(m_PassedColor, 0.5f);
            m_Border.DOColor(m_PassedColor, 0.5f);

            m_button.gameObject.SetActive(false);
        }

        /// <summary>
        /// Sets this QuestDisplayer as failed, handles the animation.
        /// </summary>
        public void SetError() {
            if(!m_isCurrent)
                return;
            m_isCurrent = false;

            m_text.DOComplete();
            m_Border.DOComplete();

            m_text.DOColor(m_ErrorColor, 0.5f);
            m_Border.DOColor(m_ErrorColor, 0.5f);

            m_button.gameObject.SetActive(false);
        }


        /// <summary>
        /// Shows this QuestDisplayer after the given delay.
        /// </summary>
        public void Show(float delay) {
            m_Border.DOComplete();
            m_text.DOComplete();

            m_text.color = m_NotYetColor;
            m_Border.color = m_NotYetColor;

            m_Border.DOFade(0f, 0.01f).OnComplete(delegate {
                m_Border.DOFade(1f, 0.75f).SetDelay(delay);
            });

            m_text.DOFade(0f, 0.01f).OnComplete(delegate {
                m_text.DOFade(1f, 0.75f).SetDelay(delay);
            });
        }

        /// <summary>
        /// hides this QuestDisplayer after the given delay.
        /// </summary>
        public void Hide(float delay) {
            m_Border.DOComplete();
            m_text.DOComplete();

            m_Border.DOFade(0f, 0.75f).SetDelay(delay);
            m_text.DOFade(0f, 0.75f).SetDelay(delay).OnComplete(delegate {
                Destroy(gameObject);
            });
        }

        /// <summary>
        /// Sets this QuestDisplayer's text.
        /// </summary>
        public Tweener SetText(string text) {
            return m_text.DOWrite(text, 1f).SetEase(Ease.InOutCubic);
        }

    }

}