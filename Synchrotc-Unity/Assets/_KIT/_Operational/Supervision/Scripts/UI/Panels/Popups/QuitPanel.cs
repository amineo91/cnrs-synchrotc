﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Keyveo.Features.Supervision {

    /// <summary>
    /// SupervisionPanel handling application quiting.
    /// </summary>
    public class QuitPanel : SupervisionPanel {

        [SerializeField, FoldoutGroup("Override References")]
        private Button confirmButton =null;

        [SerializeField]
        private UnityEvent OnAppQuiting = null;

        [SerializeField]
        private UnityEvent OnCanceling = null;

        public override void SetUp() {

            confirmButton.onClick.AddListener(() => OnAppQuiting.Invoke());
            m_closeButton.onClick.AddListener(() => OnCanceling.Invoke());
            base.SetUp();
        }
    }

}