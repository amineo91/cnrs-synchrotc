﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Keyveo.Scenarisation;
using Keyveo.Features;

namespace Keyveo.Features.Supervision {

    /// <summary>
    /// FeatureManager handling the display of the scenario's progress.
    /// </summary>
    public class QuestManager : MonoBehaviour, FeatureManager {

        [SerializeField]
        private QuestDisplayer m_questPrefab = null;

        private QuestPanel questPanel;

        #region MonoBehaviour

        // Start is called before the first frame update
        void Start() {
            questPanel = FindObjectOfType<QuestPanel>();
        }

        #endregion

        #region FeatureManager Implementation

        public void UponReachingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f) {
            //NOTHING YET
            if(scenarisationObject is ObjectiveSO)
            {
                questPanel.SetSteps(m_questPrefab, ((ObjectiveSO)scenarisationObject).steps);
            }
        }

        public void UponFailingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f) {
            questPanel.NextStep(true);
            questPanel.UpdateProgression();
        }

        public void UponSkippingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f) {
            questPanel.NextStep(false);
            questPanel.UpdateProgression();
        }

        public void UponSucceedingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f) {
            questPanel.NextStep(false);

            if(ScenarioManager.Instance.m_scenarioHasEnded)
                return;

            if(scenarisationObject is StepSO) {
                questPanel.UpdateProgression();
            }
        }

        #endregion
    }

}