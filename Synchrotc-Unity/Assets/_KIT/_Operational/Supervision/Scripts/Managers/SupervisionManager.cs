﻿using Keyveo.Features;
using Keyveo.Scenarisation;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Keyveo.Features.Supervision {

    /// <summary>
    /// Manager handling the Supervision/Admin panel & functionalities.
    /// </summary>
    public class SupervisionManager : MonoBehaviour {

        public static SupervisionManager Instance;

        [SerializeField]
        private GameObject m_infoDisplayerPrefab = null;

        #region MonoBaheviour

        private void Awake() {
            Instance = this;
        }

        #endregion



        public GameObject InstantiateInfoDisplayer(Transform parent = null) {
            return Instantiate(m_infoDisplayerPrefab, parent);
        }


    }

}