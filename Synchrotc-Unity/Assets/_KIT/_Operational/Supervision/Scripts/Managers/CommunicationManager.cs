﻿using Sirenix.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Keyveo.Features.Supervision {

    public class CommunicationManager : MonoBehaviour {

        public static CommunicationManager Instance;


        [SerializeField]
        private List<CommunicationOutlet> outlets = new List<CommunicationOutlet>();

        private CommunicationPanel panel;

        [SerializeField]
        private GameObject m_pingPrefab = null;

        private void Awake() {
            Instance = this;
            // ? DDOL ?
        }

        // Start is called before the first frame update
        void Start() {

        }

        // Update is called once per frame
        void Update() {
            if (m_isPinging) {
                if (Input.GetMouseButtonUp(0)) {

                    Ray r = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(r, out hit)) {
                        Ping(hit.point);
                    }

                }
            }
        }

        public void Send(string message) {
            outlets.ForEach(x => x.Write(message));
        }




        [SerializeField]
        private float m_pingDuration = 3f;
        
        private bool m_isPinging = false;

        public void TogglePing() {
            Invoke("DelayedTogglePing", 0.1f);
        }

        private void DelayedTogglePing() {
            m_isPinging = !m_isPinging;

        }


        public void Ping(Vector3 position) {
            TogglePing();
            GameObject go = Instantiate(m_pingPrefab);
            go.transform.position = position;

            Destroy(go, m_pingDuration);
        }

    }

}