﻿
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Keyveo.Scenarisation {

    /// <summary>
    /// States a ScenarisationObject can be in.
    /// </summary>
    public enum ScenarisationState {
        FirstReached,
        Reached,
        Successsed,
        Failed,
        Skipped,
        Asleep
    }

    /// <summary>
    /// Contains and store a ScenarisationObject results, allowing scenario tracking & results persistence.
    /// </summary>
    [System.Serializable]
    public class ScenarisationResults {
        public ScenarisationState state = ScenarisationState.Asleep;

        public bool reached = false;
        public bool succeeded = false;
        public bool failed = false;
        public bool skipped = false;

        /// <summary>
        /// Resets this ScenarisationResults.
        /// </summary>
        public void Reset() {
            state = ScenarisationState.Asleep;
            reached = false;
            succeeded = false;
            failed = false;
            skipped = false;
        }

        /// <summary>
        /// Change the state of this ScenarisationResults, updating its tracked data.
        /// </summary>
        /// <param name="newState">The new state to set.</param>
        public void SetState(ScenarisationState newState) {
            switch (newState) {
                case ScenarisationState.Asleep:
                Reset();
                break;

                case ScenarisationState.Reached:
                reached = true;
                break;

                case ScenarisationState.Successsed:
                succeeded = true;
                failed = false;
                skipped = false;
                break;

                case ScenarisationState.Failed:
                succeeded = false;
                failed = true;
                skipped = false;
                break;

                case ScenarisationState.Skipped:
                succeeded = false;
                failed = false;
                skipped = true;
                break;
            }
            state = newState;
        }
    }


    /// <summary>
    /// UnityEvent with a ScenarisationObject and a duration (float) as parameters.
    /// </summary>
    [System.Serializable]
    public class ScenarisationEvent : UnityEvent<ScenarisationObject, float> {

    }

    /// <summary>
    /// Regroups all ScenarisationEvent used by a ScenarisationObject.
    /// </summary>
    [System.Serializable]
    public class ScenarisationCallbacks {
        public ScenarisationEvent UponReaching = new ScenarisationEvent();
        public ScenarisationEvent UponFirstReaching = new ScenarisationEvent();
        public ScenarisationEvent UponSucceeding = new ScenarisationEvent();
        public ScenarisationEvent UponFailing = new ScenarisationEvent();
        public ScenarisationEvent UponSkipping = new ScenarisationEvent();
    }


    /// <summary>
    /// The ScenarioManager handles the scenario's tracking.
    /// </summary>
    public class ScenarioManager : MonoBehaviour {

        public static ScenarioManager Instance;

        [FoldoutGroup("Settings")]
        public ScenarisationProfile m_profile;

        [FoldoutGroup("Current State")]
        public ScenarioSO m_scenario;
        [FoldoutGroup("Current State")]
        public ObjectiveSO m_currentObjective;
        protected int m_objectiveID = 0;

        [FoldoutGroup("Current State")]
        public StepSO m_currentStep;
        protected int m_stepID = -1;


        [SerializeField, FoldoutGroup("Settings")]
        private GameObject m_victoryEffectPrefab = null;

        [HideInInspector]
        public List<ScenarisationComponent> m_registeredComponents = new List<ScenarisationComponent>();
        private ScenarisationComponent[] m_scenarisationComponents;

        [FoldoutGroup("Callbacks")]
        public float m_callbackDuration = 1f;

        [FoldoutGroup("Callbacks")]
        public UnityEvent m_OnStartScenario;
        [FoldoutGroup("Callbacks")]
        public UnityEvent m_OnEndScenario;

        [FoldoutGroup("Callbacks")]
        public ScenarisationCallbacks m_baseStepCallbacks;
        [FoldoutGroup("Callbacks")]
        public ScenarisationCallbacks m_baseObjectiveCallback;
        [FoldoutGroup("Callbacks")]
        public ScenarisationCallbacks m_parallelObjectiveCallback;

        [FoldoutGroup("Settings")]
        public bool m_isSequencing = true;
        
        [FoldoutGroup("Settings")]
        public bool m_expertMode = false;

        [FoldoutGroup("Settings")]
        public bool m_autoStart = false;
        

        [HideInInspector]
        public float m_progression = 0f;
        [HideInInspector]
        public bool m_scenarioHasEnded = false;
        [HideInInspector]
        public bool m_scenarioHasStarted = false;

        public bool isKeyboardInputActivated = true;

        public List<effectByObjectName> effectByObjectsName;


        #region MonoBehaviour

        protected virtual void Awake() {
            Instance = this;
            effectByObjectsName = new List<effectByObjectName>();
    }

        protected virtual void Start() {
            m_scenarisationComponents = FindObjectsOfType<ScenarisationComponent>();
            if (m_autoStart)
                Invoke("StartScenario", 2);
        }

        #endregion

        #region Scenarisation Process

        /// <summary>
        /// Reach the next ScenarisationObject of the ScenarioSO.
        /// </summary>
        public void Next() {

            m_stepID++;
            if (m_stepID == m_currentObjective.steps.Count) {
                m_currentObjective.Succeed();
                NextObjective();
            } else {
                m_currentStep = m_currentObjective.steps[m_stepID];
                m_currentStep.Reach();
            }

            SpeechesManager.Instance.isReady = true;

        }

        /// <summary>
        /// Reach the next ObjectiveSO of the ScenarioSO.
        /// </summary>
        public virtual void NextObjective() {
            m_objectiveID++;
            m_stepID = -1;

            if (m_objectiveID == m_scenario.objectives.Count)
                EndScenario();
            else {
                m_currentObjective = m_scenario.objectives[m_objectiveID];
                m_currentObjective.Reach();
            }
        }

        public bool IsCurrentStepFinal()
        {
            return m_stepID == m_currentObjective.steps.Count - 1;
        }
		
        public bool IsCurrentObjectiveFinal()
        {
            return m_objectiveID == m_scenario.objectives.Count - 1;
        }



        /// <summary>
        /// Go to the given ObjectiveSO of the ScenarioSO.
        /// </summary>
        public void GoToObjective(ObjectiveSO objectiveSO) {
            if (!m_scenario.objectives.Contains(objectiveSO)) {
#if DEBUG_SCENAR
                //Debug.Log("[SCENARISATION] Chosen Scenario does not contains the given Objective: " + objectiveSO.m_description);

#endif
                return;
            }
            m_objectiveID = m_scenario.objectives.IndexOf(objectiveSO);
            m_currentObjective = objectiveSO;
            m_currentObjective.Reach();

        }

        /// <summary>
        /// Go to the given StepSO of the ScenarioSO.
        /// </summary>
        public void GoToStep(StepSO stepSO) {
            if (stepSO.objectiveParent == null) {
#if DEBUG_SCENAR
                //Debug.Log("[SCENARISATION] Given Step is not related to any Objective.");
#endif
                return;
            }

            GoToObjective(stepSO.objectiveParent);

            //registeredComponents[0].callbacks.UponReaching.Invoke(currentObjective);

            m_stepID = m_currentObjective.steps.IndexOf(stepSO);
            m_currentStep = stepSO;
            m_currentStep.Reach();
        }

        /// <summary>
        /// Reaches the first objective of the ScenarioSO.
        /// </summary>
        public void StartScenario() {
            m_OnStartScenario.Invoke();
            m_scenarioHasStarted = true;
            m_currentObjective = m_scenario.objectives[m_objectiveID];
            m_currentObjective.Reach();
        }

        /// <summary>
        /// Ends the ScenarioSO, triggers the victory effect.
        /// </summary>
        public void EndScenario() {

#if DEBUG_SCENAR
            //Debug.Log("[SCENARISATION] End Scenario");
#endif
            GameObject effect;
            if (m_victoryEffectPrefab != null)
                effect = Instantiate(m_victoryEffectPrefab, Camera.main.transform);
            m_OnEndScenario.Invoke();
            m_scenarioHasEnded = true;
        }

        /// <summary>
        /// Verify the ScenarisationResults of all the StepSO of the current ObjectiveSO. Decides whether or not the ObjectiveSO is successed.
        /// </summary>
        public void VerifyObjective() {
#if DEBUG_SCENAR
            //Debug.Log("[SCENARISATION] Verifying Objective" + m_currentObjective.name + " : " + m_currentObjective.m_description);
#endif

            foreach (StepSO item in m_currentObjective.steps) {
                if (item.m_results.state == ScenarisationState.Asleep || item.m_results.state == ScenarisationState.Reached) {
                    return;
                }
            }

            if (m_currentObjective.VerifyMe()) {

#if DEBUG_SCENAR
                //Debug.Log("[SCENARISATION] Validating Objective" + m_currentObjective.name + " : " + m_currentObjective.m_description);
#endif
                foreach (StepSO item in m_currentObjective.steps) {
                    switch (item.m_results.state) {
                        case ScenarisationState.Successsed:
                            item.Succeed();
                        break;
                        case ScenarisationState.Failed:
                        item.Fail();
                        break;
                        case ScenarisationState.Skipped:
                        item.Skip();
                        break;
                    }
                }
                m_currentObjective.Succeed();
            } else {
                m_currentObjective.Fail();
            }
        }

        /// <summary>
        /// Compute the current ScenarioSO progression.
        /// </summary>
        public void ComputeProgression() {
            List<StepSO> tmp = new List<StepSO>();
            m_scenario.objectives.ForEach(o => tmp.AddRange(o.steps));

            m_progression = (float)(tmp.IndexOf(m_currentStep) +1) / (float)tmp.Count;
        }

        #endregion

        #region Registration

        private void GetRegisterComponents(ScenarisationObject scenarisationObject) {
            m_registeredComponents = m_scenarisationComponents.Where(x => x.m_scenarisationObject == scenarisationObject).ToList();

#if DEBUG_SCENAR

            if (m_registeredComponents.Count == 0)
                Debug.Log("[SCENARISATION] There is no Scenarisation Component in the scene for this ScenarisationObject. (" + scenarisationObject.ToString() + ")");
            else
                Debug.Log("[SCENARISATION] There is " + m_registeredComponents.Count + " Scenarisation Component in the scene for this ScenarisationObject. (" + scenarisationObject.ToString() + ")");
#endif
        }

        #endregion

        #region Utils
        

        public class effectByObjectName
        {
            public string name;
            public string effect;
        }
        
        /// <summary>
        /// Toggles all ScenarisationTarget that are not related to the current ScenarisationObject.
        /// </summary>
        public void ToggleUnusedTarget() {
            foreach(ScenarisationComponent item in m_scenarisationComponents) {
                item.ToggleTargets(item.m_scenarisationObject == m_currentStep);
            }
        }

        /// <summary>
        /// Utils method used to invoke the ScenarisationEvent corresponding to a given ScenarisationState of a given ScenarisationObject.
        /// </summary>
        /// <param name="state">The state of the ScenarisationObject.</param>
        /// <param name="scenarisationObject">The ScenarisationObject that needs to callback.</param>
        /// <param name="duration">The duration of the callback, default value can be set in "Settings".</param>
        public void Invoke(ScenarisationState state, ScenarisationObject scenarisationObject, float duration = -1f) {
            if(duration == -1f)
                duration = m_callbackDuration;

            switch(state) {
                case ScenarisationState.FirstReached:
                m_baseStepCallbacks.UponFirstReaching.Invoke(scenarisationObject, duration);
                m_baseStepCallbacks.UponReaching.Invoke(scenarisationObject, duration);
                break;
                case ScenarisationState.Reached:
                m_baseStepCallbacks.UponReaching.Invoke(scenarisationObject, duration);
                break;
                case ScenarisationState.Successsed:
                m_baseStepCallbacks.UponSucceeding.Invoke(scenarisationObject, duration);
                break;
                case ScenarisationState.Failed:
                m_baseStepCallbacks.UponFailing.Invoke(scenarisationObject, duration);
                break;
                case ScenarisationState.Skipped:
                m_baseStepCallbacks.UponSkipping.Invoke(scenarisationObject, duration);
                break;

                default:
                break;
            }

        }

        #endregion

    }

}