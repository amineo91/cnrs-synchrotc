﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Keyveo.Scenarisation {

    public class SkipManager : MonoBehaviour {

        public static SkipManager Instance;

        #region MonoBehaviour

        private void Awake() {
            Instance = this;
        } 

        #endregion

        /// <summary>
        /// Skips the current step of the scenario.
        /// </summary>
        [Button]
        public virtual void SkipCurrentStep() {
            SkipScenarisationObject(ScenarioManager.Instance.m_currentStep);            
        }

        /// <summary>
        /// Skips the current ScenarisationObject.
        /// </summary>
        protected virtual void SkipScenarisationObject(ScenarisationObject scenarisationObject) {
            scenarisationObject.Skip();
        }

        /// <summary>
        /// Skips ScenarisationObjects until the ScenarioManager has reached the given ScenarisationObject.
        /// </summary>
        /// <param name="scenarisationObject">The ScenarisationObejct to reach.</param>
        [Button]
        public virtual void SkipTo(ScenarisationObject scenarisationObject) {
            if(ScenarioManager.Instance.m_currentStep == scenarisationObject)
                return;

            StartCoroutine(SkipToRoutine(scenarisationObject));
        }

        /// <summary>
        /// Coroutine skiping ScenarisationObjects until it reaches the given ScenarisationObject.
        /// </summary>
        /// <param name="scenarisationObject">The ScenarisationObejct to reach.</param>
        protected virtual IEnumerator SkipToRoutine(ScenarisationObject scenarisationObject) {
            StepSO current = ScenarioManager.Instance.m_currentStep;

            while(ScenarioManager.Instance.m_currentStep != scenarisationObject) {
                SkipCurrentStep();
                yield return new WaitForSeconds(1f);
            }

        }


    }

}