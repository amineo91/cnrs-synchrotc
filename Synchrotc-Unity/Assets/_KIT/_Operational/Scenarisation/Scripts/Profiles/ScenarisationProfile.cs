﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Keyveo.Scenarisation {
    
    [CreateAssetMenu(fileName = "NewProfile", menuName = "Keyveo/Scenarisation/Profile")]
    public class ScenarisationProfile : ScriptableObject {

        public string profileName;

        public List<Color> objectiveColors;
    }

}