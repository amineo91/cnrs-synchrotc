﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace Keyveo.Scenarisation {

    // TMP
    public enum ParallelVerification {
        AllSuccess,     // All steps validated
        AllFail,        // All steps failed
        AllSkip,        // All steps skipped
        OneFail         // At least one step failed
    }

    /// <summary>
    /// An ObjectiveSO stores a list of StepSO.
    /// </summary>
    [CreateAssetMenu(fileName = "NewObjective", menuName = "Keyveo/Scenarisation/Objective")]
    public class ObjectiveSO : ScenarisationObject { 

        public List<StepSO> steps = new List<StepSO>();
        public string id_scenario;

        public bool isParallel = false;
        [JsonIgnore]
        public bool wasFailed = false;
        public ParallelVerification parallelVerification = ParallelVerification.AllSuccess;


        public void CopyTo(ObjectiveSO objective) {
            BaseCopy(objective);
            objective.steps = steps;
            objective.isParallel = isParallel;
        }

        public override void Reach() {
            base.Reach();
            //bool tempIsSequencing = ScenarioManager.Instance.isSequencing;
            ScenarioManager.Instance.m_isSequencing = wasFailed ? true : !isParallel;

            if(ScenarioManager.Instance.m_isSequencing) {
                //if(tempIsSequencing == ScenarioManager.Instance.isSequencing)
                ScenarioManager.Instance.Next();
            } else
                steps.ForEach(x => x.m_results.SetState(ScenarisationState.Reached));
        }

        public bool VerifyMe() {
            switch(parallelVerification) {
                case ParallelVerification.AllSuccess:
                return !steps.Select(x => x.m_results.succeeded).Contains(false);

                case ParallelVerification.AllFail:
                return !steps.Select(x => x.m_results.failed).Contains(false);

                case ParallelVerification.AllSkip:
                return !steps.Select(x => x.m_results.skipped).Contains(false);

                case ParallelVerification.OneFail:
                return steps.Select(x => x.m_results.succeeded).Contains(false);

            }
            return false;
        }



    }
}