﻿using Newtonsoft.Json;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
#if KEYVEO_VR
using Keyveo.XR;
using Valve.VR.InteractionSystem;
#endif

namespace Keyveo.Scenarisation {

    /// <summary>
    /// Base Class for ScenarisationObject.
    /// Contains several messages that are used for direct user feedback.
    /// Also defines if the object is skippable and stores the ScenarisationResults related to this object.
    /// </summary>
    public class ScenarisationObject : ScriptableObject {

        public string m_description;
        public string m_successMessage;
        public string m_failMessage;
        public string m_skipMessage;

        [HideInInspector, JsonIgnore]
        public bool m_isFirstReaching = true;

        [JsonIgnore]
        public ScenarisationResults m_results = new ScenarisationResults();

#if KEYVEO_VR
        [JsonIgnore]
        public TooltipData m_tooltipData;
#endif

        [HideInInspector, JsonIgnore]
        public ScenarisationComponent m_registeredComponent;

        /// <summary>
        /// Copy the values of the given ScenarisationObject on this one.
        /// </summary>
        /// <param name="scenarisationObject"></param>
        protected void BaseCopy(ScenarisationObject scenarisationObject) {
            scenarisationObject.m_description = m_description;
            scenarisationObject.m_successMessage = m_successMessage;
            scenarisationObject.m_failMessage = m_failMessage;
            scenarisationObject.m_skipMessage = m_skipMessage;
            scenarisationObject.m_results = m_results;
        }

        /// <summary>
        /// Called upon when the ScenarioManager reaches this ScenarisationObject.
        /// </summary>
        public virtual void Reach() {
#if DEBUG_SCENAR
            //Debug.Log("[SCENARISATION] Reaching " + GetType().Name + " : " + name + " = " + m_description);
#endif

            if(m_registeredComponent == null) {
#if DEBUG_SCENAR
                //Debug.Log("[SCENARISATION] No ScenarioComponent Found for : " + name);
#endif
            } else {
                ScenarioManager.Instance.Invoke(ScenarisationState.Reached, this);
                if(m_isFirstReaching) {
                    m_isFirstReaching = false;
                    if(m_registeredComponent.m_callbacks.UponFirstReaching != null) {
                        m_registeredComponent.Invoke(ScenarisationState.FirstReached);
                    }

                }

                if(m_registeredComponent.m_callbacks.UponReaching != null) {
                    m_registeredComponent.Invoke(ScenarisationState.Reached);
                }
            }
            m_results.SetState(ScenarisationState.Reached);
        }

        /// <summary>
        /// Called upon when the related ScenarisationComponent validates this ScenarisationObject.
        /// </summary>
        public virtual void Succeed() {
#if DEBUG_SCENAR
            //Debug.Log("[SCENARISATION] Succeeding " + GetType().Name + " : " + name + " = " + m_description);
#endif
            ScenarioManager.Instance.Invoke(ScenarisationState.Successsed, this);

            if(m_registeredComponent == null) {
#if DEBUG_SCENAR
                //Debug.Log("[SCENARISATION] No ScenarioComponent Found for : " + name);
#endif
            } else {
                m_registeredComponent.Invoke(ScenarisationState.Successsed);
            }

            m_results.SetState(ScenarisationState.Successsed);
        }

        /// <summary>
        /// Called upon when the related ScenarisationComponent refuses this ScenarisationObject.
        /// </summary>
        public virtual void Fail() {

#if DEBUG_SCENAR
            //Debug.Log("[SCENARISATION] Failing " + GetType().Name + " : " + name + " = " + m_description);
#endif
            
            ScenarioManager.Instance.Invoke(ScenarisationState.Failed, this);

            if(m_registeredComponent == null) {
#if DEBUG_SCENAR
                //Debug.Log("[SCENARISATION] No ScenarioComponent Found for : " + name);
#endif
            } else {
                //registeredComponent.m_callbacks.UponFailing.Invoke(this);
                m_registeredComponent.Invoke(ScenarisationState.Failed);
            }

            m_results.SetState(ScenarisationState.Failed);
        }

        /// <summary>
        /// Called upon when the related ScenarisationComponent skips this ScenarisationObject.
        /// </summary>
        public virtual void Skip(float skipDuration = 0f) {
#if DEBUG_SCENAR
            //Debug.Log("[SCENARISATION] Skipping " + GetType().Name + " : " + name + " = " + m_description);
#endif
            ScenarioManager.Instance.Invoke(ScenarisationState.Skipped, this);

            if(m_registeredComponent == null) {
#if DEBUG_SCENAR
                //Debug.Log("[SCENARISATION] No ScenarioComponent Found for : " + name);
#endif
            } else {
                m_registeredComponent.Invoke(ScenarisationState.Skipped);
            }

            m_results.SetState(ScenarisationState.Skipped);

            ScenarioManager.Instance.Next();
        }

        /// <summary>
        /// Resets this ScenarisationObject.
        /// </summary>
        public virtual void Reset() {
            m_isFirstReaching = true;
        }

    }

}