﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;
using Sirenix.OdinInspector;

namespace Keyveo.Scenarisation {

    /// <summary>
    /// An ScenarioSO stores a list of ObjectiveSO.
    /// </summary>
    [CreateAssetMenu(fileName = "NewScenario", menuName = "Keyveo/Scenarisation/Scenario")]
    public class ScenarioSO : ScenarisationObject {

        public List<ObjectiveSO> objectives = new List<ObjectiveSO>();

        public void Copy(ScenarioSO scenario) {
            BaseCopy(scenario);
            scenario.m_description = m_description;
        }

        [Button, FoldoutGroup("Data")]
        public void Serialize(string path, string fileName) {
            if(path == "")
                path = Application.persistentDataPath;
            string json = JsonConvert.SerializeObject(this, Formatting.Indented);

            string filePath = path + fileName + ".json";
            using(StreamWriter sw = new StreamWriter(filePath)) {
                sw.Write(json);
            }

            Debug.Log("ScenarioSO serialized at : " + filePath);
        }

        [Button, FoldoutGroup("Data")]
        private void ReadData(TextAsset json) {
            ScenarioSO tmp = JsonConvert.DeserializeObject<ScenarioSO>(json.text);
            this.m_description = tmp.m_description;
            this.m_successMessage = tmp.m_successMessage;
            this.m_failMessage = tmp.m_failMessage;
            this.m_skipMessage = tmp.m_skipMessage;
            this.objectives = tmp.objectives;
        }
    }
}
 