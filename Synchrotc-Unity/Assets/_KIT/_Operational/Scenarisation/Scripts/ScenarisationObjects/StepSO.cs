﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Events;
using System.Linq;
using Newtonsoft.Json;

namespace Keyveo.Scenarisation {

    /// <summary>
    /// A StepSO stores the data of an objective's step.
    /// </summary>
    [CreateAssetMenu(fileName = "NewStep", menuName = "Keyveo/Scenarisation/Step")]
    public class StepSO : ScenarisationObject {

        [HideInInspector, JsonIgnore]
        public ObjectiveSO objectiveParent;

        /// <summary>
        /// Copies the values of the given StepSO onto this one.
        /// </summary>
        /// <param name="step">The StepSO to copy.</param>
        public void CopyTo(StepSO step) {
            BaseCopy(step);
            step.m_skipMessage= m_skipMessage;
            step.m_failMessage = m_failMessage;
            step.m_description = m_description;
        }
        


        public override void Succeed()
        {
            base.Succeed();
            if (ScenarioManager.Instance.m_isSequencing)
            {
                m_registeredComponent.StartCoroutine(OnSuccess());
            }
        }

        IEnumerator OnSuccess()
        {
            if (ScenarioManager.Instance.IsCurrentStepFinal() == false)
            {
                // No rush
                yield return new WaitForSeconds(0.5f);

                NotificationManager.Instance.Notify("Bravo étape validée !", NotificationType.Success);

                // No rush
                yield return new WaitForSeconds(1);

                ScenarioManager.Instance.Next();
            }
            else if (ScenarioManager.Instance.IsCurrentObjectiveFinal() == false)
            {
                // No rush
                yield return new WaitForSeconds(0.5f);

                NotificationManager.Instance.Notify("Bravo tâche terminée !", NotificationType.Success);

                // No rush
                yield return new WaitForSeconds(1);

                ScenarioManager.Instance.Next();
            }
            else
            {
                ScenarioManager.Instance.Next();
            }
        }
    }
} 