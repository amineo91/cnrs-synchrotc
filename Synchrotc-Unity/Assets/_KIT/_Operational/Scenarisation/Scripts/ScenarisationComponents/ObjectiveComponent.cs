﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Keyveo.Scenarisation {
    /// <summary>
    /// Meant to be attached to a Gameobject so that it can handle the validation of the related ObjectiveSO.
    /// </summary>
    public class ObjectiveComponent : ScenarisationComponent {

        protected override void Start() {
            base.Start();
            foreach (StepSO item in (m_scenarisationObject as ObjectiveSO).steps) {
                item.objectiveParent = m_scenarisationObject as ObjectiveSO;
            }

        }

        public override void Validate() {

        }

        public override void Refuse() {

        }

        public override void ResetScenarisationObject() {
            base.ResetScenarisationObject();
            (m_scenarisationObject as ObjectiveSO).wasFailed = false;
            //Debug.Log("Objective RESET");
        }
    }

}