﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Keyveo.Features.Guidance;

#if UNITY_EDITOR
using UnityEditor;
# endif

namespace Keyveo.Scenarisation {

    /// <summary>
    /// Placed on the visual targets of a ScenarisationComponent, it handles everything related with visual scenarisation.
    /// </summary>
    public class ScenarisationTarget : MonoBehaviour {

        [HideInInspector]
        public ScenarisationComponent m_scenarisationComponent;

        [FoldoutGroup("Indicator")]
        public Indicator m_indicator;

        [FoldoutGroup("Preview")]
        public AnimatorPreview m_preview;

        protected virtual void Start() {
            if(m_indicator == null)
                m_indicator = GetComponentInChildren<Indicator>();
        }


#if UNITY_EDITOR
        /// <summary>
        /// [EDITOR] 
        /// </summary>

        [Button("Create"), FoldoutGroup("Indicator"), HideInPlayMode]
        public void AddIndicator_Editor() {
            if(m_indicator != null) {
                Debug.LogWarning("There is already an Indicator for this Target.");
                return;
            }

            m_indicator = Instantiate((Indicator)AssetDatabase.LoadAssetAtPath("Assets/_KIT/_Operational/Features/Guidance/Indicators/Prefabs/Indicator.prefab", typeof(Indicator)), transform);
        }

       /// <summary>
       /// [EDITOR] 
       /// </summary>

       [Button("Create"), FoldoutGroup("Preview"), HideInPlayMode]
        public void AddPreview_Editor() {
            if(m_preview != null) {
                Debug.LogWarning("There is already a Preview for this Target.");
                return;
            }

            m_preview = Instantiate((AnimatorPreview)AssetDatabase.LoadAssetAtPath("Assets/_KIT/_Operational/Features/Preview/Prefabs/AnimatorPreview.prefab", typeof(AnimatorPreview)), transform);
        }
# endif

        public virtual void ToggleTarget(bool truth) {
            gameObject.SetActive(truth);
        }

    }



}
