﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Keyveo.Scenarisation;
using UnityEngine.Events;
using System.Linq;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Keyveo.Scenarisation {

    /// <summary>
    /// Meant to be attached to a Gameobject so that it can handle the validation of the related StepSO.
    /// </summary>
    public class StepComponent : ScenarisationComponent {


        public override void Validate() {
            if(ScenarioManager.Instance.m_isSequencing) {
                if(ScenarioManager.Instance.m_currentStep == m_scenarisationObject)
                    m_scenarisationObject.Succeed();
            } else {
                m_scenarisationObject.m_results.SetState(ScenarisationState.Successsed);
                //ScenarioManagerV2.Instance.parallelSteps[step] = true;
                ScenarioManager.Instance.VerifyObjective();
            }
        }

        public override void Refuse() {
            if(ScenarioManager.Instance.m_isSequencing) {
                if(ScenarioManager.Instance.m_currentStep == m_scenarisationObject)
                    m_scenarisationObject.Fail();
            } else {
                m_scenarisationObject.m_results.SetState(ScenarisationState.Failed);
                ScenarioManager.Instance.VerifyObjective();
            }
        }




    }

}