﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif
namespace Keyveo.Scenarisation {

    /// <summary>
    /// Base Class for ScenarisationComponent.
    /// Contains a ScenarioObject reference, and a set a ScenaristaionCallbacks.
    /// Handles the validation/refusal of the ScenarisationObject it is related to.
    /// </summary>
    public class ScenarisationComponent : MonoBehaviour {

        public ScenarisationObject m_scenarisationObject;
        public ScenarisationCallbacks m_callbacks = new ScenarisationCallbacks();

        public List<ScenarisationTarget> m_targets = new List<ScenarisationTarget>();

        [SerializeField]
        private float m_callbackDuration = 1f;

        #region MonoBehaviour

        protected virtual void Start() {
            if(m_scenarisationObject == null) {
#if DEBUG_SCENAR
                //Debug.Log("[SCENARISATION] There is no ScenarioObject linked to this Component : " + name);
#endif
            }

            if(m_targets == null || m_targets.Count == 0) {
#if DEBUG_SCENAR
                //Debug.Log("[SCENARISATION] There are no Targets linked to this Component : " + name + ". Adding temporary Target on his gameObject.");
#endif
                m_targets = new List<ScenarisationTarget>() { gameObject.AddComponent<ScenarisationTarget>() };
            }
            m_targets.ForEach(x => x.m_scenarisationComponent = this);

            m_scenarisationObject.m_registeredComponent = this;
            ResetScenarisationObject();
        }

        #endregion

        #region Scenarisation

        /// <summary>
        /// Call this method to Validate the related ScenarisationObject.
        /// </summary>
        public virtual void Validate() {

            if (!m_scenarisationObject.m_results.reached)
                return;

            m_scenarisationObject.Succeed();
        }

        /// <summary>
        /// Call this method to Refuse the related ScenarisationObject.
        /// </summary>
        public virtual void Refuse() {
            if(!m_scenarisationObject.m_results.reached)
                return;

            m_scenarisationObject.Fail();
        }

        /// <summary>
        /// Resets this ScenarisationComponent.
        /// </summary>
        public virtual void ResetScenarisationObject() {
            m_scenarisationObject.m_results.Reset();
            m_scenarisationObject.Reset();
        }


        #endregion

        #region Targets

        /// <summary>
        /// Toggles all ScenarisationTargets associated with the related ScenarisationObejct.
        /// </summary>
        /// <param name="truth">ON/OFF.</param>
        public void ToggleTargets(bool truth) {
            m_targets.ForEach(x => x.ToggleTarget(truth));
        }

        /// <summary>
        /// Utils method used to invoke the ScenarisationEvent corresponding to a given ScenarisationState.
        /// </summary>
        /// <param name="state">The state of the ScenarisationObject.</param>
        /// <param name="duration">The duration of the callback, default value can be set in "Settings".</param>
        public void Invoke(ScenarisationState scenarisationState, float duration = -1f) {
            if(duration == -1f)
                duration = m_callbackDuration;

            switch(scenarisationState) {

                case ScenarisationState.FirstReached:
                m_callbacks.UponFirstReaching.Invoke(m_scenarisationObject, duration);
                m_callbacks.UponReaching.Invoke(m_scenarisationObject, duration);
                break;
                case ScenarisationState.Reached:
                m_callbacks.UponReaching.Invoke(m_scenarisationObject, duration);
                break;
                case ScenarisationState.Successsed:
                m_callbacks.UponSucceeding.Invoke(m_scenarisationObject, duration);
                break;
                case ScenarisationState.Failed:
                m_callbacks.UponFailing.Invoke(m_scenarisationObject, duration);
                break;
                case ScenarisationState.Skipped:
                m_callbacks.UponSucceeding.Invoke(m_scenarisationObject, duration);
                m_callbacks.UponSkipping.Invoke(m_scenarisationObject, duration);
                break;

                default:
                return;
            }
        }

        #endregion

    }

}