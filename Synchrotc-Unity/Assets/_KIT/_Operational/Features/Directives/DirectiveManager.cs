﻿using System.Collections;
using System.Collections.Generic;
#if KEYVEO_LOCALIZATION
using Keyveo.Features.Localization;
#endif
using Keyveo.Scenarisation;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Keyveo.Features.Guidance {

    /// <summary>
    /// FeatureManager handling scenarisation text directives.
    /// </summary>
    public class DirectiveManager : MonoBehaviour, FeatureManager {


        [SerializeField]
        private bool m_autoSetUp = true;

        [SerializeField, ShowIf("m_autoSetUp")]
        private List<DirectiveHandler> m_directivePrefabs = new List<DirectiveHandler>();

        [HideIf("m_autoSetUp")]
        public List<DirectiveHandler> m_directiveHandlers = new List<DirectiveHandler>();

        private bool m_isDisplayed = false;

#region MonoBehaviour
        
        protected virtual void Start() {
            //if(m_autoSetUp) {
            //    CreateDirectiveHandlers();
            //}
            InstantHideHandlers();
        }

#endregion

#region Handler Management

        //public virtual void CreateDirectiveHandlers() {
        //    foreach(DirectiveHandler item in m_directivePrefabs) {
        //        DirectiveHandler handler = Instantiate(item);
        //        PlaceDirectiveHandler(handler);
        //        m_directiveHandlers.Add(handler);
        //    }
        //}

        //public virtual void PlaceDirectiveHandler(DirectiveHandler directiveHandler) {
        //    directiveHandler.transform.SetParent(Camera.main.transform);
        //    directiveHandler.transform.localPosition = Vector3.forward;
        //    directiveHandler.transform.localEulerAngles = Vector3.zero;
        //}

        /// <summary>
        /// Updates the text of all active DirectiveHandlers.
        /// </summary>
        /// <param name="text">The text to print.</param>
        public virtual void UpdateDirectiveHandlers(string text) {
            foreach(DirectiveHandler item in m_directiveHandlers) {
                item.UpdateText(text);
            }
        }

        public virtual string TranslateDirective(ScenarisationObject so, ScenarisationState state)
        {
#if KEYVEO_LOCALIZATION
            if (LocalizationManager.Instance != null) {
                //     return so.m_description;
                return LocalizationManager.Instance.ApplyTranslationTo(so, state);
            }
            else
            {
#endif
                switch (state)
                {
                    case ScenarisationState.Successsed:
                        return so.m_successMessage;
                    case ScenarisationState.Failed:
                        return so.m_failMessage;
                    case ScenarisationState.Skipped:
                        return so.m_skipMessage;
                    default:
                        return so.m_description;
                }
#if KEYVEO_LOCALIZATION
            }
#endif
        }

        /// <summary>
        /// Shows all active DirectiveHandlers.
        /// </summary>
        public virtual void ShowHandlers() {
            foreach(DirectiveHandler item in m_directiveHandlers) {
                item.Show();
            }
            m_isDisplayed = true;
        }

        /// <summary>
        /// Hides all active DirectiveHandlers.
        /// </summary>
        public virtual void HideHandlers()
        {
            foreach (DirectiveHandler item in m_directiveHandlers)
            {
                item.Hide();
            }
            m_isDisplayed = false;
        }

        /// <summary>
        /// Hides all active DirectiveHandlers.
        /// </summary>
        public virtual void InstantHideHandlers()
        {
            foreach (DirectiveHandler item in m_directiveHandlers)
            {
                item.InstantHide();
            }
            m_isDisplayed = false;
        }

        /// <summary>
        /// Toggle all active DirectiveHandlers.
        /// </summary>
        public virtual void ToggleHelp() {
            if(KeyveoManager.Instance.GetPressedUp(Buttons.App)) {
                if(m_isDisplayed)
                    HideHandlers();
                else
                    ShowHandlers();
            }
        }

        /// <summary>
        /// Temporarily shows all active DirectiveHandlers.
        /// </summary>
        public virtual void TemporarilyShowHandlers(float duration, float delay = 0) {
            StopAllCoroutines();
            foreach(DirectiveHandler item in m_directiveHandlers) {
                StartCoroutine(item.TemporarilyShow(duration, delay));
            }
        }

#endregion

#region FeatureManager Implementation

        public void UponReachingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f) {
            if(scenarisationObject is ObjectiveSO || (scenarisationObject is StepSO && ScenarioManager.Instance.m_isSequencing)) {
                InstantHideHandlers();
                UpdateDirectiveHandlers(TranslateDirective(scenarisationObject, ScenarisationState.Reached));
                TemporarilyShowHandlers(5f);
            }
        }

        public void UponSucceedingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f)
        {
            InstantHideHandlers();
            UpdateDirectiveHandlers(scenarisationObject.m_successMessage);
            TemporarilyShowHandlers(5f);
        }

        public void UponFailingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f) {
            Debug.Log("Fail Directive");
            InstantHideHandlers();
            UpdateDirectiveHandlers(scenarisationObject.m_failMessage);
            TemporarilyShowHandlers(5f);
        }

        public void UponSkippingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f)
        {
            InstantHideHandlers();
            UpdateDirectiveHandlers(scenarisationObject.m_skipMessage);
            TemporarilyShowHandlers(5f);
        }

#endregion



    }

}