﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Keyveo.Features.Guidance {

    /// <summary>
    /// Interface to implement to define a preview.
    /// </summary>
    public interface IPreviewable {
        void StartPreview();
        void StopPreview();
    }

}