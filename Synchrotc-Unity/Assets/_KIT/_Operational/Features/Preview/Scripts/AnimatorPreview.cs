﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Keyveo.Features.Guidance {

    /// <summary>
    /// Animated preview, using an Animator component.
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class AnimatorPreview : MonoBehaviour, IPreviewable {

        [SerializeField]
        private Transform m_visualTransform = null;

        private Animator m_animator = null;

        #region MonoBehaviour

        void Start() {
            if(m_visualTransform == null)
                m_visualTransform = transform.Find("Visual");

            m_animator = GetComponent<Animator>();

            StopPreview();
        }

        #endregion

        #region IPreviewable Implementation

        public void StartPreview() {
            m_visualTransform.gameObject.SetActive(true);
            m_animator.Play(m_animator.GetCurrentAnimatorStateInfo(0).fullPathHash);
        }

        public void StopPreview() {
            m_animator.StopPlayback();
            m_visualTransform.gameObject.SetActive(false);
        }

        #endregion
    }

}