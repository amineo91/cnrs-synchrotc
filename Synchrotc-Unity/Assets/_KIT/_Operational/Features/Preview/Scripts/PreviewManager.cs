﻿using Keyveo.Scenarisation;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Keyveo.Features.Guidance {


    /// <summary>
    /// FeatureManager handling the previews related to the ScenarisationTargets in the scene.
    /// </summary>
    public class PreviewManager : MonoBehaviour, FeatureManager {

        private List<AnimatorPreview> m_currentPreviews = new List<AnimatorPreview>();

       /// <summary>
       /// Toggles the previews related to the current step of the scenario.
       /// </summary>
       /// <param name="truth"></param>
        public void ToggleCurrentPreviews(bool truth) {
            m_currentPreviews.ForEach(x => {
                if(x != null) {
                    if(truth)
                        x.StartPreview();
                    else
                        x.StopPreview();
                }
            });
        }


        #region FeatureManager Implementation

        public void UponReachingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f) {

            m_currentPreviews = scenarisationObject.m_registeredComponent.m_targets.Select(x => x.m_preview).ToList();
            ToggleCurrentPreviews(true);
        }


        public void UponFailingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f) {
            throw new System.NotImplementedException();
        }
        public void UponSkippingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f) {
            UponSucceedingScenarisationObject(scenarisationObject, duration);
        }

        public void UponSucceedingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f) {
            ToggleCurrentPreviews(false);
        }

        #endregion

    }

}
