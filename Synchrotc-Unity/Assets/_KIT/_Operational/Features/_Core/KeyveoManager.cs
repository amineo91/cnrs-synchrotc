﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if KEYVEO_VR
using Valve.VR;
using Valve.VR.InteractionSystem;
#endif

namespace Keyveo {

    /// <summary>
    /// Enumeration of compatible APIs.
    /// </summary>
    public enum APIs {
        SteamVR
    }

    /// <summary>
    /// Global Button names.
    /// </summary>
    public enum Buttons {
        Trigger,
        Grip,
        Help,
        App
    }

    /// <summary>
    /// [DEPRECEATED]
    /// </summary>
    public class KeyveoManager : MonoBehaviour {
        public static KeyveoManager Instance;
        public APIs currentAPI;

#if KEYVEO_VR
        public Player SVR_Player;
#endif

        private void Awake() {
            Instance = this;
        }

        // Start is called before the first frame update
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }

        

        public bool GetPressedUp(Buttons button)
        {
#if KEYVEO_VR
            switch (currentAPI)
            {
                case APIs.SteamVR:
                //SteamVR_Input.
                return SteamVR_Actions._default.GrabPinch.stateUp;
            }
#endif

            return false;
        }

#if KEYVEO_VR
        private SteamVR_Action ButtonToAction(Buttons button) {
            switch (button) {
                case Buttons.Trigger:
                return SteamVR_Actions._default.GrabPinch;
                case Buttons.Grip:
                return SteamVR_Actions._default.GrabGrip;
            }
            return null;
        }
#endif


    }

}