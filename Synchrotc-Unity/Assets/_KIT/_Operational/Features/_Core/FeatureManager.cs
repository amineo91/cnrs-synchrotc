﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Keyveo.Features {

    /// <summary>
    /// Interface to implement a Manager connected to the ScenarioManager's callback system.
    /// </summary>
    public interface FeatureManager {

        void UponReachingScenarisationObject(Scenarisation.ScenarisationObject scenarisationObject, float duration);
        void UponSucceedingScenarisationObject(Scenarisation.ScenarisationObject scenarisationObject, float duration);
        void UponFailingScenarisationObject(Scenarisation.ScenarisationObject scenarisationObject, float duration);
        void UponSkippingScenarisationObject(Scenarisation.ScenarisationObject scenarisationObject, float duration);

    }

}