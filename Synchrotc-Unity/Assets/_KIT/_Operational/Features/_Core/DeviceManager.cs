﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Keyveo.Features {

    /// <summary>
    /// The DeviceManager handles all FeatureManager in the scene.
    /// </summary>
    public class DeviceManager : MonoBehaviour {

        [SerializeField]
        protected bool autoSetUp = true;

        public FeatureManager[] featureManagers;


        // Use this for initialization
        protected virtual void Start() {
            if (autoSetUp)
                SetUp();


        }


        public virtual void SetUp() {
            featureManagers = GetComponentsInChildren<FeatureManager>();

            Scenarisation.ScenarioManager sM = Scenarisation.ScenarioManager.Instance;
            
            foreach (FeatureManager item in featureManagers) {
                sM.m_baseStepCallbacks.UponReaching.AddListener(item.UponReachingScenarisationObject);
                sM.m_baseStepCallbacks.UponSucceeding.AddListener(item.UponSucceedingScenarisationObject);
                sM.m_baseStepCallbacks.UponFailing.AddListener(item.UponFailingScenarisationObject);
                sM.m_baseStepCallbacks.UponSkipping.AddListener(item.UponSkippingScenarisationObject);
            }

        }
        
    }
}