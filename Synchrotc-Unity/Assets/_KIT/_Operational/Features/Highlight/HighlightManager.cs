﻿using System.Collections;
using System.Collections.Generic;
using Keyveo.Scenarisation;
using UnityEngine;
using HighlightingSystem;
using System.Linq;

namespace Keyveo.Features.Guidance {


    /// <summary>
    /// [DEPRECEATED] FeatureManager handling the highlight of ScenarisationTarget and other GameObject in the scene.
    /// </summary>
    /// <remarks>
    /// Requires the HighlightingSystem Unity Plugins.
    /// </remarks>
    public class HighlightManager : MonoBehaviour, FeatureManager {

        public static HighlightManager Instance;

        [SerializeField]
        private Gradient m_GradientColor = null;
        [SerializeField]
        [ColorUsage(true, true)]
        private Color m_ConstantColor = Color.blue;

        [SerializeField]
        private float m_GradientPulseSpeed = 1f;


        private List<GameObject> m_Targets = new List<GameObject>();

        private void Awake() {
            Instance = this;
        }

        private void Start() {
            if (Camera.main.gameObject.GetComponent<HighlighterRenderer>() == null)
                Debug.LogWarning("[DEVICE] HighlightManager needs the HighlightingRenderer to be attached to the main Camera to work.");
        }

        public void SetTargets(List<GameObject> targets) {
            m_Targets = targets;
        }
        public void SetTargets(GameObject target) {
            m_Targets = new List<GameObject> { target };
        }

        public void AddTarget(GameObject target) {
            if (!m_Targets.Contains(target))
                m_Targets.Add(target);
        }

        public void HighlightTargets(bool constantHighlight) {
            foreach (GameObject item in m_Targets) {
                HighlightGameObject(item, constantHighlight);
            }
        }

        public void UnlightTargets() {
            foreach (GameObject item in m_Targets) {
                UnlightGameObject(item);
            }
        }

  
        public void HighlightThese(List<GameObject> gameObjects, bool constantHighlight) {
            foreach (GameObject item in gameObjects) {
                HighlightGameObject(item, constantHighlight);
            }
        }

        public void UnlightThese(List<GameObject> gameObjects) {
            foreach (GameObject item in gameObjects) {
                UnlightGameObject(item);
            }
        }

        public virtual void HighlightGameObject(GameObject gameObject, bool constantHighlight) {
            if (gameObject.GetComponent<Highlighter>())
                return;

            Highlighter highlighter = gameObject.AddComponent<Highlighter>();

            if (constantHighlight) {
                highlighter.constantColor = m_ConstantColor;
                highlighter.constant = true;
            } else {
                highlighter.tweenGradient = m_GradientColor;
                highlighter.tweenDuration = m_GradientPulseSpeed;
                highlighter.tween = true;
            }
        }

        public virtual void UnlightGameObject(GameObject gameObject) {
            Destroy(gameObject.GetComponent<Highlighter>());
        }

        #region FeatureManager Implementation

        public virtual void UponReachingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f) {
            if (scenarisationObject.m_registeredComponent == null) {
                Debug.Log("No Scenarisation Component found for the ScenarisationObject " + scenarisationObject);
                return;
            }
            if (scenarisationObject.m_registeredComponent.m_targets.Count != 0) {
                SetTargets(scenarisationObject.m_registeredComponent.m_targets.Select(x=> x.gameObject).ToList());
            } else {
                SetTargets(scenarisationObject.m_registeredComponent.gameObject);
            }
            HighlightTargets(false);
        }

        public virtual void UponSucceedingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f) {
            UnlightTargets();
            SetTargets(new List<GameObject>());
        }

        public virtual void UponFailingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f) {
            UnlightTargets();
            SetTargets(new List<GameObject>());
        }

        public virtual void UponSkippingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f) {
            UnlightTargets();
            SetTargets(new List<GameObject>());
        }

        #endregion

    }

}