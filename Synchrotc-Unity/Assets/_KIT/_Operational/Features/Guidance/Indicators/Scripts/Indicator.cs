﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Linq;
using Keyveo.ExtensionMethods;
using TMPro;
using DG.Tweening;
#if KEYVEO_LOCALIZATION
using Keyveo.Features.Localization;
#endif

namespace Keyveo.Features.Guidance {

    public enum CanvasPosition {
        CenterTwoLastPoints,
        LastPoint
    }

    /// <summary>
    /// An Indicator is a spatialized tooltip that indicates the User where the next ScenarisationObject requires him to go or interact.
    /// </summary>
    [System.Serializable]
    public class Indicator : MonoBehaviour {

        public string m_textToDisplay;

        [SerializeField, FoldoutGroup("Setup")]
        public List<Transform> m_points = new List<Transform>();

        [SerializeField, FoldoutGroup("Setup")]
        private GameObject m_spherePrefab = null;

        [SerializeField, FoldoutGroup("Setup")]
        private GameObject m_spherePrefabMiniMap = null;

        [SerializeField, FoldoutGroup("Setup")]
        private GameObject m_cylinderPrefab = null;

        [SerializeField, FoldoutGroup("Setup")]
        private GameObject m_cylinderPrefabMiniMap = null;

        [SerializeField, FoldoutGroup("Setup")]
        private GameObject m_canvasPrefab = null;

        [SerializeField, FoldoutGroup("Setup")]
        private CanvasPosition m_CanvasPosition = CanvasPosition.CenterTwoLastPoints;

        [SerializeField, FoldoutGroup("Setup")]
        private bool m_ExcludeRootPoint = false;
        

        private Transform m_positionParent;
        private Transform m_visualParent;

        private Billboard m_billboard;
        private List<GameObject> m_spheres = new List<GameObject>();
        private List<GameObject> m_spheresMinimap = new List<GameObject>();
        private List<GameObject> m_cylinders = new List<GameObject>();
        private List<GameObject> m_cylindersMiniMap = new List<GameObject>();
        private List<float> m_cylinderLength = new List<float>();
        private TextMeshProUGUI m_text;

        private Sequence m_sequence = null;

#region MonoBehaviour

        // Start is called before the first frame update
        void Start() {
            m_billboard = GetComponent<Billboard>();


            if(m_visualParent == null) {
                m_visualParent = transform.Find("Visuals");

                if(m_visualParent == null)
                    CreateVisual();
                else
                    RecoverVisualReferences();
            }

            Hide(0);
        }

#endregion

#region Editor Setup

        [Button, FoldoutGroup("Setup")]
        private void CreateBasicIndicator() {
            if(m_positionParent == null) {
                GameObject go = new GameObject("Positions");
                go.transform.SetParent(transform);
                go.transform.Reset();
                m_positionParent = transform.Find("Positions");
            }

            GameObject p1 = new GameObject("P1");
            p1.transform.SetParent(m_positionParent);
            p1.transform.localPosition = Vector3.one + Vector3.back;


            GameObject p2 = new GameObject("P2");
            p2.transform.SetParent(m_positionParent);
            p2.transform.localPosition = Vector3.one + Vector3.back + Vector3.right;
        }

        [Button, FoldoutGroup("Setup")]
        public void CreateVisual(bool minimapOnly = false) {
            //Debug.Log("Recreating Visuals.");
            //RecoverPositions();

            if(m_visualParent != null)
                DestroyImmediate(m_visualParent.gameObject);

            m_spheres = new List<GameObject>();
            m_cylinders = new List<GameObject>();
            m_cylinderLength = new List<float>();


            if(m_spherePrefab == null) {
                Debug.Log("[INDICATOR] Sphere Prefab reference is null.");
                return;
            }
            if(m_cylinderPrefab == null) {
                Debug.Log("[INDICATOR] Cylinder Prefab reference is null.");
                return;
            }
            if(m_points == null || m_points.Count == 0) {
                Debug.Log("[INDICATOR] No points were found.");
                return;
            }


            GameObject go = new GameObject("Visuals");
            go.transform.SetParent(transform);
            go.transform.localPosition = Vector3.zero;
            m_visualParent = go.transform;

            GameObject c = null;
            GameObject v = null;
            GameObject u = null;
            GameObject s = null;

            for (int i = 0; i < m_points.Count; i++) {

                u = Instantiate(m_spherePrefabMiniMap, m_visualParent);
                u.transform.position = m_points[i].position;
                m_spheresMinimap.Add(u);

                if (!minimapOnly)
                {
                    s = Instantiate(m_spherePrefab, m_visualParent);
                    s.transform.position = m_points[i].position;
                    m_spheres.Add(s);
                }

                if(i != m_points.Count - 1)
                {

                    u.transform.LookAt(m_points[i + 1], u.transform.right);
                    v = Instantiate(m_cylinderPrefabMiniMap, u.transform);
                    v.transform.position = m_points[i].position;
                    v.transform.localEulerAngles = 90 * Vector3.right;
                    v.transform.localScale = new Vector3(1, Vector3.Distance(u.transform.position, m_points[i + 1].position) / 50, 1);
                    m_cylindersMiniMap.Add(v);

                    if (!minimapOnly)
                    {
                        s.transform.LookAt(m_points[i + 1], s.transform.right);
                        c = Instantiate(m_cylinderPrefab, s.transform);
                        c.transform.position = m_points[i].position;
                        c.transform.localEulerAngles = 90 * Vector3.right;
                        c.transform.localScale = new Vector3(1, Vector3.Distance(s.transform.position, m_points[i + 1].position), 1);
                        m_cylinders.Add(c);
                        m_cylinderLength.Add(Vector3.Distance(s.transform.position, m_points[i + 1].position));
                    }

                }
                else if (m_canvasPrefab != null){
                    GameObject t = Instantiate(m_canvasPrefab, s.transform);

                    RectTransform rectTransform = t.GetComponent<RectTransform>();

                    // If canvas prefab is a Canvas
                    if (rectTransform != null)
                    {
                        switch (m_CanvasPosition)
                        {
                            case CanvasPosition.CenterTwoLastPoints:
                                float dist = Vector3.Distance(m_points[i - 1].position, m_points[i].position);
                                t.GetComponent<RectTransform>().localPosition = new Vector3(-0.5f * dist, dist * 3f / 8f, 0);
                                t.GetComponent<RectTransform>().localEulerAngles = new Vector3(0, 180, 0);
                                t.GetComponent<RectTransform>().sizeDelta = new Vector2(dist, dist * 3f / 4f);
                                break;
                            case CanvasPosition.LastPoint:
                                // Nothing to do
                                break;
                        }
                    }
                    // Otherwise it's a wrapper

                    m_text = t.GetComponentInChildren<TextMeshProUGUI>();
                    //m_text.text = m_textToDisplay;
                }
            }
        }

        private void RecoverPositions() {
            m_positionParent = transform.Find("Positions");
            m_points = m_positionParent.GetComponentsInChildren<Transform>().ToList();
            if (m_ExcludeRootPoint) {
                m_points.Remove(m_positionParent);
            }
        }

#endregion

        private void RecoverVisualReferences() {
            m_spheres = new List<GameObject>();
            m_cylinders = new List<GameObject>();
            m_cylinderLength = new List<float>();
            m_text = GetComponentInChildren<TextMeshProUGUI>();

            for(int i = 0; i < m_points.Count; i++) {
                m_spheres.Add(m_visualParent.GetChild(i).gameObject);
                if(i!= m_points.Count - 1) { 
                m_cylinders.Add(m_visualParent.GetChild(i).GetChild(0).gameObject);
                m_cylinderLength.Add(m_visualParent.GetChild(i).GetChild(0).localScale.y);
                }
            }
        }

        /// <summary>
        /// Shows this Indicator.
        /// </summary>
        public void Show() {
            if(m_sequence != null)
                m_sequence.Kill();

#if KEYVEO_LOCALIZATION
            if (LocalizationManager.Instance != null)
                m_textToDisplay = LocalizationManager.Instance.ApplyTranslationTo(m_text.GetComponent<LocalizedItem>());
#endif
            m_sequence = ShowAnimation();
            m_sequence.Restart();
        }

        /// <summary>
        /// Hides this Indicator.
        /// </summary>
        public void Hide(float duration = 0.5f) {
            if(m_sequence != null)
                m_sequence.Kill();
            m_sequence = HideAnimation(duration);
            m_sequence.Restart();
        }

        /// <summary>
        /// Indicator's Fade in Animation.
        /// </summary>
        public virtual Sequence ShowAnimation() {
            Sequence deploy = DOTween.Sequence();
            for(int i = 0; i < m_spheres.Count; i++) {
                deploy.Append(SphereAnimation(m_spheres[i], true));
                if(i != m_spheres.Count - 1)
                    deploy.Append(CylinderAnimation(m_cylinders[i], m_cylinderLength[i], true));
            }

            if (m_text != null)
            {
                deploy.Append(TextAnimation(m_text, true));
            }

            return deploy;
        }


        /// <summary>
        /// Indicator's Fade out Animation.
        /// </summary>
        public virtual Sequence HideAnimation(float duration) {
            Sequence deploy = DOTween.Sequence();
            if (m_text != null)
            {
                deploy.Append(TextAnimation(m_text, false));
            }

            for(int i = 0; i < m_spheres.Count; i++) {
                deploy.Append(SphereAnimation(m_spheres[m_spheres.Count - 1 - i], false, duration*0.5f));
                if(i != m_spheres.Count - 1)
                    deploy.Append(CylinderAnimation(m_cylinders[m_spheres.Count - i - 2], m_cylinderLength[m_spheres.Count - i - 2], false, duration));
            }

            return deploy;
        }

        /// <summary>
        /// Make this Indicator's spheres dissolve in or out.
        /// </summary>
        /// <param name="sphere">The sphere to dissolve (or any GO).</param>
        /// <param name="inAnimation">IN/OUT.</param>
        /// <param name="duration">Duration of the animation.</param>
        /// <returns></returns>
        protected virtual Tweener SphereAnimation(GameObject sphere, bool inAnimation, float duration = 0.25f) {

            return DOTween.To(
                () => sphere.GetComponent<MeshRenderer>().sharedMaterial.GetFloat("_Amount"),
                x => sphere.GetComponent<MeshRenderer>().sharedMaterial.SetFloat("_Amount", x),
                inAnimation ? 0 : 1,
                duration
                ).SetEase(Ease.InOutSine);
        }

        /// <summary>
        /// Make this Indicator's cylinders scale in or out.
        /// </summary>
        /// <param name="cylinder">The cylinder to scale (or any GO).</param>
        /// <param name="length">The actual length of the cylinder (the up scale).</param>
        /// <param name="inAnimation">IN/OUT.</param>
        /// <param name="duration">Duration of the animation.</param>
        /// <returns></returns>
        protected virtual Tweener CylinderAnimation(GameObject cylinder, float length, bool inAnimation, float duration = 0.5f) {
            if(inAnimation)
                return cylinder.transform.DOScaleY(length, duration).SetEase(Ease.InSine).OnStart(() => cylinder.SetActive(true));
            else
                return cylinder.transform.DOScaleY(0, duration).SetEase(Ease.InSine).OnComplete(() => cylinder.SetActive(false));
        }

        /// <summary>
        /// Writes this Indicator's text.
        /// </summary>
        /// <param name="text">The text to display.</param>
        /// <param name="inAnimation">IN/OUT.</param>
        /// <param name="duration">Duration of the animation.</param>
        /// <returns></returns>
        protected virtual Tweener TextAnimation(TextMeshProUGUI text, bool inAnimation, float duration = 0.5f) {
            return text.DOWrite(inAnimation ? m_textToDisplay : "", duration);
        }


    }

}