﻿using Keyveo.Scenarisation;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Keyveo.Features.Guidance {


    public enum GuidanceType {
        Classic,

        VR_Feet,
        VR_Hand
    }

    /// <summary>
    /// A GuidanceHandler points at the current ScenarisationTarget.
    /// </summary>
    public class GuidanceHandler : MonoBehaviour {

        //[SerializeField]
        //private bool m_autoSetup = false;

        [SerializeField]
        private GuidanceType m_guidanceType = GuidanceType.Classic;

        [SerializeField, ShowIf("m_autoSetup"), Tooltip("The moving part of the Handler, is set to this gameObject's transform is nothing is referenced here.")]
        protected Transform movingTransform;

        [SerializeField, ShowIf("m_autoSetup")]
        protected Vector3 offsetPosition = Vector3.zero;

        private Transform m_Target;
        private Resizer m_Resizer;

        #region MonoBehaviour

        protected virtual void Awake() {
            if(movingTransform == null)
                movingTransform = transform;

            m_Resizer = GetComponentInChildren<Resizer>();
        }

        // Use this for initialization
        void Start() {
            //if(m_autoSetup)
            //    SetUp();

            Hide();
        }

        // Update is called once per frame
        void Update() {

            if(m_guidanceType == GuidanceType.VR_Feet) {
                Vector3 camPosition = Camera.main.transform.position;
                transform.position = new Vector3(camPosition.x, transform.position.y, camPosition.z);
            }

            if(m_Target != null) {
                transform.LookAt(m_Target);
                transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
            }
        } 

        #endregion


        //public virtual void SetUp() {
        //    switch(m_guidanceType) {
        //        case GuidanceType.VR_Feet:
        //        // TODO Place at feet
        //        break;
        //        case GuidanceType.VR_Hand:
        //        // TODO Place at Hand
        //        break;
        //        default:
        //        break;
        //    }

        //    transform.SetParent(Camera.main.transform);
        //    transform.localPosition = offsetPosition;
        //}

            /// <summary>
            /// Shows this GuidanceHandler.
            /// </summary>
        public virtual void Show() {
            if(m_Target != null) {
                m_Resizer.Grow();
            }
        }

        /// <summary>
        /// Hides this GuidanceHandler.
        /// </summary>
        public virtual void Hide() {
            m_Resizer.Shrink();
        }

        /// <summary>
        /// Temporarily shows this GuidanceHandler.
        /// </summary>
        public IEnumerator TemporarilyShow(float duration) {
            Show();
            yield return new WaitForSeconds(duration);
            Hide();
        }


        /// <summary>
        /// Given a ScenarisationComponent, set this GuidanceHandler's current target.
        /// </summary>
        public void SetTargets(ScenarisationComponent scenarisationComponent) {
            if(scenarisationComponent == null)
                return;

            if(scenarisationComponent.m_targets == null || scenarisationComponent.m_targets.Count != 0)
                m_Target = scenarisationComponent.m_targets[0].transform;
            else
                m_Target = scenarisationComponent.transform;

            Show();
        }

        /// <summary>
        /// Set this GuidanceHandler's current target.
        /// </summary>
        public void SetTarget(GameObject go) {
            if(go == null)
                return;

            m_Target = go.transform;
            Show();
        }

    }

}