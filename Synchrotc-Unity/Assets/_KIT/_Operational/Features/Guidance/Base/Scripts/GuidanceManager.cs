﻿using System.Collections;
using System.Collections.Generic;
using Keyveo.Scenarisation;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Keyveo.Features.Guidance {

    /// <summary>
    /// FeatureManager that handles all GuidanceHandler, making sure they are correctly pointing to the right targets.
    /// </summary>
    public class GuidanceManager : MonoBehaviour, FeatureManager {

        //[SerializeField]
        //private bool m_autoSetup = false;

        [SerializeField, ShowIf("m_autoSetup")]
        private List<GuidanceHandler> m_guidancePrefabs = new List<GuidanceHandler>();

        [SerializeField, HideIf("m_autoSetup")]
        private List<GuidanceHandler> m_guidanceHandlers = new List<GuidanceHandler>();

        #region MonoBehaviour

        void Start() {
            //if(m_autoSetup) {
            //    CreateGuidanceHandlers();
            //}
            HideHandlers();
        }

        #endregion

        //public virtual void CreateGuidanceHandlers() {
        //    foreach(GuidanceHandler item in m_guidancePrefabs) {
        //        GuidanceHandler gH = Instantiate(item);
        //        gH.SetUp();
        //        m_guidanceHandlers.Add(gH);
        //    }
        //}

        /// <summary>
        /// Updates the target of all active GuidanceHandler.
        /// </summary>
        /// <param name="target">The new target to give.</param>
        public virtual void UpdateHandlers(GameObject target) {
            foreach(GuidanceHandler item in m_guidanceHandlers) {
                item.SetTarget(target);
            }
        }

        /// <summary>
        /// Shows all active GuidanceHandler.
        /// </summary>
        public virtual void ShowHandlers() {
            foreach(GuidanceHandler item in m_guidanceHandlers) {
                item.Show();
            }
        }

        /// <summary>
        /// Hides all active GuidanceHandler.
        /// </summary>
        public virtual void HideHandlers() {
            foreach(GuidanceHandler item in m_guidanceHandlers) {
                item.Hide();
            }
        }

        /// <summary>
        /// Temporarily shows all active GuidanceHandler.
        /// </summary>
        public virtual void TemporarilyShowHandlers(float duration) {
            foreach(GuidanceHandler item in m_guidanceHandlers) {
                StartCoroutine(item.TemporarilyShow(duration));
            }
        }

        /// <summary>
        /// Toggles the Indicator related to the given ScenarisationObject.
        /// </summary>
        /// <param name="scenarisationObject">The related ScenarisationObject.</param>
        /// <param name="truth">ON/OFF</param>
        public virtual void ToggleIndicators(ScenarisationObject scenarisationObject, bool truth) {
            if(scenarisationObject.m_registeredComponent == null)
                return;
            if(scenarisationObject.m_registeredComponent.m_targets == null)
                return;

            foreach(ScenarisationTarget item in scenarisationObject.m_registeredComponent.m_targets) {
                if(item.m_indicator != null) {
                    if(truth)
                        item.m_indicator.Show();
                    else
                        item.m_indicator.Hide();
                }
            }

        }

        #region FeatureManager Implementation

        public void UponReachingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f) {
            foreach(GuidanceHandler item in m_guidanceHandlers) {
                item.SetTargets(scenarisationObject.m_registeredComponent);
                StartCoroutine(item.TemporarilyShow(3f));
            }

            ToggleIndicators(scenarisationObject, true);
        }

        public void UponSucceedingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f) {
            HideHandlers();
            ToggleIndicators(scenarisationObject, false);
        }

        public void UponFailingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f) {
            HideHandlers();
            ToggleIndicators(scenarisationObject, false);
        }

        public void UponSkippingScenarisationObject(ScenarisationObject scenarisationObject, float duration = 0f) {
            HideHandlers();
            ToggleIndicators(scenarisationObject, false);
        }

        #endregion

    }


}