﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine.UI;

namespace Keyveo.FrontEnd {

    /// <summary>
    /// ScriptableObject containing FrontEnd data (i.e. color sets).
    /// </summary>
    [CreateAssetMenu(fileName = "NewFrontEndSkin", menuName = "Keyveo/FrontEnd/Skin")]
    public class FrontEndSkin : SerializedScriptableObject {

        [FoldoutGroup("UI Colors")]
        public Dictionary<UITag, Color> uiColors = new Dictionary<UITag, Color>();
        [FoldoutGroup("UI Colors")]
        public Dictionary<TextTag, Color> textColors = new Dictionary<TextTag, Color>();

        [FoldoutGroup("UI Font")]
        public TMP_FontAsset font;
        
        [FoldoutGroup("UI Font")]
        public TMP_FontAsset font_noz;

        [FoldoutGroup("Material Colors")]
        public List<MaterialColor> materialColors = new List<MaterialColor>();

        /// <summary>
        /// Applies this skin to the given Transform.
        /// </summary>
        /// <param name="parent">The Transform to reskin.</param>
        public void Apply(Transform parent) {
            foreach(UITag c in uiColors.Keys) {
                foreach(Transform tr in FindChildRec(parent, c.ToString())) {
                    if(tr.GetComponent<Image>() != null)
                        tr.GetComponent<Image>().color = uiColors[c];
                }
            }
            foreach(TextTag c in textColors.Keys) {
                foreach(Transform tr in FindChildRec(parent, c.ToString())) {
                    if(tr.GetComponent<TextMeshProUGUI>() != null)
                    {
                        if (tr.GetComponent<FrontEndItem>().isNOZ)
                        {
                            tr.GetComponent<TextMeshProUGUI>().font = font_noz;
                        } else
                        {
                            tr.GetComponent<TextMeshProUGUI>().font = font;
                        }
                        tr.GetComponent<TextMeshProUGUI>().UpdateFontAsset();
                        tr.GetComponent<TextMeshProUGUI>().color = textColors[c];
                    }
                    else if (tr.GetComponent<Text>() != null)
                    {
                        tr.GetComponent<Text>().color = textColors[c];
                    }
                }
            }

            foreach(MaterialColor c in materialColors) {
                foreach(Transform tr in FindChildRec(parent, c.m_tag.ToString())) {
                    if(tr.GetComponent<MeshRenderer>() != null)
                        c.ApplyToMaterial(tr.GetComponent<MeshRenderer>().material);
                    else if (tr.GetComponent<Image>() != null)
                        c.ApplyToMaterial(tr.GetComponent<Image>().material);


                }
            }
        }

        private List<Transform> FindChildRec(Transform tr, string filter) {

            List<Transform> outList = new List<Transform>();

            for(int i = 0; i < tr.childCount; i++) {
                if(tr.GetChild(i).GetComponent<FrontEndItem>() != null) {
                    if(tr.GetChild(i).GetComponent<FrontEndItem>().Filter(filter))
                        outList.Add(tr.GetChild(i));
                }

                outList.AddRange(FindChildRec(tr.GetChild(i), filter));
            }

            return outList;
        }

    }

    /// <summary>
    /// Util class, helping modifying Materials.
    /// </summary>
    [System.Serializable]
    public class MaterialColor {
        public MaterialTag m_tag;

        [ColorUsage(true, true)]
        public Color m_color;

        public void ApplyToMaterial(Material material) {
            switch(m_tag) {

                case MaterialTag.Emission:
                material.SetColor("_EmissionColor", m_color);
                break;

                case MaterialTag.ColorTint:
                material.SetColor("_TintColor", m_color);
                break;

                default:
                material.color = m_color;
                break;
            }
        }
    }

}