﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System.Linq;

namespace Keyveo.FrontEnd {

    /// <summary>
    /// Component that marks the GameObject and his children to be treated by the FrontEndManager.
    /// </summary>
    public class FrontEndItem : MonoBehaviour {

        public TagType m_tagType;

        [ShowIf("m_tagType", TagType.UI)]
        public UITag m_uiTag;
        [ShowIf("m_tagType", TagType.Text)]
        public TextTag m_textTag;
        [ShowIf("m_tagType", TagType.Material)]
        public List<MaterialTag> m_materialTags;

        [HideInInspector]
        public string m_tag;

        public bool isNOZ = false;

        #region MonoBehaviour

        private void Awake() {
            switch(m_tagType) {
                case TagType.UI:
                m_tag = m_uiTag.ToString();
                break;
                case TagType.Text:
                m_tag = m_textTag.ToString();
                break;
            }
        }

        private void Start() {
            FrontEndManager.ApplySkinTo(transform);
        } 

        #endregion


        /// <summary>
        /// Search if a given tag was given to this FrontEndItem.
        /// </summary>
        /// <param name="filter">The tag to search for.</param>
        /// <returns>True if this FrontEndItem contains the given filter.</returns>
        public bool Filter(string filter) {
            if(m_tagType == TagType.Material) {
                return m_materialTags.Select(x => x.ToString()).Contains(filter);
            } else {
                return m_tag == filter;
            }
        }

    }

}