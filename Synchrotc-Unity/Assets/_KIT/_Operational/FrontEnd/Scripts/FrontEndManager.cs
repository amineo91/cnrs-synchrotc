﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Keyveo.Utilities;

namespace Keyveo.FrontEnd {

    public enum TagType {
        Material,
        UI,
        Text
    }

    public enum MaterialTag {
        Albedo,
        Emission,
        ColorTint
    }

    public enum UITag {
        Background,
        Foreground,
        Whiteground,
        Outline,
        Confirmation,
        Refusal,
        Accent,
        Shiny
    }

    public enum TextTag {
        MainText,
        BlackText,
        WhiteText,
        Shiny
    }


    /// <summary>
    /// Manager handling color chart, based on a FrontEndSkin.
    /// </summary>
    public class FrontEndManager : Singleton<FrontEndManager> {

        public FrontEndSkin m_skin = null;

        #region MonoBehaviour

        private void Start() {
            ApplyToScene();
        }

        #endregion

        /// <summary>
        /// Applies the selected FrontEndSkin to the active scene.
        /// </summary>
        [Button]
        public void ApplyToScene() {
            foreach (GameObject go in UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects()) {
                Apply(go.transform);
            }
        }

        /// <summary>
        /// Applies the selected FrontEndSkin to the given Transform.
        /// </summary>
        /// <param name="parent"></param>
        public static void ApplySkinTo(Transform parent) {
            if(Instance == null) {
                Debug.LogWarning("[FRONT END] The FrontEndManager was not found.");
                return;
            }

            Instance.m_skin.Apply(parent);
        }

        private void Apply(Transform parent) {
            m_skin.Apply(parent);
        }
    }

}