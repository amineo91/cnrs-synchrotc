﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

/// <summary>
/// Make a gameobject Pulse with Emission, using DOTween.
/// </summary>
public class PulseEffect : MonoBehaviour {

    [SerializeField]
    private bool PulseAtStart = false;
    [SerializeField]
    private Vector2 m_EmissionBounds = Vector2.up;

    [SerializeField]
    private float m_RiseDuration = 1f;
    [SerializeField]
    private float m_DecreaseDuration = 1f;

    [SerializeField]
    private Ease m_RiseEase = Ease.InOutSine;
    [SerializeField]
    private Ease m_DecreaseEase = Ease.InOutSine;

    private Material mat;

    private Color matColor;

    private float m_EmissionForce;

    private Tweener tweener;

    [SerializeField]
    private bool m_UseAlbedo = false;

    [SerializeField]
    private Color m_ForceColor = Color.black;

	// Use this for initialization
	void Start () {

        MeshRenderer mr = GetComponent<MeshRenderer>();
        if(mr != null)
        {
            mat = mr.material;
        } else
        {
            Image im = GetComponent<Image>();
            if(im != null)
            {
                mat = im.material;
            } else
            {
                Destroy(gameObject);
                return;
            }
        }
        
        matColor = m_ForceColor != Color.black ? m_ForceColor : mat.GetColor(m_UseAlbedo ? "_Color" : "_EmissionColor");
        m_EmissionForce = m_EmissionBounds.x;

        if (PulseAtStart)
        {
            Pulse();
        }
	}
	
	// Update is called once per frame
	void Update () {
        mat.SetColor(m_UseAlbedo ? "_Color" : "_EmissionColor", matColor * m_EmissionForce);
	}

    public void Pulse()
    {
        if (tweener != null)
            tweener.Kill();

        tweener = DOTween.To(() => m_EmissionForce, x => m_EmissionForce = x, m_EmissionBounds.y, m_RiseDuration).SetEase(m_RiseEase).OnComplete(delegate
        {
            tweener = DOTween.To(() => m_EmissionForce, x => m_EmissionForce = x, m_EmissionBounds.x, m_DecreaseDuration).SetEase(m_DecreaseEase);
        });
    }
}
