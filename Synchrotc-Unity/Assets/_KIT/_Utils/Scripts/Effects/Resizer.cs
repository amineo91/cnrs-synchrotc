﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Sirenix.OdinInspector;

namespace Keyveo
{
    public enum InitialSizeState
    {
        Shrink,
        Grow,
        Unmodified
    }
    /// <summary>
    /// Component used to resize a gameObject, using DOTween.
    /// </summary>
    public class Resizer : MonoBehaviour
    {

        [HideInInspector]
        public bool isMaxed = true;

        [FoldoutGroup("Parameters")]
        public float m_Duration = 1f;
        [SerializeField, FoldoutGroup("Parameters")]
        public Ease m_EaseGrow = Ease.OutSine;
        [SerializeField, FoldoutGroup("Parameters")]
        private Ease m_EaseShrink = Ease.InSine;

        private Tweener tweener;

        [SerializeField, FoldoutGroup("Parameters")]
        private Vector3 m_MinSize = Vector3.zero;
        [SerializeField, FoldoutGroup("Parameters")]
        private Vector3 m_MaxSize = Vector3.one;

        [SerializeField, FoldoutGroup("Parameters")]
        private InitialSizeState SizeStateAtStart = InitialSizeState.Unmodified;
        [SerializeField, FoldoutGroup("Parameters")]
        private bool PlayAtStart = false;
        [SerializeField, FoldoutGroup("Parameters")]
        private float DelayAtStart = 0.0f;

        [SerializeField, FoldoutGroup("Parameters")]
        private bool m_IsMaxSizeReinitAtAwake = true;

        private void Start()
        {
            if (m_IsMaxSizeReinitAtAwake)
            {
                m_MaxSize = transform.localScale;
            }
            switch (SizeStateAtStart)
            {
                case InitialSizeState.Grow:
                    Grow(0, 0);
                    break;
                case InitialSizeState.Shrink:
                    Shrink(0, 0);
                    break;
            }

            if (PlayAtStart)
            {
                if (transform.localScale == m_MaxSize)
                {
                    Shrink(DelayAtStart);
                }
                else
                {
                    Grow(DelayAtStart);
                }
            }
        }

        public void SetMaxSize(Vector3 size)
        {
            m_MaxSize = size;
        }

        public Tweener Show(float delay = 0, float duration = -1)
        {
            return Grow(delay, duration);
        }

        public Tweener Grow(float delay = 0, float duration = -1)
        {
            if (tweener != null)
                tweener.Kill();

            tweener = transform.DOScale(m_MaxSize, duration == -1 ? m_Duration : duration).SetEase(m_EaseGrow).SetDelay(delay).OnComplete(() => isMaxed = true);

            return tweener;
        }

        public Tweener Hide(float delay = 0, float duration = -1)
        {
            return Shrink(delay, duration);
        }

        public Tweener Shrink(float delay = 0, float duration = -1)
        {
            if (tweener != null)
                tweener.Kill();
            
            tweener = transform.DOScale(m_MinSize, duration == -1 ? m_Duration : duration).SetEase(m_EaseShrink).SetDelay(delay).OnComplete(() => isMaxed = false);
            return tweener;
        }

        public void InstantHide()
        {
            InstantShrink();
        }

        public void InstantShrink()
        {
            if (tweener != null)
                tweener.Kill();

            transform.localScale = m_MinSize;
            isMaxed = false;
        }

        public void InstantShow()
        {
            InstantGrow();
        }

        public void InstantGrow()
        {
            if (tweener != null)
                tweener.Kill();

            transform.localScale = m_MaxSize;
            isMaxed = true;
        }

        public void ToPrincipal(float delay = 0)
        {
            Grow(delay);
        }

        public void ToSecondary(float delay = 0)
        {
            Shrink(delay);
        }

        public void ToPrincipalInstant()
        {
            InstantGrow();
        }

        public void ToSecondaryInstant()
        {
            InstantShrink();
        }

        [Button("Toggle", ButtonSizes.Small)]
        public void Toggle()
        {
            if (transform.localScale == m_MinSize)
            {
                ToPrincipalInstant();
            }
            else
            {
                ToSecondaryInstant();
            }
        }


        public void ChangeToTheOpposit(float DelayBeforeGrow = 0)
        {
            if (transform.localScale == m_MinSize)
            {
                Grow(DelayBeforeGrow);
            }
            else
            {
                Shrink();
            }
        }
    }
}