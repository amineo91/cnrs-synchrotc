﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

/// <summary>
/// Used to imitate the SteamVR teleport's Fade.
/// </summary>
public class ScreenFader : MonoBehaviour {

    private MeshRenderer m_Renderer;

    public static ScreenFader Instance;

    public float m_FadeSpeed = 0.1f;

    private Tweener fader;

	// Use this for initialization
	void Awake () {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
	}

    void Start()
    {
        m_Renderer = GetComponent<MeshRenderer>();

        m_Renderer.material.color = Color.black;

        FadeOut();
    }
	
	public void FadeIn()
    {
        if (fader != null)
            fader.Kill();
        
        Color col = m_Renderer.material.GetColor("_OutlineColor");

        fader = m_Renderer.material.DOColor(new Color(col.r, col.g, col.b, 1), "_OutlineColor", m_FadeSpeed);
    }

    public void FadeOut()
    {
        if (fader != null)
            fader.Kill();

        Color col = m_Renderer.material.GetColor("_OutlineColor");

        fader = m_Renderer.material.DOColor(new Color(col.r, col.g, col.b, 0), "_OutlineColor", m_FadeSpeed);
    }

    public void FadeIn(float duration)
    {
        if (fader != null)
            fader.Kill();


        Color col = m_Renderer.material.GetColor("_OutlineColor");

        fader = m_Renderer.material.DOColor(new Color(col.r, col.g, col.b, 1), "_OutlineColor", duration);
    }

    public void FadeOut(float duration)
    {
        if (fader != null)
            fader.Kill();

        Color col = m_Renderer.material.GetColor("_OutlineColor");

        fader = m_Renderer.material.DOColor(new Color(col.r, col.g, col.b, 0), "_OutlineColor", duration);
    }

    public void FadeInOut()
    {
        if (fader != null)
            fader.Kill();

        Color col = m_Renderer.material.GetColor("_OutlineColor");

        fader = m_Renderer.material.DOColor(new Color(col.r, col.g, col.b, 1), "_OutlineColor", m_FadeSpeed).OnComplete(delegate
        {
            FadeOut();
        });
    }
}
