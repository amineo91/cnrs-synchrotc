﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Keyveo.ExtensionMethods {

    public static class Vector3Extension {
        public static Vector2 xy(this Vector3 vector3) {
            return new Vector2(vector3.x, vector3.y);
        }
        public static Vector2 xz(this Vector3 vector3) {
            return new Vector2(vector3.x, vector3.z);
        }
        public static Vector2 yx(this Vector3 vector3) {
            return new Vector2(vector3.y, vector3.x);
        }
        public static Vector2 yz(this Vector3 vector3) {
            return new Vector2(vector3.y, vector3.z);
        }
        public static Vector2 zx(this Vector3 vector3) {
            return new Vector2(vector3.z, vector3.x);
        }
        public static Vector2 zy(this Vector3 vector3) {
            return new Vector2(vector3.z, vector3.y);
        }

        public static Vector3 Mean(this List<Vector3> vectors3) {
            Vector3 mean = Vector3.zero;
            foreach(Vector3 item in vectors3) {
                mean += item;
            }
            return mean / (float)vectors3.Count;
        }

    }

}