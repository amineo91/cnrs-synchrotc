﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Keyveo.ExtensionMethods {

    public static class DOTweenExtension {

        public static Tweener DOEmission(this Material material, Color color, float duration) {
            return DOTween.To(
                   () => material.GetColor("_EmissionColor"),
                   x => material.SetColor("_EmissionColor", x),
                   color,
                   duration
                   );
        }


        public static Tweener DOWrite(this TMPro.TMP_Text tmPro, string message, float duration) {
            return DOTween.To(
              () => tmPro.text,
              x => tmPro.text = x,
              message,
              duration
           );
        }
    }
}