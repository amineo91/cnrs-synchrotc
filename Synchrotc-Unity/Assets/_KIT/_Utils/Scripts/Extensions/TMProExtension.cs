﻿#if KEYVEO_LOCALIZATION
using Keyveo.Features.Localization;
#endif
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

namespace Keyveo.ExtensionMethods
{

    public static class TMProExtension
    {
#if KEYVEO_LOCALIZATION
        public static void SetLocalizedText(this TMP_Text text, LocalizedString localizedString)
        {
            if (LocalizationManager.Instance == null)
            {
                Debug.LogWarning("No Localizer Instance Found, please add it to your scene.");
                text.text = localizedString.m_values.Values.ToList()[0];
            }
            text.text = localizedString.m_values[LocalizationManager.Instance.m_currentLanguage];
        }
#endif

    }

}