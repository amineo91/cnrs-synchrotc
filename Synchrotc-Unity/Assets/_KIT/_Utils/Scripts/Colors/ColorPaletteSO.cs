﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

/// <summary>
/// ScriptableObject used to store a list Colors.
/// </summary>
[CreateAssetMenu(fileName = "NewPalette.asset", menuName = "Keyveo/ColorPalette"), CanEditMultipleObjects]
public class ColorPaletteSO : ScriptableObject {
    public List<Color32> palette = new List<Color32>();
}

#endif