﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Keyveo.Utilities.UI {

    /// <summary>
    /// Hoverable UI Component, using a FaderBase upon hovering start/end.
    /// </summary>
    public class HoverEvent : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {
        [SerializeField]
        private UnityEvent m_OnEnter = null;

        [SerializeField]
        private UnityEvent m_OnExit = null;

        [SerializeField]
        private bool m_AutoLink = true;

        private void Start() {
            if(m_AutoLink) {
                foreach(FaderBase fb in GetComponents<FaderBase>()) {
                    m_OnEnter.AddListener(() => fb.Show());
                    m_OnExit.AddListener(() => fb.Hide());
                }
            }
        }

        public void OnPointerEnter(PointerEventData eventData) {
            m_OnEnter.Invoke();
        }

        public void OnPointerExit(PointerEventData eventData) {
            m_OnExit.Invoke();
        }
    }

}