﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

namespace Keyveo.Utilities.UI {

    /// <summary>
    /// UI Outline size fader.
    /// </summary>
    public class OutlineFader : FaderBase {
        private Outline outline = null;

        [SerializeField]
        private Vector2 m_OffsetDisplayed = Vector2.one;

        private Tweener tweener = null;

        // Start is called before the first frame update
        void Start() {
            outline = GetComponent<Outline>();
            outline.effectDistance = Vector2.zero;
        }

        public override void Show() {
            if(tweener != null)
                tweener.Kill();

            tweener = DOTween.To(() => outline.effectDistance, x => outline.effectDistance = x, m_OffsetDisplayed, m_Duration).SetEase(Ease.OutBack);
        }

        public override void Hide() {
            if(tweener != null)
                tweener.Kill();

            tweener = DOTween.To(() => outline.effectDistance, x => outline.effectDistance = x, Vector2.zero, m_Duration).SetEase(Ease.InOutCirc);
        }
    }

}