﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Keyveo.Utilities.UI {

    /// <summary>
    /// UI Graphic alpha fader.
    /// </summary>
    public class GraphicFader : FaderBase {
        private Graphic graphic;

        // Start is called before the first frame update
        void Start() {
            graphic = GetComponent<Graphic>();
            graphic.DOFade(0f, 0f);
        }

        public override void Show() {
            graphic.DOKill();

            graphic.DOFade(1f, m_Duration).SetEase(Ease.OutBack);
        }

        public override void Hide() {
            graphic.DOKill();

            graphic.DOFade(0f, m_Duration).SetEase(Ease.InOutCirc);
        }
    }
}