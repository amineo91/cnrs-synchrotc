﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Keyveo.Utilities.UI {

    /// <summary>
    /// Upon clicking this button, offset horizontally its transform to hide/show it.
    /// </summary>
    public class HideButton : MonoBehaviour {
        private RectTransform m_rectTransform;

        [SerializeField]
        private float m_offset = 0f;

        [SerializeField]
        private float m_duration = 0.75f;

        private bool m_isOut = true;

        private void Awake() {
            m_rectTransform = GetComponent<RectTransform>();
        }

        public void Toggle(Transform tr) {
            m_rectTransform.DOKill();

            m_isOut = !m_isOut;

            m_rectTransform.DOAnchorPosX(m_isOut ? 0 : m_offset, m_duration).SetEase(Ease.OutCirc);

            tr.DOScaleX(m_isOut ? 1 : -1, m_duration).SetEase(Ease.InOutCirc);
        }

    }

}