﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Keyveo.Utilities.UI {

    /// <summary>
    /// Abstract class serving as base for a fading behaviour.
    /// </summary>
    public abstract class FaderBase : MonoBehaviour {
        [SerializeField]
        protected float m_Duration;

        public abstract void Show();

        public abstract void Hide();
    }

}