<?php
// This teh router of the application.
// Depending on the "action", it asks the corresponding controller
require_once "models/User.php";

session_start();

require_once "controllers/SessionController.php";
require_once "controllers/UserController.php";
require_once "controllers/ScenarioController.php";
require_once "controllers/DayController.php";

if(isset($_GET["action"]) && ($_GET["action"] === "login" || isset($_SESSION["user"]) ) ) {
    $action = $_GET["action"];

    switch($action) {
        // SESSION
        case "home":
            $_SESSION["page"] = "Accueil";
            SessionController::home();
            break;
        case "login":
            ( isset($_POST["login"]) && isset($_POST["password"]) ) or die();
            $_SESSION["page"] = "Accueil";
            SessionController::login($_POST["login"], $_POST["password"]);
            break;
        case "logout":
            SessionController::logout();
            break;
        
        // USER
        case "userList":
            $_SESSION["page"] = "Utilisateurs";
            UserController::listing();
            break;
        case "userAdd":
            ( isset($_POST["login"]) && isset($_POST["password"]) && isset($_POST["role"]) ) or die();
            UserController::add($_POST["login"], $_POST["password"], $_POST["role"]);
            break;
        case "userDelete":
            ( isset($_GET["id"]) ) or die();
            UserController::delete($_GET["id"]);
            break;
        case "userDetail":
            $_SESSION["page"] = "Fiche utilisateur";
            ( isset($_GET["id"]) ) or die();
            UserController::detail($_GET["id"]);
            break;
        case "userAddDetail":
            ( isset($_POST["patient_id"]) && isset($_POST["age"]) && isset($_POST["trauma"]) ) or die();
            UserController::addDetail($_POST["patient_id"], $_POST["age"], $_POST["trauma"]);
            break;
        
        // SCENARIO
        /*case "scenarioList":
            $_SESSION["page"] = "Scénarios";
            ScenarioController::listing();
            break;
        case "scenarioAdd":
            ( isset($_POST["id_place"]) && isset($_POST["name"]) && isset($_POST["description"]) && isset($_POST["difficulty"]) && isset($_POST["completion_time"]) ) or die();
            ScenarioController::add($_POST["id_place"], $_POST["name"], $_POST["description"], $_POST["difficulty"], $_POST["completion_time"]);
            break;
        case "scenarioDetail":
            ( isset($_GET["id"]) ) or die();
            ScenarioController::detail($_GET["id"]);
            break;*/
            
        // DAY
        case "dayAdd":
            ( isset($_POST["patient_id"]) && isset($_POST["date"]) ) or die();
            DayController::add($_POST["patient_id"], $_POST["date"]);
            break;
        case "dayDetail":
            $_SESSION["page"] = "Journée";
            ( isset($_GET["patient_id"]) && isset($_GET["id"]) ) or die();
            DayController::detail($_GET["id"], $_GET["patient_id"]);
            break;
        case "dayDelete":
            ( isset($_GET["id"]) && isset($_GET["patient_id"]) ) or die();
            DayController::delete($_GET["id"], $_GET["patient_id"]);
            break;
        case "dayAddScenario":
            ( isset($_POST["day_id"]) && isset($_POST["scenario_id"]) && isset($_POST["order"]) ) or die();
            DayController::addScenario($_POST["day_id"], $_POST["scenario_id"], $_POST["order"]);
            break;
        case "dayDeleteScenario":
            ( isset($_GET["day_id"]) && isset($_GET["scenario_id"])) or die();
            DayController::deleteScenario($_GET["day_id"], $_GET["scenario_id"]);
            break;
            
        default:
            throw new Exception("Invalid action");
        }
} else {
    SessionController::home();
}
?>