<?php // param needed $day $patient $scenarioAndOrders $scenarios $places ?>
<?php require_once "views/header.php" ?>



<h2>Détails de la journée</h2>
<ul class="list-group list-group-flush list-group-flush-noexternalborders">
    <?php if($currentUser->role === "admin") {?>
    <li class="list-group-item list-group-item-noexternalborders">Id : <?= $day->id ?></li>
    <li class="list-group-item list-group-item-noexternalborders">Id de l'utilisateur : <?= $day->id_account ?></li>
    <?php } ?>
    <li class="list-group-item list-group-item-noexternalborders">Journée : <?= $day->day ?></li>
    <li class="list-group-item list-group-item-noexternalborders">Début : <?= $day->begin_time ?></li>
    <li class="list-group-item list-group-item-noexternalborders">Fin : <?= $day->end_time ?></li>
    <?php if($day->fail_reason != null) {?>
    <li class="list-group-item list-group-item-noexternalborders">Raison de l'échec : <?= $day->fail_reason ?></li>
    <?php } ?>
    <li class="list-group-item list-group-item-noexternalborders">Parcours du patient : <a href=<?= $day->parcour_player ?>  onclick="open('<?= $day->parcour_player ?>', 'Popup', 'scrollbars=1,resizable=1,height=560,width=770'); return false;" >Cliquez ici</a></li>
</ul>

<h2>Tâches</h2>

<div class="pre-scrollable" style="max-height: 260px;">
<table class="table table-striped table-borderless">
    <thead>
        <tr>
            <?php if($currentUser->role === "admin") { ?>
            <th>Id</th>
            <th>Identifiant mission account</th>
            <?php } ?>
            <th>Début</th>
            <th>Fin</th>
            <th>NB erreurs</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($tasks as $task): ?>
        
        <tr>
        <?php if($currentUser->role === "admin") {?>
        <td><?= $task->id ?></td>
        <td><?= $task->id_mission_account ?></td>
        <?php } ?>
        <td><?= $task->begin_time ?></td>
        <td><?= $task->end_time ?></td>
        <td><?= $task->nb_error ?></td>
        <!--<td>
            <a href="?action=dayDetail&patient_id=<?= $day->id_account ?>&id=<?= $day->id ?>">Détails</a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <td>-->
    </tr>
<?php endforeach; ?>
    </tbody>
</table>
</div>

<?php require_once "views/footer.php"; ?>