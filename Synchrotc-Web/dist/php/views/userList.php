<?php // param needed $users, $currentUser ?>
<?php require_once "views/header.php"; ?>
    
<!--<h1>Utilisateurs</h1>-->
<h2>Liste d'utilisateurs</h2>
<?php if(count($users) == 0){ ?>
    <p>Aucun utilisateur</p>
<?php } else { ?>
<div class="pre-scrollable">
<table class="table table-striped table-borderless">
    <thead>
        <tr>
            <?php if($currentUser->role === "admin") {?>
            <th>N° Utilisateur</th>
            <?php } ?>
            <th>Identifiant</th>
            <?php if($currentUser->role === "admin") {?>
            <th>Mot de passe</th>
            <th>Date de création</th>
            <th>Dernière connexion</th>
            <th>Rôle</th>
            <th>N° Créateur</th>
            <?php } ?>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
<?php foreach($users as $user): ?>
    <tr>
        <?php if($currentUser->role === "admin") {?>
        <td><?= $user->id ?></td>
        <?php } ?>
        <td><?= $user->login ?></td>
        <?php if($currentUser->role === "admin") {?>
        <td><?= $user->password ?></td>
        <td><?= $user->creation_date ?></td>
        <td><?= $user->last_connection ?></td>
        <td><?= $user->role ?></td>
        <td><?= $user->creator_id ?></td>
        <?php } ?>
        <td>
            <a href="?action=userDetail&id=<?= $user->id ?>">Détails</a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="?action=userDelete&id=<?= $user->id ?>">Supprimer</a> <!-- TODO - Confirm action popup -->
        </td>
    </tr>
<?php endforeach; ?>
    </tbody>
</table>
</div>
<?php } ?>

<div class="subsection">
    <h4>Ajouter un utilisateur</h4>
    <form id="form" action="?action=userAdd" method="POST" class="form">

    <div class="form-group">
        <input class="form-control" type="text" name="login" placeholder="login" required readonly value="<?= uniqid() ?>">
    </div>

    <div class="form-group">
        <input class="form-control" type="text" name="password" placeholder="mot de passe" required>
    </div>

    <?php if($currentUser->role === "admin") {?>

        <div class="form-group">
            <select name="role" class="form-control" required>
                <option value="">-- rôle --</option>
                <option value="admin">admin</option>
                <option value="doctor">docteur</option>
                <option value="patient">utilisateur</option>
            </select>
        </div>

    <?php } else { ?>
        <input type="text" name="role" value="patient" required readonly hidden>
    <?php } ?>

        <input class="btn btn-primary btn-lg btn-block" type="submit" value="Ajouter">
    </form>
</div>
  
<?php require_once "views/footer.php"; ?>