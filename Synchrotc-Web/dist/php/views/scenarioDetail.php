<?php // param needed $scenario $place $itemsAndAction ?>
<?php require_once "views/header.php" ?>

<h1>Détails du scénario</h1>
<ul class="list-group list-group-flush">
    <?php if($currentUser->role === "admin") {?>
    <li class="list-group-item">id : <?= $scenario->id ?></li>
    <li class="list-group-item">id_place : <?= $scenario->id_place ?></li>
    <?php } ?>
    <li class="list-group-item">place : <?= $place->name ?></li>
    <li class="list-group-item">name : <?= $scenario->name ?></li>
    <li class="list-group-item">description : <?= $scenario->description ?></li>
    <li class="list-group-item">difficulty : <?= $scenario->difficulty ?></li>
    <!-- TODO - completion_time ? -->
</ul>


<h2>Item list</h2>

<table class="table table-striped">
    <thead>
        <tr>
            <th>id</th><th>id_place</th><th>name</th><th>action</th>
        </tr>
    </thead>
    <tbody>
<?php foreach($itemsAndAction as $itemAndAction): ?>
    <tr>
        <td><?= $itemAndAction->id ?></td>
        <td><?= $itemAndAction->id_place ?></td> <!-- TODO - Show place name instead of id -->
        <td><?= $itemAndAction->name ?></td>
        <td><?= $itemAndAction->action ?></td>
    </tr>
<?php endforeach; ?>
    </tbody>
</table>

<?php require_once "views/footer.php"; ?>