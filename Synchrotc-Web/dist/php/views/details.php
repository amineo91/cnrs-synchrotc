<?php // param needed $users, $currentUser ?>
<?php require_once "views/header.php"; ?>

<h2>Informations de l'utilisateur <?= $user->login ?></h2>

<br>
<form action="?action=userAddDetail" method="post">
    <input type="hidden" name="patient_id" value="<?= $user->id ?>">
    <p>Sélectionner l'âge de l'utilisateur :</p>
    <div class="form-check">
        <input class="form-check-input" type="radio" name="age" value="from 10 to 20 years old" id="ageRange10">
        <label class="form-check-label" for="ageRange10">de 10 à 20 ans</label>
    </div>

    <div class="form-check">
        <input class="form-check-input" type="radio" name="age" value="from 21 to 30 years old" id="ageRange21" checked>
        <label class="form-check-label" for="ageRange21">de 21 à 30 ans</label>
    </div>

    <div class="form-check">
        <input class="form-check-input" type="radio" name="age" value="from 31 to 40 years old" id="ageRange31">
        <label class="form-check-label" for="ageRange31">de 31 à 40 ans</label>
    </div>

    <div class="form-check">
        <input class="form-check-input" type="radio" name="age" value="from 41 to 50 years old" id="ageRange41">
        <label class="form-check-label" for="ageRange41">de 41 à 50 ans</label>
    </div>

    <div class="form-check">
        <input class="form-check-input" type="radio" name="age" value="from 51 to 60 years old" id="ageRange51">
        <label class="form-check-label" for="ageRange51">de 51 à 60 ans</label>
    </div>

    <div class="form-check">
        <input class="form-check-input" type="radio" name="age" value="from 61 to 70 years old" id="ageRange61">
        <label class="form-check-label" for="ageRange61">de 61 à 70 ans</label>
    </div>

    <div class="form-check">
        <input class="form-check-input" type="radio" name="age" value="more than 71 years old" id="ageRange71">
        <label class="form-check-label" for="ageRange71">plus de 71 ans</label>
    </div>

    <br>
    <p>Sélectionner le niveau de trauma de l'utilisateur :</p>
    <div class="form-check">
        <input class="form-check-input" type="radio" name="trauma" value="low" id="traumaLevelLow" checked>
        <label class="form-check-label" for="traumaLevelLow">léger</label>
    </div>

    <div class="form-check">
        <input class="form-check-input" type="radio" name="trauma" value="medium" id="traumaLevelMedium">
        <label class="form-check-label" for="traumaLevelMedium">moyen</label>
    </div>

    <div class="form-check">
        <input class="form-check-input" type="radio" name="trauma" value="high" id="traumaLevelHigh">
        <label class="form-check-label" for="traumaLevelHigh">sévère</label>
    </div>

    <!--<br>
    <p>Sélectionner la difficulté des scénarios pour cet utilisateur :</p>
    <div class="form-check">
        <input class="form-check-input" type="radio" name="difficulty" value="easy" id="difficultyEasy" checked>
        <label class="form-check-label" for="difficultyEasy">facile</label>
    </div>

    <div class="form-check">
        <input class="form-check-input" type="radio" name="difficulty" value="medium" id="difficultyMedium">
        <label class="form-check-label" for="difficultyMedium">moyen</label>
    </div>

    <div class="form-check">
        <input class="form-check-input" type="radio" name="difficulty" value="hard" id="difficultyHard">
        <label class="form-check-label" for="difficultyHard">difficile</label>
    </div>-->

    <br>
    <input type="submit" class="btn btn-primary" value="Valider">
</form>

<?php require_once "views/footer.php"; ?>