<?php // param needed $user $days $messages $detail ?>
<?php require_once "views/header.php"?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>
    function myAjax(id) {
      $.ajax({
           type: "POST",
           url: 'GetStatForCSV.php',
           data:{id_patient:id},
           success:function(html) {
             alert(html);
           }
      });
    }
</script>

<h2>Détails de l'utilisateur</h2>
<ul class="list-group list-group-flush list-group-flush-noexternalborders">
    <?php if($currentUser->role === "admin") {?>
    <li class="list-group-item list-group-item-noexternalborders">Id : <?= $user->id ?></li>
    <?php } ?>
    <li class="list-group-item list-group-item-noexternalborders">Identifiant : <?= $user->login ?></li>
    <?php if($currentUser->role === "admin") {?>
    <li class="list-group-item list-group-item-noexternalborders">Mot de passe : <?= $user->password ?></li>
    <?php } ?>
    <li class="list-group-item list-group-item-noexternalborders">Date de création : <?= $user->creation_date ?></li>
    <li class="list-group-item list-group-item-noexternalborders">Dernière connexion : <?= $user->last_connection ?></li>
    <?php if($currentUser->role === "admin") {?>
    <li class="list-group-item list-group-item-noexternalborders">Rôle : <?= $user->role ?></li>
    <li class="list-group-item list-group-item-noexternalborders">Id du créateur : <?= $user->creator_id ?></li>
    <?php } ?>
    <?php if(isset($detail) && $detail !== null) {?>
    <?php 
    // Translatations
    $age = $detail->age;
    $age = str_replace("from", "de", $age);
    $age = str_replace("to", "à", $age);
    $age = str_replace("years old", "ans", $age);
    $age = str_replace("more than", "plus de", $age);
    ?>
    <li class="list-group-item list-group-item-noexternalborders">Âge : <?= $age ?></li>
    <?php 
    // Translatations
    $trauma = $detail->trauma;
    $trauma = str_replace("low", "léger", $trauma);
    $trauma = str_replace("medium", "moyen", $trauma);
    $trauma = str_replace("high", "fort", $trauma);
    ?>
    <li class="list-group-item list-group-item-noexternalborders">Trauma : <?= $trauma ?></li>
    <?php 
    // Translatations
    $difficulty = $detail->difficulty;
    $difficulty = str_replace("easy", "facile", $difficulty);
    $difficulty = str_replace("medium", "moyen", $difficulty);
    $difficulty = str_replace("hard", "difficile", $difficulty);
    ?>
    <!--<li class="list-group-item list-group-item-noexternalborders">Difficulté des scénarios : <?= $difficulty ?></li>-->
    <?php } ?>
</ul>
    </br>
    <form action="requests/GetStatMissionsForCSV.php" method="post">
        <input type="text" name="id_patient" value="<?= $user->id ?>" style="opacity:0;">
        Exporter CSV Missions : <button  type="submit">Télécharger</button>
    </form>
    </br>
    <form action="requests/GetStatTacheForCSV.php" method="post">
        <input type="text" name="id_patient" value="<?= $user->id ?>" style="opacity:0;">
        Exporter CSV Tâche : <button  type="submit">Télécharger</button>
    </form>
    </br>
    <form action="requests/GetStatPositionsForCSV.php" method="post">
        <input type="text" name="id_patient" value="<?= $user->id ?>" style="opacity:0;">
        Exporter CSV Positions : <button  type="submit">Télécharger</button>
    </form>
<br>
<h2>Missions</h2>
<?php if(count($days) == 0){ ?>
    <p>Aucune missions</p>
<?php } else { ?>
<div class="pre-scrollable" style="max-height: 260px;">
<table class="table table-striped table-borderless">
    <thead>
        <tr>
            <?php if($currentUser->role === "admin") {?>
            <th>Id</th>
            <th>Identifiant de l'utilisateur</th>
            <?php } ?>
            <th>Mission</th>
            <th>Date de l'essai</th>
        </tr>
    </thead>
    <tbody>
<?php foreach($days as $day): ?>
    <tr>
        <?php if($currentUser->role === "admin") {?>
        <td><?= $day->id ?></td>
        <td><?= $day->id_account ?></td>
        <?php } ?>
        <td><?= $day->day ?></td>
        <td><?= $day->begin_time ?></td>
        <td>
            <a href="?action=dayDetail&patient_id=<?= $day->id_account ?>&id=<?= $day->id ?>">Détails</a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <!--<a href="?action=dayDelete&patient_id=<?= $user->id ?>&id=<?= $day->id ?>">Supprimer</a> -->
        <td>
    </tr>
<?php endforeach; ?>
    </tbody>
</table>
</div>
<?php } ?> 

<!--<h2>Journées</h2>
<?php if(count($days) == 0){ ?>
    <p>Aucune journée</p>
<?php } else { ?>
<div class="pre-scrollable" style="max-height: 260px;">
<table class="table table-striped table-borderless">
    <thead>
        <tr>
            <?php if($currentUser->role === "admin") {?>
            <th>Id</th>
            <th>Identifiant de l'utilisateur</th>
            <?php } ?>
            <th>Date</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
<?php foreach($days as $day): ?>
    <tr>
        <?php if($currentUser->role === "admin") {?>
        <td><?= $day->id ?></td>
        <td><?= $day->patient_id ?></td>
        <?php } ?>
        <td><?= $day->date ?></td>
        <td>
            <a href="?action=dayDetail&patient_id=<?= $user->id ?>&id=<?= $day->id ?>">Détails</a>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="?action=dayDelete&patient_id=<?= $user->id ?>&id=<?= $day->id ?>">Supprimer</a> 
        <td>
    </tr>
<?php endforeach; ?>
    </tbody>
</table>
</div>
<?php } ?> 

<div class="subsection">
    <h4>Ajouter une journée</h4>
    <form id="form" action="?action=dayAdd" method="POST" class="form">
        <div class="form-group">
            <input class="form-control" type="hidden" name="patient_id" value="<?= $user->id ?>">
        </div>
        <div class="form-group">
            <input class="form-control" type="date" name="date" placeholder="YYYY-MM-DD" required>
        </div>
        <input class="btn btn-primary btn-lg btn-block" type="submit" value="Ajouter">
    </form>
</div>

<br>
<h2>Messages</h2>
<?php if(count($messages) == 0){ ?>
    <p>Aucun message</p>
<?php } else { ?>
<table class="table table-striped">
    <thead>
        <tr>
            <th>id</th><th>patient_id</th><th>difficulty</th><th>date</th><th>action</th>
        </tr>
    </thead>
    <tbody>
<?php foreach($messages as $message): ?>
    <tr>
        <td><?= $message->id ?></td>
        <td><?= $message->patient_id ?></td>
        <td><?= $message->file ?></td>
        <td><?= $message->date ?></td>
        <td>
            <a href="<?= $message->file ?>" download>download</a>
        <td>
    </tr>
<?php endforeach; ?>
    </tbody>
</table>
<?php } ?>
-->
<?php require_once "views/footer.php"; ?>