<?php // param needed $scenarios $currentUser $places ?>
<?php require_once "views/header.php"; ?>
    
<h1>Scénarios</h1>
<h2>Liste</h2>

<table class="table table-striped">
    <thead>
        <tr>
        <?php if($currentUser->role === "admin") {?>
        <th>id</th><th>id_place</th>
        <?php } ?>
        <th>name</th><th>description</th><th>place</th><th>difficulty</th>
        <!-- TODO - completion_time ? -->
        </tr>
    </thead>
    <tbody>
<?php foreach($scenarios as $scenario): ?>
    <tr>
        <?php if($currentUser->role === "admin") {?>
        <td><?= $scenario->id ?></td>
        <td><?= $scenario->id_place ?></td>
        <?php } ?>
        <td><?= $scenario->name ?></td>
        <td><?= $scenario->description ?></td>
        
<?php
$place = null;
foreach($places as $forPlace):
    if ($forPlace->id == $scenario->id_place) {
        $place = $forPlace;
    }
endforeach;
?>
        <td><?= $place->name ?></td>
        <td><?= $scenario->difficulty ?></td>
        <!-- TODO - completion_time ? -->
        <td>
            <a href="?action=scenarioDetail&id=<?= $scenario->id ?>">detail</a>
        <td>
    </tr>
<?php endforeach; ?>
    </tbody>
</table>

<?php
if($currentUser->role === "admin") {
?>

<h2>Ajouter un scénario</h2>

    <form id="form" action="?action=scenarioAdd" method="POST" class="form" style="text-align: center;">
    <select class="form-control" name="id_place" required>
        <option value="">-- place --</option>;
<?php foreach($places as $place): ?>
        <option value="<?= $place->id ?>"><?= $place->name ?></option>;
<?php endforeach; ?>
    </select>
    <input type="text" name="name" placeholder="name" class="form-control" required>
    <textarea type="text" name="description" placeholder="description" class="form-control" required></textarea>
    <input type="text" name="difficulty" placeholder="easy" class="form-control" required>
    <input type="text" name="completion_time" placeholder="completion_time HH:MM:SS" class="form-control" required>
    <input type="submit" value="Ajouter" class="btn btn-primary btn-lg btn-block">
    </form>
<?php
}
?>
  
<?php require_once "views/footer.php"; ?>