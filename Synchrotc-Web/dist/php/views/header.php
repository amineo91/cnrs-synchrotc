<!DOCTYPE html>
<html lang="fr">

<head>
  <title>SYNCHRO TC</title>
  <meta charset="UTF-8">
  <!--Favicon-->
  <link rel="icon" href="../img/icon.png" type="image/png" />
  <!--CSS-->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="./views/css/main.css">
</head>

<body touch-action="none">
<!-- Disable native touch actions like zoom or scroll on web browser -->


<?php
if( isset($_SESSION["user"])){
    echo '<div class="wellcomeMessage">';
    $role = $_SESSION["user"]->role;
    if($role === "doctor") {
      $role = "docteur";
    }
    echo 'Bonjour,<br><span style="text-transform: uppercase;">'.$role.'</span>';
    echo '</div>';
    echo '<div id="topCorner" class="topCorner" >';
    echo ''.$_SESSION["page"].'';
    echo '</div>';
    echo '<div class="bottomCorner">';
    echo 'Application<br>SynchroTC';
    echo '</div>';
}

if( isset($_SESSION["user"]) ) {
  echo '<div id="verticalMenu" class="vertical-menu">';
  echo '<a href="?action=home"><img class="icone" src="../img/picto_home.png">Accueil</a>';
  echo '<a href="?action=userList"><img class="icone" src="../img/picto_user.png">Utilisateurs</a>';
  
  /*if($_SESSION["user"]->role === "admin") {
    echo '<a href="?action=scenarioList"><img class="icone" src="../img/picto_scenar.png">Scénarios</a>';
  } */
  echo '<a href="?action=logout"><img class="icone" src="../img/picto_exit.png">Déconnexion</a>';
  echo '</div>';
}

if( isset($_SESSION["user"]) ) {
  echo '<div class="content">';
}


?>

