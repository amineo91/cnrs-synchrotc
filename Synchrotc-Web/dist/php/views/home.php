<?php require "views/header.php"; ?>

<div class="jumbotron jumbotron-fluid text-center" style="border-bottom:4px solid #007BFF">
  <div class="container">
    <h1 class="display-4">Application SynchroTC</h1>
    <p class="lead">Bienvenue dans l'application SynchroTC, veuillez vous connecter pour continuer</p>
  </div>
</div>

<br><br>

<form action="?action=login" method="POST">
  <div class="form-group text-center">
    <label for="login">
      <input type="text" name="login" class="form-control" id="login" style="width: 20em;" aria-describedby="loginHelp" placeholder="Identifiant">
    </label>
  </div>
  <div class="form-group text-center">
    <label for="password">
      <input type="password" class="form-control" name="password" id="password" style="width: 20em;" placeholder="Mot de passe">
    </label>
  </div>
  <div class="text-center">
    <button type="submit" class="btn btn-primary">Connexion</button>
  </div>
</form>

  <!--Index-->
  <!--Libraries-->
  <!--Project-->

<?php require "views/footer.php"; ?>