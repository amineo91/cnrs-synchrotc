<?php
class User {
    public $id, $login, $password, $creation_date, $last_connection, $role, $creator_id;

    public function __construct($id, $login, $password, $creation_date, $last_connection, $role, $creator_id) {
        $this->id = $id;
        $this->login = $login;
        $this->password = $password;
        $this->creation_date = $creation_date;
        $this->last_connection = $last_connection;
        $this->role = $role;
        $this->creator_id = $creator_id;
    }
}
?>