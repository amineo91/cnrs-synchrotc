<?php
require_once "models/Day.php";
require_once "models/ConnectionDB.php";
require_once "models/Utils.php";

class DayManager {
    public $mysqli;

    public function __construct() {
        $connection = new ConnectionDB();
        $this->mysqli = $connection->mysqli;
    }

    public function create($patient_id, $date) {
        // Perform SQL queries
        $stmt = $this->mysqli->prepare("INSERT INTO `days` (`patient_id`, `difficulty`, `date`) VALUES (?,?,?)");
        $difficulty = "easy";
        $stmt->bind_param("dss", $patient_id, $difficulty, $date); // TODO - Remove difficulty from Day
        return $stmt->execute();
    }

    public function read() {
        // TODO
        echo "DayManager.read";
        exit;
    }

    public function readAllWithPatientId($patient_id) {
        // Perform SQL queries
        $days = array();
        $stmt = null;
        $stmt = $this->mysqli->prepare("SELECT * FROM `rapport_day` WHERE `id_account` = ?");
        $stmt->bind_param("d", $patient_id);
        $stmt->execute();

        if($stmt) {
            // store the result in an array
            $result = get_result($stmt);
            while($row = array_shift($result)) {
                $day = new Day($row["id"], $row["id_account"], $row["day"], $row["begin_time"], $row["end_time"], $row["fail_reason"],$row["parcour_player"]);
                array_push($days, $day);
            }
        }

        return $days;
    }

    public function readWithId($patient_id, $id) {
         // Perform SQL queries
         $stmt = null;
         $stmt = $this->mysqli->prepare("SELECT * FROM `rapport_day` WHERE `id_account` = ?");
         $stmt->bind_param("d", $patient_id);
         $stmt->execute();
 
         if($stmt) {
             // store the result in an array
             $result = get_result($stmt);
             while($row = array_shift($result)) {
                if($row["id"] == $id){
                    $day = new Day($row["id"], $row["id_account"], $row["day"], $row["begin_time"], $row["end_time"], $row["fail_reason"],$row["parcour_player"]);
                break;
                }
             }
         }
 
         return $day;
    }

    public function update() {
        // TODO
        echo "DayManager.update";
        exit;
    }

    public function delete($id) {
        // Perform SQL queries
        $stmt = $this->mysqli->prepare("DELETE FROM `days` WHERE `days`.`id` = ?");
        $stmt->bind_param("i", $id);
        return $stmt->execute();
    }
}