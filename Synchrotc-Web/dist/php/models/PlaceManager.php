<?php
require_once "models/Place.php";
require_once "models/ConnectionDB.php";
require_once "models/Utils.php";

class PlaceManager {
    public $mysqli;

    public function __construct() {
        $connection = new ConnectionDB();
        $this->mysqli = $connection->mysqli;
    }

    public function read($id) {
        // Perform SQL queries
        $stmt = $this->mysqli->prepare("SELECT * FROM `places` WHERE `id` = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();

        $place = null;
        
        // Read result
        $result = get_result($stmt);
        if ($data = array_shift($result)) {
            $place = new Place($data["id"], $data["name"]);
        }

        return $place;
    }

    public function readAll() {
        // Perform SQL queries
        $places = array();
        $stmt = null;
        $stmt = $this->mysqli->prepare("SELECT * FROM `places`");
        $stmt->execute();

        if($stmt) {
            // store the result in an array
            $result = get_result($stmt);
            while($row = array_shift($result)) {
                $place = new Place($row["id"], $row["name"]);
                array_push($places, $place);
            }
        }

        return $places;
    }
}