<?php
class ConnectionDB {
    public $mysqli;
    protected $servername, $username, $password, $database;

    public function __construct() {
        
        // ----------------- //
        // --- IMPORTANT --- //
        // ----------------- //
        $isLocal = true;

        if ($isLocal) {
            $this->servername = "localhost";
            $this->username = "root";
            $this->password = "root";
            $this->database = "keyveocomasynctc";
        }
		
        else
		{
            $this->servername = "keyveocomasynctc.mysql.db";
            $this->username = "root";
            $this->password = "root";
            $this->database = "keyveocomasynctc";
        }


        $this->connect();
    }

    public function connect() 
	{	
		$mysql = new mysqli($this->servername, $this->username, $this->password, $this->database);
        
        if ($mysql->connect_error) 
		{
			die(utf8_encode($mysql->connect_error));
			
            print_r($mysql->connect_error);
            http_response_code(500);
            die();
        }
		
		//echo 'Connexion réussie';

        $this->mysqli = $mysql;
        $this->mysqli->set_charset("utf8");
    }

    public function __sleep() {
        return ["servername","username","password","database"];
    }

    public function __wakeup() {
        $this->connection();
    }
}
?>