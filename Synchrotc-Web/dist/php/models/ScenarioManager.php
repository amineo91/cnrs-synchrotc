<?php
require_once "models/Scenario.php";
require_once "models/ConnectionDB.php";
require_once "models/Utils.php";

class ScenarioManager {
    public $mysqli;

    public function __construct() {
        $connection = new ConnectionDB();
        $this->mysqli = $connection->mysqli;
    }

    public function create($id_place, $name, $description, $difficulty, $completion_time) {
        // Perform SQL queries
        $stmt = $this->mysqli->prepare("INSERT INTO `scenarios` (`id_place`, `name`, `description`, `difficulty`, `completion_time`) VALUES (?,?,?,?,?)");
        $stmt->bind_param("issss", $id_place, $name, $description, $difficulty, $completion_time);
        return $stmt->execute();
    }

    public function read($id) {
        // Perform SQL queries
        $stmt = $this->mysqli->prepare("SELECT * FROM `scenarios` WHERE `id` = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();

        $scenario = null;
        
        // Read result
        $result = get_result($stmt);
        if ($data = array_shift($result)) {
            $scenario = new Scenario($data["id"], $data["id_place"], $data["name"], $data["description"], $data["difficulty"], $data["completion_time"]);
        }

        return $scenario;
    }

    public function readAll() {
        // Perform SQL queries
        $scenarios = array();
        $stmt = null;
        $stmt = $this->mysqli->prepare("SELECT * FROM `scenarios`");
        $stmt->execute();

        if($stmt) {
            // store the result in an array
            $result = get_result($stmt);
            while($row = array_shift($result)) {
                $scenario = new Scenario($row["id"], $row["id_place"], $row["name"], $row["description"], $row["difficulty"], $row["completion_time"]);
                array_push($scenarios, $scenario);
            }
        }

        return $scenarios;
    }

    public function update() {
        // TODO
        echo "ScenarioManager.update";
        exit;
    }

    public function delete() {
        // TODO
        echo "ScenarioManager.delete";
        exit;
    }
}