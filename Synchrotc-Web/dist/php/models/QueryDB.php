<?php

require_once "../models/ConnectionDB.php";

class QueryDB {

    public function doThen($query, $action) {
        $connection = new ConnectionDB();
        $mysqli = $connection->mysqli;
        
        if ($result = $mysqli->query($query))
        {
            if ($action) {
                $action($result);
            }

            $result->close();
        }

        $mysqli->close();
        return $result;
    }

    public function select($query) {
        $connection = new ConnectionDB();
        $mysqli = $connection->mysqli;
        
        $result = $mysqli->query($query);

        $mysqli->close();
        return $result;
    }

    public function selectOne($query) {
        $connection = new ConnectionDB();
        $mysqli = $connection->mysqli;
        
        $result = $mysqli->query($query);
        $row = null;
        if ($result) {
            $row = $result->fetch_array(MYSQLI_ASSOC);
            $result->close();
        }

        $mysqli->close();
        return $row;
    }

    public function do($query) {
        $connection = new ConnectionDB();
        $mysqli = $connection->mysqli;
        
        $result = $mysqli->query($query);

        $mysqli->close();
        return $result;
    }

    public function real_escape_string($parameter) {
        $connection = new ConnectionDB();
        $mysqli = $connection->mysqli;
        return $mysqli->real_escape_string($parameter);
    }
}
?>