<?php

require_once "models/ConnectionDB.php";
require_once "models/Utils.php";

class JoinItemScenarioManager {
    public $mysqli;

    public function __construct() {
        $connection = new ConnectionDB();
        $this->mysqli = $connection->mysqli;
    }

    public function readItemsAndActionWithScenarioId($scenario_id) {
        // Perform SQL queries
        $itemAndActions = array();
        $stmt = null;
        // TODO - Fabrice: can't make joins work
        // $stmt = $this->mysqli->prepare("SELECT * FROM ((`join_objects_scenarios` JOIN scenarios ON `join_objects_scenarios`.`id_scenario` = ?) JOIN `objects` on `join_objects_scenarios`.`id_object` = `objects`.`id`)");
        $stmt = $this->mysqli->prepare("SELECT JOS.action, JOS.id_object, O.id_place, O.name FROM `join_objects_scenarios` AS JOS, `objects` as O WHERE JOS.`id_scenario` = ? AND JOS.`id_object` = O.`id`");
        $stmt->bind_param("i", $scenario_id);
        $stmt->execute();

        if($stmt) {
            // store the result in an array
            $result = get_result($stmt);
            while($row = array_shift($result)) {
                $itemAndAction = new stdClass;
                $itemAndAction->id = $row["id_object"];
                $itemAndAction->id_place = $row["id_place"]; // TODO - Show place name instead of id
                $itemAndAction->name = $row["name"];
                $itemAndAction->action = $row["action"];
                array_push($itemAndActions, $itemAndAction);
            }
        }

        return $itemAndActions;
    }
}
?>