<?php
require_once "models/Task.php";
require_once "models/ConnectionDB.php";
require_once "models/Utils.php";

class TaskManager {
    public $mysqli;

    public function __construct() {
        $connection = new ConnectionDB();
        $this->mysqli = $connection->mysqli;
    }

    public function readWithId($id_account, $day) {
        $tasks = array();
        $stmt = null;
        $stmt = $this->mysqli->prepare("SELECT `id` FROM `missions_account` WHERE `id_account` = ? AND `day` = ? ");
        $stmt->bind_param("ii", $id_account, $day);
        $stmt->execute();
        if($stmt) {
            $id_missions_accounts = get_result($stmt);
            while($rowIdAccount = array_shift($id_missions_accounts)) {
                //$id_mission_account = get_result($stmt);
                // Perform SQL queries
                $stmt2 = null;
                $stmt2 = $this->mysqli->prepare("SELECT * FROM `mission_rapport` WHERE `id_mission_account` = ?");
                $stmt2->bind_param("i", $rowIdAccount["id"]);
                $stmt2->execute();

                if($stmt2) {
                   // store the result in an array
                    $result = get_result($stmt2);
                    while($row = array_shift($result)) {
                        $task = new Task($row["id"], $row["id_mission_account"], $row["time_begin"], $row["time_end"], $row["status"], $row["parcour_length"],$row["nb_error"]);
                        array_push($tasks, $task);
                    }
                }
            }
        }

        return $tasks;
    }
}