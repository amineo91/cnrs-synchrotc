<?php
require_once "models/User.php";
require_once "models/ConnectionDB.php";
require_once "models/Utils.php";

class UserManager {
    public $mysqli;

    public function __construct() {
        $connection = new ConnectionDB();
        $this->mysqli = $connection->mysqli;
    }

    private function copyCanvas($idAccountResult,$canvasMission,$count){
        $stmt4 = $this->mysqli->prepare("INSERT INTO `missions_account` (`id_account`, `id_scenario`, `status`, `day`) VALUES (?,?,?,?)");
        //echo "///"+$idAccountResult["id"];
        $stmt4->bind_param("iisi", $idAccountResult, $canvasMission[$count]["id_scenario"], $canvasMission[$count]["status"], $canvasMission[$count]["day"]);
        //echo $canvasMission[$count]["id_scenario"];
        //try{
            $stmt4->execute();
            //echo "exe";
        //}catch(Exception $e){
            //echo $e;
        //}
    }

    public function create($login, $password, $role, $creator_id) {
        // Perform SQL queries
        $stmt = $this->mysqli->prepare("INSERT INTO `accounts` (`login`, `password`, `role`, `creator_id`) VALUES (?,?,?,?)");
        $stmt->bind_param("sssi", $login, $password, $role, $creator_id);
        $returnLater = $stmt->execute();

        $stmt2 = $this->mysqli->prepare("SELECT `id` FROM accounts WHERE `login` = ? AND `password` = ?");
        $stmt2->bind_param("ss", $login, $password);
        $stmt2->execute();
        $idAccountResult = get_result($stmt2);

        $stmt3 = $this->mysqli->prepare("SELECT * FROM canvas_missions");
        $stmt3->execute();
        $canvasMission = get_result($stmt3);
        
        if ( count($canvasMission) > 0) {

            //echo $idAccountResult[0]["id"];
            //echo $canvasMission[0]["id_scenario"];
            //echo $canvasMission[0]["day"];
            //echo $canvasMission[0]["status"];
            // output data of each row
            for($i = 0;$i<=count($canvasMission);$i++){
            $this->copyCanvas($idAccountResult[0]["id"],$canvasMission,$i);
            }
        } else {
            //echo "0 results";
        }

        return $returnLater;

    }

    public function read($login, $password) {
        // Perform SQL queries
        $stmt = $this->mysqli->prepare("SELECT * FROM accounts WHERE `login` = ? AND `password` = ? AND `role` != 'patient'");
        $stmt->bind_param("ss", $login, $password);
        $stmt->execute();

        $user = null;
        
        // Read result
        $result = get_result($stmt);
        if ($account = array_shift($result)) {
            // Do stuff with the data
            $user = new User($account["id"], $account["login"], $account["password"], $account["creation_date"], $account["last_connection"], $account["role"], $account["creator_id"]);

            // Update the last connection
            $stmt = $this->mysqli->prepare("UPDATE `accounts` SET `last_connection`= CURRENT_TIMESTAMP WHERE `id` = ?");
            $stmt->bind_param("d", $user->id);
            $stmt->execute();
        }
        
        return $user;
    }

    public function update() {
        // TODO
        echo "UserManager.update";
        exit;
    }

    public function delete($id) {
        // Perform SQL queries
        $stmt = $this->mysqli->prepare("DELETE FROM `accounts` WHERE `accounts`.`id` = ?");
        $stmt->bind_param("i", $id);
        return $stmt->execute();
    }

    public function readAll() {
        // Perform SQL queries
        // if admin => see all user
        // if doctor => see his patient only
        $role = $_SESSION["user"]->role;
        $id = $_SESSION["user"]->id;

        $users = array();
        $stmt = null;
        if($role === "admin") {
            $stmt = $this->mysqli->prepare("SELECT * FROM `accounts`");
            $stmt->execute();
        } 

        if($role === "doctor") {
            $stmt = $this->mysqli->prepare("SELECT * FROM `accounts` WHERE `creator_id` = ?");
            $stmt->bind_param("d", $id);
            $stmt->execute();
        }

        if($stmt) {
            // store the result in an array
            $result = get_result($stmt);
            while($row = array_shift($result)) {
                $user = new User($row["id"], $row["login"], $row["password"], $row["creation_date"], $row["last_connection"], $row["role"], $row["creator_id"]);
                array_push($users, $user);
            }
        }

        return $users;
    }

    public function readWithId($id) {
        // Perform SQL queries
        $stmt = $this->mysqli->prepare("SELECT * FROM accounts WHERE `id` = ?");
        $stmt->bind_param("d", $id);
        $stmt->execute();

        $user = null;
        
        // Read result
        $result = get_result($stmt);
        if ($account = array_shift($result)) {
            $user = new User($account["id"], $account["login"], $account["password"], $account["creation_date"], $account["last_connection"], $account["role"], $account["creator_id"]);
        }

        return $user;
    }

    public function readWithLogin($login) {
        // Perform SQL queries
        $stmt = $this->mysqli->prepare("SELECT * FROM accounts WHERE `login` = ?");
        $stmt->bind_param("s", $login);
        $stmt->execute();

        $user = null;
        
        // Read result
        $result = get_result($stmt);
        if ($account = array_shift($result)) {
            $user = new User($account["id"], $account["login"], $account["password"], $account["creation_date"], $account["last_connection"], $account["role"], $account["creator_id"]);
        }

        return $user;
    }

    public function addDetail($patient_id, $age, $trauma) {
        // Perform SQL queries
        $stmt = $this->mysqli->prepare("INSERT INTO `details` (`patient_id`, `age`, `trauma`) VALUES (?,?,?)");
        $stmt->bind_param("dss", $patient_id, $age, $trauma);
        return $stmt->execute();
    }

    public function readDetail($id) {
        // Perform SQL queries
        $stmt = $this->mysqli->prepare("SELECT * FROM `details` WHERE `patient_id` = ?");
        $stmt->bind_param("d", $id);
        $stmt->execute();

        $detail = null;

        // Read result
        $result = get_result($stmt);
        if ($detailData = array_shift($result)) {
            $detail = new \stdClass;
            $detail->patient_id = $detailData["patient_id"];
            $detail->age = $detailData["age"];
            $detail->trauma = $detailData["trauma"];
            $detail->difficulty = $detailData["difficulty"];
        }

        return $detail;
    }
}

?>