<?php
class Message {
    public $id, $patient_id, $file, $date;

    public function __construct($id, $patient_id, $file, $date) {
        $this->id = $id;
        $this->patient_id = $patient_id;
        $this->file = $file;
        $this->date = $date;
    }
}
?>