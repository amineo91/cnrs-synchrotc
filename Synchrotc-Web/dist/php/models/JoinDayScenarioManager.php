<?php

require_once "models/ConnectionDB.php";
require_once "models/Utils.php";

class JoinDayScenarioManager {
    public $mysqli;

    public function __construct() {
        $connection = new ConnectionDB();
        $this->mysqli = $connection->mysqli;
    }

    public function create($day_id, $scenario_id, $order) {
        // Perform SQL queries
        $stmt = $this->mysqli->prepare("INSERT INTO `join_days_scenarios` (`day_id`, `scenario_id`, `order`) VALUES (?,?,?)");
        $stmt->bind_param("iii", $day_id, $scenario_id, $order);
        $stmt->execute();
    }

    public function delete($day_id, $scenario_id) {
        // Perform SQL queries
        $stmt = $this->mysqli->prepare("DELETE FROM `join_days_scenarios` WHERE `join_days_scenarios`.`day_id` = ? AND `join_days_scenarios`.`scenario_id` = ?");
        $stmt->bind_param("ii", $day_id, $scenario_id);
        return $stmt->execute();
    }

    public function readScenarioAndOrderWithDayId($day_id) {
        // Perform SQL queries
        $scenarioAndOrders = array();
        $stmt = null;
        // TODO - Fabrice: can't make joins work
        //$stmt = $this->mysqli->prepare("SELECT * FROM ((`join_days_scenarios` JOIN days ON `join_days_scenarios`.`day_id` = ?) JOIN `scenarios` on `join_days_scenarios`.`scenario_id` = `scenarios`.`id`)");
        $stmt = $this->mysqli->prepare("SELECT JDS.order, JDS.scenario_id, S.id_place, S.name, S.description, S.difficulty, S.completion_time FROM `join_days_scenarios` AS JDS, `scenarios` as S WHERE JDS.`day_id` = ? AND JDS.`scenario_id` = S.`id` ORDER BY JDS.order");
        $stmt->bind_param("i", $day_id);
        $stmt->execute();

        if($stmt) {
            // store the result in an array
            $result = get_result($stmt);
            while($row = array_shift($result)) {
                $scenarioAndOrder = new stdClass;
                $scenarioAndOrder->id = $row["scenario_id"];
                $scenarioAndOrder->id_place = $row["id_place"];
                $scenarioAndOrder->name = $row["name"];
                $scenarioAndOrder->description = $row["description"];
                $scenarioAndOrder->difficulty = $row["difficulty"];
                $scenarioAndOrder->completion_time = $row["completion_time"];
                $scenarioAndOrder->order = $row["order"];
                array_push($scenarioAndOrders, $scenarioAndOrder);
            }
        }

        return $scenarioAndOrders;
    }
}
?>