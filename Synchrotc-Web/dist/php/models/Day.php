<?php
class Day {
    public $id, $id_account, $day, $begin_time, $end_time, $fail_reason, $parcour_player;

    public function __construct($id, $id_account, $day, $begin_time, $end_time, $fail_reason, $parcour_player) {
        $this->id = $id;            
        $this->id_account = $id_account;        
        $this->day = $day;      
        $this->begin_time = $begin_time;  
        $this->end_time = $end_time;    
        $this->fail_reason = $fail_reason;  
        $this->parcour_player = $parcour_player;  
    }
}
?>