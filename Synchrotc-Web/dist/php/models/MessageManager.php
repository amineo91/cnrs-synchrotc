<?php
require_once "models/Message.php";
require_once "models/ConnectionDB.php";
require_once "models/Utils.php";

class MessageManager {
    public $mysqli;

    public function __construct() {
        $connection = new ConnectionDB();
        $this->mysqli = $connection->mysqli;
    }

    public function readAllWithPatientId($patient_id) {
        // Perform SQL queries
        $messages = array();
        $stmt = null;
        $stmt = $this->mysqli->prepare("SELECT * FROM `messages` WHERE `patient_id` = ?");
        $stmt->bind_param("d", $patient_id);
        $stmt->execute();

        if($stmt) {
            // store the result in an array
            $result = get_result($stmt);
            while($row = array_shift($result)) {
                $message = new Message($row["id"], $row["patient_id"], $row["file"], $row["date"]);
                array_push($messages, $message);
            }
        }

        return $messages;
    }
}