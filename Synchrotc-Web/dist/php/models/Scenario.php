<?php
class Scenario {
    public $id, $id_place, $name, $description, $difficulty, $completion_time;

    public function __construct($id, $id_place, $name, $description, $difficulty, $completion_time) {
        $this->id = $id;
        $this->id_place = $id_place;
        $this->name = $name;
        $this->description = $description;
        $this->difficulty = $difficulty;
        $this->completion_time = $completion_time;
    }
}
?>