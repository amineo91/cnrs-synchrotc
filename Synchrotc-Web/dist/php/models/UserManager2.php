<?php
// require_once __DIR__."/User.php";
// require_once __DIR__."/ConnectionDB.php";
require_once "models/User.php";
require_once "models/ConnectionDB.php";
require_once "models/Utils.php";

class UserManager2 {
    public $mysqli;

    public function __construct() {
        $connection = new ConnectionDB();
        $this->mysqli = $connection->mysqli;
    }

    public function read($login, $password) {
        // Perform SQL queries
        $stmt = $this->mysqli->prepare("SELECT * FROM accounts WHERE `login` = ? AND `password` = ? AND `role` != 'patient'");
        $stmt->bind_param("ss", $login, $password);
        $stmt->execute();

        $user = null;
        
        $result = get_result($stmt);
        if ( $account = array_shift( $result ) ) {
            // Do stuff with the data
            $user = new User($account["id"], $account["login"], $account["password"], $account["creation_date"], $account["last_connection"], $account["role"], $account["creator_id"]);

            // Update the last connection
            $stmt = $this->mysqli->prepare("UPDATE `accounts` SET `last_connection`= CURRENT_TIMESTAMP WHERE `id` = ?");
            $stmt->bind_param("d", $user->id);
            $stmt->execute();
        }
        
        return $user;
    }
}

?>