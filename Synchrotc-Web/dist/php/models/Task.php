<?php
class Task {
    public $id, $id_mission_account, $begin_time, $end_time, $status, $parcour_length, $nb_error;

    public function __construct($id, $id_mission_account, $begin_time, $end_time, $status, $parcour_length, $nb_error) {
        $this->id = $id;            
        $this->id_mission_account = $id_mission_account;        
        $this->begin_time = $begin_time;  
        $this->end_time = $end_time;    
        $this->status = $status;  
        $this->parcour_length = $parcour_length;  
        $this->nb_error = $nb_error;  
    }
}
?>