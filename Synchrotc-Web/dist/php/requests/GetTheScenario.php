<?php
 
require_once "../models/ConnectionDB.php";
require_once "../models/Utils.php";

$connection = new ConnectionDB();
$mysqli = $connection->mysqli;

// Get parameters
$id_day = $mysqli->real_escape_string($_GET['id_day']);
$id_patient = $mysqli->real_escape_string($_GET['id_player']);

// Do queries
$request = 'SELECT S.id, S.name, S.description, P.name as location, MA.id as id_missionAccount FROM scenarios AS S, places as P, missions_account as MA WHERE MA.day = '. $id_day . ' AND S.id = MA.id_scenario AND MA.id_account = '. $id_patient .' AND P.id = S.id_place';
$stmt = $mysqli->prepare($request);

if($stmt==false)
    print_r($mysqli->error);
    
$isOk = $stmt->execute();

if($isOk==false)
    print_r($mysqli->error);

// Constants
$querySeparatorItems = "<html></br></html>";
$rowSeparator = "<html></br></br></html>";
$rowSeparatorItems = "</br>";
$fieldSeparator = ";";

if($stmt) {
    // store the result in an array
    $result = get_result($stmt);
    while($row = array_shift($result)) {

        foreach($row as $key => $value){
            echo($value);
            echo($fieldSeparator);
        }

        $id_scenario = $row["id"];

        $requestItems = 'SELECT O.name, O.id_place, JOS.action FROM objects AS O, join_objects_scenarios as JOS WHERE JOS.id_scenario = '. $id_scenario . ' AND O.id = JOS.id_object';
        $stmtItems = $mysqli->prepare($requestItems);

        if($stmtItems==false)
            print_r($mysqli->error);
            
        $isOk = $stmtItems->execute();

        if($isOk==false)
            print_r($stmtItems->error);
        
        echo($querySeparatorItems);
        
        if($stmtItems) {
            // store the result in an array
            $resultItem = get_result($stmtItems);
            while($rowItem = array_shift($resultItem)) {
                foreach($rowItem as $key => $value){
                    echo($value);
                    echo($fieldSeparator);
                }
                echo($rowSeparatorItems);
            }
        }

        echo($rowSeparator);
    }
}

?>