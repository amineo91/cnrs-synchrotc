<?php
 
require_once "../models/ConnectionDB.php";
require_once "../models/Utils.php";

$connection = new ConnectionDB();
$mysqli = $connection->mysqli;

$idRapport = $mysqli->real_escape_string($_GET['id_scenario_rapport']); 
$PosX = $mysqli->real_escape_string($_GET['PosX']);
$PosY = $mysqli->real_escape_string($_GET['PosY']);


$stmt = $mysqli->prepare("INSERT INTO `position_player` (`id_rapport_day`,`posX`,`posY`) VALUES (?,?,?)");
$stmt->bind_param("idd", $idRapport, $PosX,$PosY);

$returnLater = $stmt->execute();
if($returnLater==false)
print_r($mysqli->error);

?>