<?php

require_once "../models/ConnectionDB.php";
require_once "../models/Utils.php";

$id_patient = $_POST["id_patient"];

$connection = new ConnectionDB();
$mysqli = $connection->mysqli;

// Get parameters
$idPatient = $mysqli->real_escape_string($id_patient);

$ListToCSVied = array();

// Do queries
/*$request = 'SELECT * FROM details WHERE patient_id = '. $idPatient;
$stmt = $mysqli->prepare($request);

if($stmt==false)
    print_r($mysqli->error);
    
$isOk = $stmt->execute();

if($isOk==false)
    print_r($mysqli->error);

if($stmt) {
    array_push($ListToCSVied,get_result($stmt));  
}

$request = 'SELECT * FROM rapport_day WHERE id_account = '.$idPatient;
$stmt2 = $mysqli->prepare($request);

if($stmt2==false)
    print_r($mysqli->error);
    
$isOk = $stmt2->execute();

if($isOk==false)
    print_r($mysqli->error);

if($stmt2) {
    array_push($ListToCSVied,get_result($stmt2)); 
}*/

$request = 'SELECT position_player.id_rapport_day, position_player.posX, position_player.posY, position_player.timestamp, position_player.ref_bottom_left_corner FROM position_player, rapport_day WHERE position_player.id_rapport_day = rapport_day.id AND rapport_day.id_account ='.$idPatient;
$stmt3 = $mysqli->prepare($request);

if($stmt3==false)
    print_r($mysqli->error);
    
$isOk = $stmt3->execute();

if($isOk==false)
    print_r($mysqli->error);

if($stmt3) {
    array_push($ListToCSVied,get_result($stmt3));
}
/*

$request = 'SELECT MR.id, S.name, MR.time_begin, MR.time_end, MR.status, MR.fail_cause, MR.parcour_length, MR.nb_error_speech, MR.nb_error_object, MR.nb_error_AFK FROM scenarios AS S, missions_account as MA, mission_rapport as MR WHERE S.id = MA.id_scenario AND MA.id_account = '.$idPatient.' AND MR.id_mission_account = MA.id';
$stmt4 = $mysqli->prepare($request);

if($stmt4==false)
    print_r($mysqli->error);
    
$isOk = $stmt4->execute();

if($isOk==false)
    print_r($mysqli->error);

if($stmt4) {
    array_push($ListToCSVied,get_result($stmt4));
}*/


download_send_headers("data_export_positions_" . date("Y-m-d") . ".csv");

echo array2csv($ListToCSVied[0]);
/*echo array2csv($ListToCSVied[1]);
echo array2csv($ListToCSVied[2]);
echo array2csv($ListToCSVied[3]);*/

die();




function array2csv(array &$array)
{
   if (count($array) == 0) {
     return null;
   }
   ob_start();
   $df = fopen("php://output", 'w');
   fputcsv($df, array_keys(reset($array)));
   foreach ($array as $row) {
        fputcsv($df, $row);
   }
   fclose($df);
   return ob_get_clean();
}

function download_send_headers($filename) {
    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
}

?>