<?php
 
require_once "../models/ConnectionDB.php";
require_once "../models/Utils.php";

$connection = new ConnectionDB();
$mysqli = $connection->mysqli;

// Get parameters
$id_date = $mysqli->real_escape_string($_GET['id_date']);
$id_scenario = $mysqli->real_escape_string($_GET['id_scenario']);
$nbErrors = $mysqli->real_escape_string($_GET['nbErrors']);

// Do queries
$request = 'UPDATE join_days_scenarios SET nberror = '.$nbErrors.' WHERE day_id = '.$id_date.' AND scenario_id ='.$id_scenario.'';
$stmt = $mysqli->prepare($request);

if($stmt==false)
    print_r($mysqli->error);
    
$isOk = $stmt->execute();

if($isOk==false)
    print_r($mysqli->error);
?>