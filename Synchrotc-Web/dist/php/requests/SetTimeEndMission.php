<?php
 
require_once "../models/ConnectionDB.php";
require_once "../models/Utils.php";

$connection = new ConnectionDB();
$mysqli = $connection->mysqli;

$idRapport = $mysqli->real_escape_string($_GET['id_mission_rapport']);
$nbErrorSpeech = $mysqli->real_escape_string($_GET['nb_error_speech']);
$nbErrorObject = $mysqli->real_escape_string($_GET['nb_error_object']);
$nbErrorAFK = $mysqli->real_escape_string($_GET['nb_error_afk']);
$lengthParcour = $mysqli->real_escape_string($_GET['parcour_length']); 

$stmt = $mysqli->prepare("UPDATE `mission_rapport` SET `time_end` = NOW(), `status` = 'success', `nb_error_speech` = ".$nbErrorSpeech.", `nb_error_object` = ".$nbErrorObject.", `nb_error_AFK` = ".$nbErrorAFK.", `parcour_length` = ".$lengthParcour." WHERE `id` = ".$idRapport);

$returnLater = $stmt->execute();
if($returnLater==false)
print_r($mysqli->error);

$stmt2 = $mysqli->prepare("SELECT `id_account`,`day` FROM `rapport_day` WHERE `id` = ".$idRapport);
$returnLater02 = $stmt2->execute();

if($returnLater02==false)
print_r($mysqli->error);

   if($stmt2) {
        // store the result in an array
        $result = get_result($stmt2);
    }

?>