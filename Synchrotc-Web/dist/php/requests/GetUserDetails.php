<?php
 
require_once "../models/ConnectionDB.php";
require_once "../models/Utils.php";

$connection = new ConnectionDB();
$mysqli = $connection->mysqli;

// Get parameters
$idPatient = $mysqli->real_escape_string($_GET['idPatient']);

// Do queries
$request = 'SELECT difficulty FROM details WHERE patient_id = '. $idPatient;
$stmt = $mysqli->prepare($request);

if($stmt==false)
    print_r($mysqli->error);
    
$isOk = $stmt->execute();

if($isOk==false)
    print_r($mysqli->error);

if($stmt) {
    $rowSeparator = "<html></br></br></html>";

    // store the result in an array
    $result = get_result($stmt);
    while($row = array_shift($result)) {
        foreach($row as $key => $value){
            echo($value);
        }
        echo($rowSeparator);
    }
}

?>