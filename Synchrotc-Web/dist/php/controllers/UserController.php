<?php
require_once "models/UserManager.php";
require_once "models/DayManager.php";
require_once "models/MessageManager.php";

class UserController {
    public static function listing() {
        $currentUser = $_SESSION["user"];

        // retrive the list of user
        $userManager = new UserManager();
        $users = $userManager->readAll();

        require_once "views/userList.php";
    }

    public static function add($login, $password, $role) {
        $currentUser = $_SESSION["user"];

        $userManager = new UserManager();
        $result = $userManager->create($login, $password, $role, $currentUser->id);

        if($result === true) {
            if($role === "patient") {
                $user = $userManager->readWithLogin($login);

                require_once "views/details.php";
            } else {
                self::listing();
            }
        } else {
            // TODO
            echo "User already exists";
        }
    }

    public static function delete($patient_id) {
        $currentUser = $_SESSION["user"];

        $userManager = new UserManager();
        $patient = $userManager->readWithId($patient_id);

        // if the patient creator is the doctor or if its an admin
        if($currentUser->role === "admin" || $currentUser->id == $patient->creator_id) {
            $result = $userManager->delete($patient_id);

            if($result === true) {
                self::listing();
            } else {
                // TODO
                echo "Failed delete user";
            }
        } else {
            SessionController::home();
        }
    }

    public static function addDetail($patient_id, $age, $trauma) {
        
        $userManager = new UserManager();
        $result = $userManager->addDetail($patient_id, $age, $trauma);

        if($result === true) {
            self::listing();
        } else {
            echo "Error add detail";
        }
    }

    public static function detail($id) {
        $currentUser = $_SESSION["user"];

        $userManager = new UserManager();
        $user = $userManager->readWithId($id);

        if($user != null && ($currentUser->role === "admin" || $currentUser->id == $user->creator_id) ) {

            // get patient details
            $detail  = $userManager->readDetail($user->id);

            // get the patien days
            $dayManager = new DayManager();
            $days = $dayManager->readAllWithPatientId($user->id);
            $messageManager = new MessageManager();
            $messages = $messageManager->readAllWithPatientId($user->id);

            require_once "views/userDetail.php";
        } else {
            SessionController::home();
        }
    }
}
?>