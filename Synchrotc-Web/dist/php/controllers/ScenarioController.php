<?php
require_once "models/ScenarioManager.php";
require_once "models/JoinItemScenarioManager.php";
require_once "models/PlaceManager.php";

class ScenarioController {
    public static function listing() {
        $currentUser = $_SESSION["user"];

        // retrive the list of scenario
        $scenarioManager = new ScenarioManager();
        $scenarios = $scenarioManager->readAll();

        // get places
        $placeManager = new PlaceManager();
        $places = $placeManager->readAll();

        require_once "views/scenarioList.php";
    }

    public static function add($id_place, $name, $description, $difficulty, $completion_time) {
        $currentUser = $_SESSION["user"];

        if($currentUser->role === "admin") {
            $scenarioManager = new ScenarioManager();
            $result = $scenarioManager->create($id_place, $name, $description, $difficulty, $completion_time);
    
            if($result === true) {
                self::listing();
            } else {
                // TODO
                echo "Error";
            }
        } else {
            self::listing();
        }
    }

    public static function detail($id) {
        $currentUser = $_SESSION["user"];

        if($currentUser->role === "admin") {
            // get scenario details
            $scenarioManager = new ScenarioManager();
            $scenario  = $scenarioManager->read($id);

            // get place
            $placeManager = new PlaceManager();
            $place = $placeManager->read($scenario->id_place);

            // get items binded to scenario
            $joinItemScenarioManager = new JoinItemScenarioManager();
            $itemsAndAction = $joinItemScenarioManager->readItemsAndActionWithScenarioId($id);

            require_once "views/scenarioDetail.php";
        } else {
            SessionController::home();
        }
    }
}
?>