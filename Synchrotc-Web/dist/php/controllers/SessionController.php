<?php
require_once "models/User.php";
require_once "models/UserManager.php";

class SessionController {
    public static function login($login, $password) {
        $userManager = new UserManager();
        
        $user = $userManager->read($login, $password);

        if($user) {
            $_SESSION["user"] = $user;
        }

        SessionController::home();
    }

    public static function logout() {
        $_SESSION = array();
        session_destroy();
        
        require "views/home.php";
    }

    public static function home() {
        if( isset($_SESSION["user"]) )  {
            $user = $_SESSION["user"];
            switch ($user->role) {
                case "admin":
                    require "views/admin.php";
                    break;
                case "doctor":
                    require "views/doctor.php";
                    break;
                default:
                    require "views/home.php";
            }
        } else {
            require "views/home.php";
        }
    }
}
?>