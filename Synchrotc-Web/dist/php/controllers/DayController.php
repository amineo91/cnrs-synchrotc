<?php
require_once "models/DayManager.php";
require_once "models/UserManager.php";
require_once "models/TaskManager.php";
require_once "models/JoinDayScenarioManager.php";
require_once "controllers/UserController.php";
require_once "controllers/SessionController.php";

class DayController {

    public static function add($patient_id, $date) {
        $currentUser = $_SESSION["user"];

        $userManager = new UserManager();
        $patient = $userManager->readWithId($patient_id);

        // if the patient creator is the doctor or if its an admin
        if($currentUser->role === "admin" || $currentUser->id == $patient->creator_id) {
            $dayManager = new DayManager();
            $result = $dayManager->create($patient_id, $date);
    
            if($result === true) {
                UserController::detail($patient_id);
            } else {
                // TODO
                echo "Day already exists";
            }
        } else {
            SessionController::home();
        }
    }

    public static function detail($id, $patient_id) {
        $currentUser = $_SESSION["user"];

        $userManager = new UserManager();
        $patient = $userManager->readWithId($patient_id);

        // if the patient creator is the doctor or if its an admin
        if($currentUser->role === "admin" || $currentUser->id == $patient->creator_id) {
            $dayManager = new DayManager();
            $day = $dayManager->readWithId($patient_id, $id);

            $taskManager = new TaskManager();
            $tasks = $taskManager->readWithId($patient_id, $day->day);

           /* $joinDayScenarioManager = new JoinDayScenarioManager();
            $scenarioAndOrders = $joinDayScenarioManager->readScenarioAndOrderWithDayId($id);
            
            $scenarioManager = new ScenarioManager();
            $scenarios = $scenarioManager->readAll();
            
            // get places
            $placeManager = new PlaceManager();
            $places = $placeManager->readAll();*/

            require_once "views/dayDetail.php";
        } else {
            SessionController::home();
        }
    }

    public static function delete($id, $patient_id) {
        $currentUser = $_SESSION["user"];

        $userManager = new UserManager();
        $patient = $userManager->readWithId($patient_id);

        // if the patient creator is the doctor or if its an admin
        if($currentUser->role === "admin" || $currentUser->id == $patient->creator_id) {
            $dayManager = new DayManager();
            $result = $dayManager->delete($id);

            if($result === true) {
                UserController::detail($patient_id);
            } else {
                // TODO
                echo "Failed delete day";
            }
        } else {
            SessionController::home();
        }
    }

    public static function addScenario($day_id, $scenario_id, $order) {
        $currentUser = $_SESSION["user"];

        $dayManager = new DayManager();
        $day = $dayManager->readWithId($day_id);

        $userManager = new UserManager();
        $patient = $userManager->readWithId($day->patient_id);

        // if the patient creator is the doctor or if its an admin
        if($currentUser->role === "admin" || $currentUser->id == $patient->creator_id) {
            $joinDayScenarioManager = new JoinDayScenarioManager();
            $scenarioAndOrders = $joinDayScenarioManager->create($day_id, $scenario_id, $order);
            
            self::detail($day_id, $patient->id);
        } else {
            SessionController::home();
        }
    }

    public static function deleteScenario($day_id, $scenario_id) {
        $currentUser = $_SESSION["user"];
        
        $dayManager = new DayManager();
        $day = $dayManager->readWithId($day_id);

        $userManager = new UserManager();
        $patient = $userManager->readWithId($day->patient_id);

        //echo 'deleteScenario';
        // if the patient creator is the doctor or if its an admin
        if($currentUser->role === "admin" || $currentUser->id == $patient->creator_id) {
            $joinDayScenarioManager = new JoinDayScenarioManager();
            $result = $joinDayScenarioManager->delete($day_id, $scenario_id);

            self::detail($day_id, $patient->id);
        } else {
            SessionController::home();
        }
    }
}
?>